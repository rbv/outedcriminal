
bug: should have bet in this case, given betsizes available...
but maybe it computed opponent should never call that bet:

((((BOT))))  ->first_bet
Using endgame_bet

possible scores =  [[59, 84], [43, 78]] probabilities = [[ 0.6   0.4 ]
 [ 0.45  0.55]]
seq table time: 0.00057s
solve time: 0.0186s
profile payoffs for p0: [15.165255591054313]
profile payoffs for p1: [-15.165255591054313]
Payoff by quadrant: [[ 40.60001 -62.01438]
 [ 51.58946  70.     ]]
Total expected value: 15.183360859
ante: 46 betsizes: [[25, 15], [25, 15]]
behaviours for 0: [[-1, 0, -1, -1], [-1, 0, 25, 15], [-1, 0, -1, 15], [0, 25, None, None], [1, 15, -1, None], [1, 15, 10, None], [1, 15, -1, None]]
behaviours for 1: [[-1, 0, -1, -1], [-1, 0, 25, 15], [-1, 0, -1, 15], [0, 25, None, None], [1, 15, -1, None], [1, 15, 10, None], [1, 15, -1, None]]
strat0
[[ 0.23643  0.62318  0.123    0.0174   0.       0.       0.     ]
 [ 0.       0.65803  0.       0.34197  0.       0.       0.     ]]
Cumulative:
[[ 0.23643  0.85961  0.9826   1.       1.       1.       1.     ]
 [ 0.       0.65803  0.65803  1.       1.       1.       1.     ]]
strat1
[[ 0.73642  0.       0.       0.26358  0.       0.       0.     ]
 [ 0.       0.       0.       1.       0.       0.       0.     ]]
Cumulative:
[[ 0.73642  0.73642  0.73642  1.       1.       1.       1.     ]
 [ 0.       0.       0.       1.       1.       1.       1.     ]]

Uniform [0,1]: 0.605287405324 index: 1
Behaviour: [-1, 0, 25, 15]
betsizes: [[25, 15], [25, 15]] score_idx: 1
selected behaviour:  [-1, 0, 25, 15]
((((BOT))))  ->first_bet = 0



bug:
values almost 0
remember_best ([[3, 1], [2, 0], [4, 5]], [[5, 2], [4, 3]]) value 7.25628060507e-20 cnt 432 46


bot played 22 rounds against hindrance doctor!


       SimpleD3Bot vs        SimpleD2Bot: 0.89 +/ 0.45  (996 games)
       SimpleD3Bot vs    LessSimpleD2Bot: 0.58 +/ 0.44  (1063 games)
       SimpleD3Bot vs        SimpleR2Bot: 1.99 +/ 0.43  (944 games)
   LessSimpleD3Bot vs        SimpleD2Bot: 0.81 +/ 0.45  (1021 games)
   LessSimpleD3Bot vs    LessSimpleD2Bot: 1.19 +/ 0.43  (1150 games)
   LessSimpleD3Bot vs        SimpleR2Bot: 1.99 +/ 0.43  (1006 games)
       SimpleD2Bot vs    LessSimpleD2Bot: -0.10 +/ 0.44  (1097 games)
       SimpleD2Bot vs        SimpleR2Bot: 1.88 +/ 0.43  (935 games)
Ran 3000 rounds each pairup

     SRSimpleD2Bot vs        SimpleD2Bot: -0.01 +/ 0.41  (1187 games)
Ran 3500 rounds each pairup
real	1m46.100s
