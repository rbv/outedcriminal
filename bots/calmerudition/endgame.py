#!/usr/bin/env python

import sys
import os
import numpy as np
import itertools
import pickle
import gambit
import random

import search
import inner_circle
import cordialminuet.util as util
from cordialminuet.util import print3


def _play():
    game = gambit.Game.new_tree()
    p = game.players.add('0')
    game.players.add('1')
    print("0:", p)
    print("chance:", game.players.chance)

    g = gambit.Game.new_table([2,2])


    g[0,0][0] = 8

    g[0,0][1] = 8

    g[0,1][0] = 2

    g[0,1][1] = 10

    g[1,0][0] = 10

    g[1,1][1] = 2

    g[1,0][1] = 2

    g[1,1][0] = 5

    g[1,1][1] = 5

    print(g.outcomes)
    print(g.players[0].strategies)
    print(list(g.contingencies))
    p = g.mixed_strategy_profile()
    print("profile:",p)
    print(p.payoff(0))
    print(p.payoff(g.players[0]))
    with util.Timer() as timer:
        print(gambit.nash.lcp_solve(g))
    print(timer)
    with util.Timer() as timer:
        print(gambit.nash.lcp_solve(g, rational=False))
    print(timer)
    with util.Timer() as timer:
        print(gambit.nash.lcp_solve(g, rational=True, external=True))
    print(timer)
    with util.Timer() as timer:
        solver = gambit.nash.ExternalLogitSolver()
        print(solver.solve(g))
    print(timer)


    # with util.Timer() as timer:
    #     print(gambit.nash.lp_solve(g))
    # print(timer)

class SolutionTimes:
    setup = 0.
    seq_form = 0.
    normal_form = 0.
    new_table = 0.
    solve = 0.
    other = 0.
    def __str__(self):
        return (
            'setup %.3g ' % self.setup
            + 'seq_form %.3g ' % self.seq_form
            + 'normal_form %.3g ' % self.normal_form
            + 'new_table %.3g ' % self.new_table
            + 'solve %.3g ' % self.solve
            + 'other %.3g' % self.other
        )

timings = SolutionTimes()

PAYOFF_MULT = 20.

stCHECK_FOLD = 0   # check-fold
stCHECK_MATCH = 1
stBET = 2

bhFOLD = -1
# Nonzero integers are bet amount or
bhCHECK = 0

class BasicEndGame():
    """
    Basic endgame where both players bet with fixed bet size.

    scores:
        A [2][2] array giving the two possible scores (possibly equal)
        for each player.
    score_probs:
        A [2][2] array where score_probs[i][j] is probability of player i getting scores[i][j]

    Sequence-form strategies if player i gets scores[i][j] are:
        strat[j][0]: chance to check-fold
        strat[j][1]: chance to check-match
        strat[j][2]: chance to bet
    The strategies need to sum to:
        (strat.sum(axis = 1) == score_probs[i]).all()
    So 4 independent parameters in total, and 9 pure strategies (payoff matrix rows)
    """
    def __init__(self, scores, score_probs, ante, betsizes = None):
        """
        See class docstring.
        ante is the intial size of the pot.
        betsizes can optionally be constructed later
        """
        self.scores = scores
        assert np.array(scores).shape == (2, 2)
        self.score_probs = np.array(score_probs)
        self.ante = ante
        if betsizes is not None:
            self.set_betsizes(betsizes)

        # Chance of the scores = (i,j) instance occurring
        instance_probs = np.ones((2,2))
        instance_probs[0,:] *= self.score_probs[0,0]
        instance_probs[1,:] *= self.score_probs[0,1]
        instance_probs[:,0] *= self.score_probs[1,0]
        instance_probs[:,1] *= self.score_probs[1,1]
        assert np.isclose(instance_probs.sum(), 1.)
        self.instance_probs = instance_probs

    def set_betsizes(self, betsizes):
        assert len(betsizes) == 2
        b0, b1 = betsizes
        if isinstance(b0, int):
            b0 = [b0]
        if isinstance(b1, int):
            b1 = [b1]
        self.betsizes = [b0, b1]

        # Form strategies:
        self.behaviours = [self._form_behaviours(0, b0, b1), self._form_behaviours(1, b1, b0)]

    def _form_behaviours(self, who, betsizes, other_betsizes):
        # A behaviour is a list giving actions to perform
        # First two elements are initial bet index and initial bet, followed by k responses to each of the
        # opponent's possible bet sizes --- which may not be possible for that behaviour.
        # The responses are bhFOLD==-1 for fold, or additional amount to be >= 0
        # The initial bet index is -1 for no bet, otherwise an index into betsizes
        # The behaviours list for a player only includes actual possible play.
        k = len(other_betsizes)
        ret = []
        for init_bet_idx, init_bet in enumerate([0] + betsizes):
            # if init_bet == 0 and who == self.who_must_bet:
            #     # Calling is not an option, analysing a game 
            #     continue
            for how_high_to_call_idx in range(-1,k):  # index
                if how_high_to_call_idx == -1:
                    how_high_to_call = 0
                else:
                    how_high_to_call = other_betsizes[how_high_to_call_idx]
                if how_high_to_call < init_bet and False:
                    # This behaviour is relevant
                    #print3("skip", how_high_to_call, init_bet)
                    continue
                
                behaviour = [init_bet_idx - 1, init_bet]
                for other in other_betsizes:
                    if other <= init_bet:
                        # this can't happen
                        behaviour.append(None)
                    elif other > how_high_to_call:
                        behaviour.append(bhFOLD)
                    else:
                        behaviour.append(other - init_bet)
                if len(ret) and ret[-1] == behaviour:
                    continue
                ret.append(behaviour)
        #print3("betsizes", betsizes, other_betsizes)
        #print3("behaviours")
        #print3(ret)
        return ret


    # def payoff(self, player0_score_idx, strat0, strat1):
    #     """Expected payoff for player 0, given score scores[0][player0_score_idx]"""
        
    def seq_tables(self, two_tables = False):
        #if len(self.betsizes[0]) == 1 and len(self.betsizes[1]) == 1:   #BROKEN
        #    return self._seq_tables_single_betsizes(two_tables)
        return self._seq_tables_multi_betsizes(two_tables)

    def _seq_tables_single_betsizes(self, two_tables = False):
        """Return tuple of two sequence-form payoff matrices for the game,
        partitioned into quarters: shape [2][3][2][3],
        where [i,j][k,l] is the payoff if player 0 gets scores[0][i]
        and uses strategy j, and player 1 gets scores[1][k] and uses
        strategy l.

        two_tables:  Return payoff tables for player 0 and 1 rather than just first
        """
        a = self.ante
        [b0], [b1] = self.betsizes
        payoff0 = np.zeros((2,3,2,3))
        if two_tables:
            payoff1 = np.zeros((2,3,2,3))
        else:
            payoff1 = None
        
        for player0_score_idx in range(2):
            score0 = self.scores[0][player0_score_idx]
            for player1_score_idx in range(2):
                score1 = self.scores[1][player1_score_idx]
                for strat0 in range(3):
                    for strat1 in range(3):
                        if strat0 == stBET and strat1 == stCHECK_FOLD:
                            value0 = a - search.tribute([a + b0, a])
                            value1 = -a
                        elif strat1 == stBET and strat0 == stCHECK_FOLD:
                            value0 = -a
                            value1 = a - search.tribute([a, a + b1])
                        else:
                            value = a
                            # One bets, the other bets or matches
                            if strat0 == stBET:
                                value = a + b0
                            if strat1 == stBET:
                                value = max(value, a + b1)

                            trib = search.tribute([value, value])
                            if score0 < score1:
                                value0 = -value
                                value1 = value - trib
                            elif score0 == score1:
                                value0 = value1 = 0
                            else:
                                value0 = value - trib
                                value1 = -value
                        index = player0_score_idx, strat0, player1_score_idx, strat1
                        payoff0[index] = value0
                        if two_tables:
                            payoff1[index] = value1
        return payoff0, payoff1

    def _seq_tables_multi_betsizes(self, two_tables = False):
        """Return tuple of two sequence-form payoff matrices for the game,
        partitioned into quarters: shape [2][n0][2][n1],
        where [i,j][k,l] is the payoff if player 0 gets scores[0][i]
        and uses strategy j, and player 1 gets scores[1][k] and uses
        strategy l.

        two_tables:  Return payoff tables for player 0 and 1 rather than just first
        """
        a = self.ante
        b0, b1 = self.betsizes
        n0 = len(self.behaviours[0])
        n1 = len(self.behaviours[1])
        payoff0 = np.zeros((2,n0,2,n1))
        if two_tables:
            payoff1 = np.zeros((2,n0,2,n1))
        else:
            payoff1 = None
        
        for player0_score_idx in range(2):
            score0 = self.scores[0][player0_score_idx]
            for player1_score_idx in range(2):
                score1 = self.scores[1][player1_score_idx]
                for i0, strat0 in enumerate(self.behaviours[0]):
                    for i1, strat1 in enumerate(self.behaviours[1]):
                        # initial bet indices (plan number)
                        initidx0 = strat0[0]
                        initidx1 = strat1[0]
                        # initial bets
                        init0 = strat0[1]
                        init1 = strat1[1]
                        pot = [a + init0, a + init1]
                        flop = True
                        if init0 > init1:
                            # Check player 1's response
                            response = strat1[2 + initidx0]
                            if response == -1:
                                # fold
                                value0 = pot[1] - search.tribute(pot)
                                value1 = -pot[1]
                                flop = False
                            else:
                                # match
                                assert response + pot[1] == pot[0]
                                pot[1] = pot[0]
                        elif init1 > init0:
                            response = strat0[2 + initidx1]
                            if response == -1:
                                # fold
                                value1 = pot[0] - search.tribute(pot)
                                value0 = -pot[0]
                                flop = False
                            else:
                                # match
                                assert response + pot[0] == pot[1]
                                pot[0] = pot[1]
                        if flop:
                            assert pot[0] == pot[1]
                            value = pot[0]
                            trib = search.tribute(pot)
                            if score0 < score1:
                                value0 = -value
                                value1 = value - trib
                            elif score0 == score1:
                                value0 = value1 = 0
                            else:
                                value0 = value - trib
                                value1 = -value
                        index = player0_score_idx, i0, player1_score_idx, i1
                        payoff0[index] = value0
                        if two_tables:
                            payoff1[index] = value1
        return payoff0, payoff1

    def to_normal_table(self, table):
        """Expand a sequential form payoff table from seq_table() to strategic form,
        using self.score_probs.

        Each strategy in the matrix is a pair of strategies in the seq form matrix."""
        # Number of strategies/actions for each player
        _, n0, _, n1 = table.shape
        ret = np.zeros((n0, n0, n1, n1))
        # Scale payoffs in each quadrant according to its actual chance to occur
        scaled = table.copy()
        scaled[0,:,:,:] *= self.score_probs[0,0]
        scaled[1,:,:,:] *= self.score_probs[0,1]
        scaled[:,:,0,:] *= self.score_probs[1,0]
        scaled[:,:,1,:] *= self.score_probs[1,1]
        # for st0_0, st0_1, st1_0, st1_1 in itertools.product(range(n0), range(n0), range(n1), range(n1)):
        #     # If player 0 plays (i,j) and player 1 plays (k,l), then
        #     # expected payoff is i&k + i&l + j&k + j&l,
        #     # where a&b is payoff for playing a and b times probability of it happening
        #     ret[st0_0, st0_1, st1_0, st1_1] += scaled[0, st0_0, 0, st1_0]
        #     ret[st0_0, st0_1, st1_0, st1_1] += scaled[0, st0_0, 1, st1_1]
        #     ret[st0_0, st0_1, st1_0, st1_1] += scaled[1, st0_1, 0, st1_0]
        #     ret[st0_0, st0_1, st1_0, st1_1] += scaled[1, st0_1, 1, st1_1]

        # Faster way
        ret[:, :, :, :] += scaled[0, :, 0, :][:,None,:,None]
        ret[:, :, :, :] += scaled[0, :, 1, :][:,None,None,:]
        ret[:, :, :, :] += scaled[1, :, 0, :][None,:,:,None]
        ret[:, :, :, :] += scaled[1, :, 1, :][None,:,None,:]
        return ret.reshape(n0 ** 2, n1 ** 2)
        
    def normal_table(self, two_tables = False):
        """Return the payoff matrix or matrices in normal/strategic form"""
        payoff0, payoff1 = self.seq_tables(two_tables) 
        if two_tables:
            return self.to_normal_table(payoff0), self.to_normal_table(payoff1)
        return self.to_normal_table(payoff0)

    
    def strategy_normal_to_sequence_form(self, half_profile, who = None, verbose = False):
        """Go back from solution to normal form game to sequential form.
        half_profile: half a NashProfile: a mixture of the pure normal-form strategies for one player.

        Returns a seq form strategy of shape [2][n].
        Each half of the strategy sums to 1, which is NOT the usual convention!
        """
        # if who == 0:
        #     n = n0
        # else:
        #     n = n1
        n = int(round(len(half_profile) ** 0.5))
        ret = np.zeros((2,n))
        half_profile = half_profile.reshape((n, n))
        if verbose:
            print3("profile for player", who)
            print3(half_profile)

        # Break up the (low-score strategy, high-score strategy) pairs
        for st_low, st_high in itertools.product(range(n), range(n)):
            cell = half_profile[st_low, st_high]
            ret[0, st_low] += cell #* self.score_probs[who,0]
            ret[1, st_high] += cell #* self.score_probs[who,1]
        return ret

    def sequence_form_quadrant_payoff(self, seqtable, strat0, strat1):
        """Returns a [2,2] array giving payoffs for low/high score for each player.
        seqtable: [2,n0,2,n1] array giving sequence-form payoffs
        stratI: [2,nI] array giving chance to play each strategy"""
        ret = np.zeros((2,2))
        _, n0, _, n1 = seqtable.shape
        #seqtable = seqtable.reshape((2*n0, 2*n1))
        #print3("shapes", strat0.shape, strat1.shape, seqtable.shape)
        ret[0,0] = strat0[0] .dot( seqtable[0,:,0,:] ).dot( strat1[0] )
        ret[0,1] = strat0[0] .dot( seqtable[0,:,1,:] ).dot( strat1[1] )
        ret[1,0] = strat0[1] .dot( seqtable[1,:,0,:] ).dot( strat1[0] )
        ret[1,1] = strat0[1] .dot( seqtable[1,:,1,:] ).dot( strat1[1] )
        return ret

    def _NashProfile_to_seq_strategies(self, game, profile):
        """
        profile: a MixedStrategyProfile"""
        profile0 = profile[game.players[0]]
        profile1 = profile[game.players[1]]
        #print3("profile", profile0, type(profile0))
        strat0 = self.strategy_normal_to_sequence_form(np.array(profile0), 0)
        strat1 = self.strategy_normal_to_sequence_form(np.array(profile1), 1)
        return strat0, strat1

    def form_and_solve_gambit_tree_Game(self):
        """Tree form instead of normal (table) form. Gambit turns out to be unusuably slow"""
        game = gambit.Game.new_tree()
        chance = game.players.chance
        chance.label = 'chance'
        p0 = game.players.add()
        p0.label = 'p0'
        p1 = game.players.add()
        p1.label = 'p1'
        players = [p0, p1]

        # At root, chance picks high/low scores for each player
        infoset = game.root.append_move(chance, 4)
        for idx, prob in enumerate(self.score_probs.flatten()):
            infoset.actions[idx].prob = gambit.Rational(int(128 * prob), 128)
        children = list(game.root.children)
        # The indices of subroots are: [whether p0 has high card][whether p1 has high card]
        subroots = [children[:2], children[2:]]
        assert subroots[1][1].is_successor_of(game.root)

        # infoset_nodes[player_idx][low_or_high] contains nodes 
        infoset_nodes = [[], []], [[], []]

        def set_outcomes(node, o0, o1):
            outcome = game.outcomes.add()
            node.outcome = outcome
            if o0 > 0:
                o0 -= search.tribute([o0, o0])
            if o1 > 0:
                o1 -= search.tribute([o1, o1])
            outcome[0] = o0
            outcome[1] = o1

        for idx0 in 0, 1:
            score0 = self.scores[0][idx0]
            for idx1 in 0, 1:
                score1 = self.scores[1][idx0]
                node0 = subroots[idx0][idx1]
                # Player 0 bets
                bets0 = [0] + self.betsizes[0]
                infoset = node0.append_move(p0, len(bets0))
                #children1 = list(node.children)
                for bet0_idx, bet0 in enumerate(bets0):
                    # Player 1 bets
                    node1 = node0.children[bet0_idx]
                    bets1 = [0] + self.betsizes[0]
                    infoset = node1.append_move(p1, len(bets1))
                    #children2 = list(node2.children)
                    for bet1_idx, bet1 in enumerate(bets1):
                        # Resolve
                        node2 = node1.children[bet1_idx]
                        #print3(node2.is_terminal)

                        pot = [self.ante + bet0, self.ante + bet1]
                        if bet1 < bet0:
                            # player 1 bet or raise
                            node2.append_move(p1, 2)
                            fold, match = node2.children
                            set_outcomes(fold, pot[1], -pot[1])
                            # Go on and calc outcomes for match node
                            pot[1] = self.ante + bet0
                        elif bet0 < bet1:
                            # player 0 bet or raise
                            node2.append_move(p0, 2)
                            fold, match = node2.children
                            set_outcomes(fold, -pot[0], pot[0])
                            # Go on and calc outcomes for match node
                            pot[0] = self.ante + bet1
                        else:
                            # Already matched
                            match = node2


                        assert pot[0] == pot[1]
                        value = pot[0]
                        if score0 < score1:
                            value0 = -value
                            value1 = value
                        elif score0 == score1:
                            value0 = value1 = 0
                        else:
                            value0 = value
                            value1 = -value
                        set_outcomes(match, value0, value1)

        # Each player chooses to bet
        print3(type(infoset))

        force_lp = False
        with util.Timer() as t:
            if force_lp:
                profiles = []
            else:
                profiles = gambit.nash.lcp_solve(game, rational=False, max_depth=1, stop_after=1)# max_depth=1)
            # if len(profiles) == 0:
            #     print "FOUND NONE"
            #     profiles = gambit.nash.lp_solve(game, rational=False)
        timings.solve += t.time
        print3("tree solve time:", t)
        if len(profiles) != 1:
            print3("profiles", profiles)
            #assert len(profiles) == 1
        #profile = profiles[0]
        print(profiles)


        return game

    def form_gambit_Game(self, seqtable0, seqtable1 = None):
        """
        Returns a normal-form gambit.Game, with payoffs multiplied by PAYOFF_MULT
        If seqtable1 is None, not a bimatrix game
        """
        with util.Timer() as t:
            normtable0 = self.to_normal_table(seqtable0)
            if seqtable1 is not None:
                normtable1 = self.to_normal_table(seqtable1)
            else:
                normtable1 = -normtable0
        timings.normal_form += t.time
        #print3("normal table time:", t)
        #print3(normtable0)

        m0, m1 = normtable0.shape
        with util.Timer() as timer:
            if True:
                # Old
                normtable0 = (normtable0 * PAYOFF_MULT).astype(int).astype(object)
                normtable1 = (normtable1 * PAYOFF_MULT).astype(int).astype(object)
                game = gambit.Game.new_table(normtable0.shape)
                for i0 in range(m0):
                    nt0 = list(normtable0[i0])
                    nt1 = list(normtable1[i0])
                    for i1 in range(m1):
                        outcome = game._get_contingency(i0,i1)
                        #outcome = game[i0,i1]
                        #outcome[0] = normtable0[i0,i1]
                        #outcome[1] = normtable1[i0,i1]
                        outcome[0] = nt0[i1]
                        outcome[1] = nt1[i1]
            else:
                # New
                temp = np.empty((m1, m0, 2), dtype = int)
                temp[:,:,0] = PAYOFF_MULT * normtable0.T
                temp[:,:,1] = PAYOFF_MULT * normtable1.T
                temp = temp.flatten()
                #body = ' '.join(str(x) for x in temp.flatten())
                with util.Timer() as timer2:
                    body = ' '.join([np.array_str(temp[i*1000:i*1000+1000], max_line_width = 999999)[1:-1] for i in range(1 + len(temp) / 1000)])
                timings.other += timer2.time
                #print body
                #body = np.array_str(temp.flatten(), max_line_width = 70)[1:-1]
                template = """NFG 1 R "title"
                { "Player 1" "Player 2" } { %d %d }

%s"""
                text = template % (m0, m1, body)
                game = gambit.Game.parse_game(text)
                        
        timings.new_table += timer.time
        #print3("Game setup time:", timer)
        return game

    def save_gambit_Game(self, fname, two_tables = False):
        seqtable0, seqtable1 = self.seq_tables(two_tables)
        game = self.form_gambit_Game(seqtable0, seqtable1)
        contents = game.write()
        with open(fname, "w") as fil:
            fil.write(contents)

    def solve(self, two_tables = False, verbose = False, printout = False, force_lp = False):
        if printout:
            verbose = True
        with util.Timer() as t:
            seqtable0, seqtable1 = self.seq_tables(two_tables)
            #print3(seqtable0)
            #print3(seqtable1)
        timings.seq_form += t.time
        if verbose:
            print3("seq table time:", t)

        game = self.form_gambit_Game(seqtable0, seqtable1)
        with util.Timer() as t:
            if force_lp:
                profiles = []
            else:
                profiles = gambit.nash.lcp_solve(game, rational=False, max_depth=1, stop_after=1)# max_depth=1)
            if len(profiles) == 0:
                print("FOUND NONE: lcp_solve failed, falling back to lp_solve")
                profiles = gambit.nash.lp_solve(game, rational=False)
        timings.solve += t.time
        if verbose:
            print3("solve time:", t)
            print3("profile payoffs for p0:", [prof.payoff(0) / PAYOFF_MULT for prof in profiles])
            print3("profile payoffs for p1:", [prof.payoff(1) / PAYOFF_MULT for prof in profiles])
            # print3("profile0 strategy payoffs for p0:", np.array(profiles[0].strategy_values(game.players[0])) / PAYOFF_MULT)
            # print3("profile0 strategy payoffs for p1:", np.array(profiles[0].strategy_values(game.players[1])) / PAYOFF_MULT)
            
        if len(profiles) != 1:
            print3(len(profiles), "profiles:", profiles)
            #assert len(profiles) == 1
        profile = profiles[0]

        with util.Timer() as t:
            strat0, strat1 = self._NashProfile_to_seq_strategies(game, profile)
        timings.normal_form += t.time

        quadrant_payoffs = self.sequence_form_quadrant_payoff(seqtable0, strat0, strat1)

        total_value = (quadrant_payoffs * self.instance_probs).sum()
        # The tests for disagreement between gambit's reported payoff
        # and our computed. No idea why it can be large
        if abs(profiles[0].payoff(0) / PAYOFF_MULT - total_value) > 4.5:
            print3(profiles[0].payoff(0) / PAYOFF_MULT, total_value)
            assert False

        if verbose:
            print3("Payoff by quadrant:", quadrant_payoffs)
            print3("Total expected value:", total_value)

        if printout:
            print3("ante:", self.ante, "betsizes:", self.betsizes)
            print3("behaviours for 0:", self.behaviours[0])
            print3("behaviours for 1:", self.behaviours[1])
            print3("strat0")
            print3(strat0)
            print3("Cumulative:")
            print3(np.cumsum(strat0, axis = 1))
            print3("strat1")
            print3(strat1)
            print3("Cumulative:")
            print3(np.cumsum(strat1, axis = 1))
            print3()

        return strat0, strat1, quadrant_payoffs, float(total_value)
        
        
    def play_solve(self, seqtable):
        """
        Playing around.
        table: seq form table
        """
        g = self.form_gambit_Game(seqtable)

        def consider(profile):
            strat0, strat1 = self._NashProfile_to_seq_strategies(g, profile)
            print3("Player0 seq-strat", strat0)
            print3("Player1 seq-strat", strat1)
            quadrant_payoffs = self.sequence_form_quadrant_payoff(seqtable, strat0, strat1)

            print3("Payoff by instance:", quadrant_payoffs)
            print3("Total expected value:", (quadrant_payoffs * self.instance_probs).sum())

        # print(list(g.outcomes[0]))
        # print(g.players[0].strategies)
        # print(list(g.contingencies))
        #p = g.mixed_strategy_profile()
        # print("profile:",p)
        # print(p.payoff(0))
        
        with util.Timer() as timer:
            print3(gambit.nash.lp_solve(g, rational=False))
        print3("lp_solve no rational", timer)
        with util.Timer() as timer:
            print3(gambit.nash.lcp_solve(g))
            
        print3("\nlcp_solve-------", timer)
        with util.Timer() as timer:
            profiles = gambit.nash.lcp_solve(g, rational=False, max_depth=1, stop_after=1)
            #print3("profile", profiles[0])
            print3("Payoff for 0", float(profiles[0].payoff(0)))
            print3("Payoff for 1", float(profiles[0].payoff(1)))
            consider(profiles[0])
        print3("lcp_solve no rational", timer)

        print3("\nlcp_solve rat ext------", timer)
        with util.Timer() as timer:
            ret=gambit.nash.lcp_solve(g, rational=True, external=True)
            print3(ret)
            print3("Payoff for 0", float(ret[0].payoff(0)))
            print3("Payoff for 1", float(ret[0].payoff(1)))           
        print3("lcp_solve rat ext", timer)
        
        print3("\nExternalLogitSolver-------")
        with util.Timer() as timer:
            solver = gambit.nash.ExternalLogitSolver()
            profiles = solver.solve(g)
            print3("Payoff for 0", float(profiles[0].payoff(0)))
            print3("Payoff for 1", float(profiles[0].payoff(1)))
            consider(profiles[0])
        print3("ExternalLogitSolver", timer)

def endgame_testcase():
    p1 = 0.5
    p2 = 0.5
    score_probs = [[1 - p1, p1], [1 - p2, p2]]
    endgame = BasicEndGame([[20, 40], [10, 30]], score_probs, ante = 1, betsizes = (2,2))
    normal0 = endgame.normal_table(two_tables = False)
    assert normal0[0,0] == 0.5
    assert normal0[8,8] == 0.75
    assert normal0[6,6] == 0.75
    assert normal0[0,3] == 0.5

    
def endgame_testcase_multi_betsizes():
    p1 = 0.5
    p2 = 0.5
    score_probs = [[1 - p1, p1], [1 - p2, p2]]

    endgame = BasicEndGame([[20, 40], [10, 30]], score_probs, ante = 1, betsizes = ([2,4], [2,4]))
    assert endgame.behaviours[0] == [[-1, 0, -1, -1], [-1, 0, 2, -1], [-1, 0, 2, 4], [0, 2, None, -1], [0, 2, None, 2], [1, 4, None, None]]
    assert endgame.behaviours[0] == endgame.behaviours[1]
    table, _ = endgame.seq_tables()
    assert table[0,0,1,5] == -1
    assert table[0,2,1,5] == -5
    assert table[0,3,1,5] == -3
    assert table[0,4,1,5] == -5
    assert table[1,4,1,4] == 3
    assert table[1,5,1,4] == 5

def find_optimal_betsizes(endgame, try_hard = False):
    """
    """
    # half = max(1, endgame.ante * 0.5)
    # guess = half, half
    n0 = 3
    n1 = 6
    n2 = 8

    def inner(b1, b2):
        with util.Timer() as t:
            endgame.set_betsizes((b1, b2))
        timings.setup += t.time

        stuff = endgame.solve()
        strat0, strat1, quadrant_payoffs, total_value = stuff
        #table, _ = endgame.seq_tables()
        #print3(table.reshape(12,12), table.shape)
        #printout(endgame, stuff)
        return quadrant_payoffs, total_value
        

    global timings
    timings = SolutionTimes()

    ante = endgame.ante
    bets1_0 = np.linspace(1, 1.8 * ante, n0).astype(int)
    bets1_1 = np.linspace(1, 5 * ante, n1).astype(int)

    def minimise(b1):
        best_quad = None
        lowest = 1e99
        best_bets = None
        for idx1, r0 in enumerate(bets1_0):
            for idx2, r1 in enumerate(bets1_1):
                if r0 < r1:
                    quadrant_payoffs, val = inner(b1, [r0, r1])
                    if val < lowest:
                        lowest = val
                        best_quad = quadrant_payoffs
                        best_bets = [r0, r1]
        return best_quad, lowest, best_bets

    # maximise
    r3 = ante
    best_quad = None
    highest = -1e99
    for r4 in np.linspace(1, 5 * ante, n2).astype(int):
        b1 = [r4]
        quadrant_payoffs, val, b2 = minimise(b1)
        if val > highest:
            highest = val
            best_quad = quadrant_payoffs
            best_bets = b1, b2

    print3(timings)
    return highest, best_quad, best_bets
        
    
def experiment_endgame_solvers():
    p1 = 0.4
    p2 = 0.6
    score_probs = [[1 - p1, p1], [1 - p2, p2]]
    endgame = BasicEndGame([[20, 40], [10, 30]], score_probs, ante = 1, betsizes = (2,2))
    with util.Timer() as t:
        payoff0, payoff1 = endgame.seq_tables(two_tables = True)
    print3("seq table:", t)
    #print3(payoff0.reshape(6,6))
    #print3(payoff1.reshape(6,6))
    endgame.play_solve(payoff0)

def experiment_endgame_betsizes():
    ante = 10
    
    p1 = 0.7333
    p2 = 0.4
    score_probs = [[1 - p1, p1], [1 - p2, p2]]

    endgame = BasicEndGame([[10, 20], [15, 30]], score_probs, ante = ante)
    print3(find_optimal_betsizes(endgame))
    print(endgame.instance_probs)
    print()
    return


    def inner(b1, b2):
        with util.Timer() as t:
            endgame = BasicEndGame([[10, 20], [15, 30]], score_probs, ante = ante, betsizes = (b1, b2))
        timings.setup += t.time

        stuff = endgame.solve(printout = False)
        strat0, strat1, quadrant_payoffs, total_value = stuff
        
        #table, _ = endgame.seq_tables()
        #print3(table.reshape(12,12), table.shape)
        #printout(endgame, stuff)
        return total_value
        
    n = 6
    values = np.zeros((n,n))

    #global timings
    #for p2 in np.linspace(0.05, 0.95, 5):
        #timings = SolutionTimes()
    r3 = 1
    bets1_0 = np.linspace(1, 8 * ante, n).astype(int)
    bets1_1 = np.linspace(1, 8 * ante, n).astype(int)
    for r4 in np.linspace(1, 4 * ante, n).astype(int):
        print("player 0 bets=", [r4])
        print("player 1 bets=", bets1_0, bets1_1)

        for idx1, r0 in enumerate(bets1_0):
            for idx2, r1 in enumerate(bets1_1):
                val = inner([r3,r4], [r0, r1])
                values[idx1, idx2] = val / ante
        print3(values)
        print3("min:", values.min())
        print3()
    print3(timings)
    print3("ante = ", ante)
    print
    return


    betsizes = (1, 2, 3, 10, 100)

    for idx1, b1 in enumerate(betsizes):
        for idx2, b2 in enumerate(betsizes):
            with util.Timer() as t:
                endgame = BasicEndGame([[10, 40], [20, 30]], score_probs, ante = 1, betsizes = (b1, b2))
            timings.setup += t.time
            strat0, strat1, quadrant_payoffs, total_value = endgame.solve()
            values[idx1, idx2] = total_value
    print3(timings)

class EndGameCounter:
    "For computing number of distinct possible score distributions"
    # Assume player 1 has $pips distinct scores
    # player 2's scores are equal to these, or in between
    num = 0
    pips = 2
    all_types = []
    allow_singular = False
    def recurse(self, bins, pipnum = 0, lastbin = 0):
        if pipnum == self.pips:
            self.num += 1
            self.all_types.append(bins[:])
        else:
            for nextbin in range(lastbin, self.pips * 2 + 1):
                if self.allow_singular == False and (nextbin == lastbin and lastbin % 2 == 1):
                    # can't have 2 pips equal to one of player0's pips
                    continue
                bins[nextbin] += 1
                #print("recurse", pipnum, nextbin)
                self.recurse(bins, pipnum + 1, nextbin)
                bins[nextbin] -= 1

    def enum(self):
        bins = [0] * (self.pips * 2 + 1)
        self.recurse(bins)


BIN_COUNT = 11
#TEMP HACK FOR BOT
cache_file = '/mnt/common2/proj/outedcriminal/cached_endgames_v1_' + '%dbin.pickled' % BIN_COUNT

def reverse_endgame_type(p1_idx, p2_idx):
    p1 = (p1_idx + 0.5) * 1. / BIN_COUNT
    p2 = (p2_idx + 0.5) * 1. / BIN_COUNT
    return p1, p2

def load_endgame_cache():
    if not os.path.isfile(cache_file):
        return {}
    with open(cache_file, 'rb') as f:
        cached = pickle.load(f)
    return cached

def endgame_cache_lookup(cached_games, poss_scores, p1, p2, ante):
    """
    Lookup an endgame in the cached_games dict.
    p1: chance of player 0 getting high score
    p1: chance of player 1 getting high score
    """
    key = inner_circle.endgame_type(poss_scores, p1, p2, ante)
    if key not in cached_games:
        print3("eee",scores, score_probs[0][1], score_probs[1][1], ante)
        return (0., np.array([[0., 0.], [0., 0.]]), [[1], [1]])
    (value, quad, bets) = cached_games[key]
    # if ante <= 3:
    #     mult = ante / 2.
    # else:
    #     mult = ante / 10.
    value *= ante
    quad *= ante
    return (value, quad, bets)


def cache_endgames(find_betsizes = False):
    endgames = EndGameCounter()
    endgames.allow_singular = True  # allow scores like [[20,20],[30,40]]
    endgames.enum()
    all_score_types = []
    for bins in endgames.all_types:
        # find 1st nonzero
        for idx, num in enumerate(bins):
            if num:
                s10 = idx
                bins[idx] -= 1
                break
        # find 2nd pip
        for idx, num in enumerate(bins):
            if num:
                s11 = idx
                break
        # if s10 < s11:
        #     # Scores can be mirrored
        #     if 

        all_score_types.append( [[1, 3], [s10, s11]] )
    print(all_score_types)
    #return

    #cached = {}
    cached = load_endgame_cache()
    #assert len(sys.argv) == 2
    #iii = int(sys.argv[1])
    # TODO: could switch to multiprocessing, think Gambit is leaking
    # huge amounts of memory
    for ante in (10,):
        for scores in all_score_types:
            print("solving for ante %d scoretype %s" % (ante, scores))
        #for scores in all_score_types[iii:iii+1]:
            for p1_idx in range(BIN_COUNT):
                for p2_idx in range(BIN_COUNT):
                    p1, p2 = reverse_endgame_type(p1_idx, p2_idx)
                    score_probs = [[1 - p1, p1], [1 - p2, p2]]

                    key = inner_circle.endgame_type(scores, p1, p2, ante)
                    endgame = BasicEndGame(scores, score_probs, ante)
                    
                    if find_betsizes:
                        value, quad, bets = find_optimal_betsizes(endgame)
                        print3(key, scores, score_probs, ante, "   VALUE= %3g MIN=%3g MAX=%3g" % (value, quad.min(), quad.max()))
                    else:
                        betinc = ante + 1
                        betsizes = [int(betinc * .5), int(betinc), int(betinc * 1.6)]
                        bets = [betsizes, betsizes]
                        endgame.set_betsizes(bets)
                        stuff = endgame.solve()
                        strat0, strat1, quad, value = stuff
    
                    assert key not in cached
                    cached[key] = (value, quad, bets)


    with open(cache_file, 'wb') as f:
        pickle.dump(cached, f)
    print3("cached", len(cached))

def test_cache_symmetry():
    "Check how close cached endgame results are after swapping player 0, player 1."
    cached = load_endgame_cache()
    for num in range(20):
        p1 = random.uniform(0, 1)
        p2 = random.uniform(0, 1)
        ante = 4
        scores = [[10, 12], [8, random.randint(9, 13)]]

        #score_probs = [[1 - p1, p1], [1 - p2, p2]]
        value1, quad1, bets1 = endgame_cache_lookup(cached, scores, p1, p2, ante)
        value2, quad2, bets2 = endgame_cache_lookup(cached, [scores[1], scores[0]], p2, p1, ante)
        print()
        print3("p1 %s p2 %s scores %s ante %s" % (p1, p2, scores, ante))
        print3("ret 1:", value1, quad1, bets1)
        print3("ret 2:", value2, quad2, bets2)

        

def experiment_count_endgames():
    temp = EndGameCounter()
    temp.enum()
    print(temp.num)
    print(temp.all_types)

if __name__ == '__main__':
    cache_endgames()
    #experiment_count_endgames()
    exit()
    #experiment_endgame_solvers()
    #endgame_testcase_multi_betsizes()
    experiment_endgame_betsizes()
