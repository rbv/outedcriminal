# distutils: language = c++
# distutils: sources = bots/calmerudition/possible_scores.cpp


import numpy as np
cimport numpy as np
cimport cython
cimport libc.string
cimport libc.math

ctypedef np.uint32_t uint32_t
ctypedef np.int32_t int32_t
ctypedef np.int_t npint_t
ctypedef np.uint8_t pixel_t
ctypedef np.float_t float_t

cdef extern from "possible_scores.hpp":
    cdef enum:
        MAX_SCORE_RANGE = 106

    cdef cppclass PossibleScores:
        void computePossibleScoresFast()
        #### Input

        # The game board is stored column-wise, [row*6 + col]
        int mGameBoard[36]

        # -1 if not known or applicable
        int columnsGivenUs[3]
        int rowsGivenUs[3]
        int columnsGivenThem[3]
        int rowsGivenThem[3]
        # The column we revealed, or -1 for none
        int mRevealChoiceForUs

        #### Result (boolean arrays)
        char mOurPossibleScores[MAX_SCORE_RANGE]
        char mTheirPossibleScores[MAX_SCORE_RANGE]
        char mOurPossibleScoresFromTheirPerspective[MAX_SCORE_RANGE]



def possible_scores(state):
    """Given a GameState, returns a triple
    (ourPossibleScores, theirPossibleScores, ourPossibleScoresFromTheirPerspective),
    each is an array of length 106
    """
    assert state.board.strides == (24, 4)
    cdef PossibleScores scores
    libc.string.memcpy(scores.mGameBoard, (<np.ndarray> state.board).data, 36 * sizeof(int32_t))
    for i in range(3):
        if i >= len(state.picks1):
            scores.columnsGivenUs[i] = -1
            scores.columnsGivenThem[i] = -1
        else:
            if state.picks1[i][0] == '?':
                scores.columnsGivenUs[i] = -1
            else:
                scores.columnsGivenUs[i] = state.picks1[i][0]
            #scores.columnsGivenUs[i] = state.picks1[i][0]
            scores.columnsGivenThem[i] = state.picks1[i][1]

        if i >= len(state.picks2):
            scores.rowsGivenUs[i] = -1
            scores.rowsGivenThem[i] = -1
        else:
            scores.rowsGivenUs[i] = state.picks2[i][1]
            if state.picks2[i][0] == '?':
                scores.rowsGivenThem[i] = -1
            else:
                scores.rowsGivenThem[i] = state.picks2[i][0]
    scores.mRevealChoiceForUs = -1
    if len(state.picks1) >= 4:
        scores.mRevealChoiceForUs = state.picks1[3][0]

    scores.computePossibleScoresFast()
    cdef np.ndarray out1, out2, out3
    out1 = np.zeros(MAX_SCORE_RANGE, dtype = np.int8)
    out2 = np.zeros(MAX_SCORE_RANGE, dtype = np.int8)
    out3 = np.zeros(MAX_SCORE_RANGE, dtype = np.int8)
    libc.string.memcpy(out1.data, scores.mOurPossibleScores, MAX_SCORE_RANGE)
    libc.string.memcpy(out2.data, scores.mTheirPossibleScores, MAX_SCORE_RANGE)
    libc.string.memcpy(out3.data, scores.mOurPossibleScoresFromTheirPerspective, MAX_SCORE_RANGE)
    return out1, out2, out3

cdef enum:
    BIN_COUNT = 11

def endgame_type(list poss_scores, float p1, float p2, int ante):
    """poss_scores: [2][2] list of possibles

    Returns integer key for the cache
    """
    cdef int s00, s01, s10, s11, low, high, p1_idx, p2_idx
    s00 = poss_scores[0][0]
    s01 = poss_scores[0][1]
    s10 = poss_scores[1][0]
    s11 = poss_scores[1][1]
    # find the position of player 1's lowest score
    if s10 < s00:
        low = 0
    elif s10 == s00:
        low = 1
    else: #if s10 > s00:
        if s10 < s01:
            low = 2
        elif s10 == s01:
            low = 3
        else:
            low = 4

    # find the position of player 1's highest score
    if s11 < s01:
        if s11 < s00:
            high = 0
        elif s11 == s00:
            high = 1
        else:
            high = 2
    elif s11 == s01:
        high = 3
    else: #if s11 > s01:
        high = 4

    # if ante <= 3:
    #     #ante = 2
    #     ante = 0
    # else:
    #     ante = 1
    #     #ante = 10
    ante = 0

    # 0-BIN_COUNT-1
    p1_idx = min(int(p1 * BIN_COUNT), BIN_COUNT - 1)
    p2_idx = min(int(p2 * BIN_COUNT), BIN_COUNT - 1)

    #return low, high, ante, p1_idx, p2_idx
    return 1000000 * low + 100000 * high + 10000 * ante + 100 * p1_idx + p2_idx
