
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
//#include <unordered_map>

#include <algorithm>
#include "mcts.hpp"




using namespace std;

#define DBG_SIM
#define DBG_SELECT

static const char *movenames[] = {
    "root", "pick", "reveal", "firstbet", "nextbet", "leaf", "ILLEGAL!!!"
};


static uint32_t prng_state = 1;
static bool prng_initialised = false;

// 32 bit int, low bits are very poor
uint32_t prng_int() {
    prng_state = (prng_state * 1103515245 + 12345);
    return prng_state;
}

// range [0, upper_bound)
uint32_t prng_randint(int upper_bound) {
    prng_state = (prng_state * 1103515245 + 12345);
    return (prng_state >> 14) % upper_bound;
    //return ((unsigned long long)prng_state * 0xffffffffU / upper_bound);
}



// Value in [0.0, 1.0] ??
double prng() {
    prng_state = (prng_state * 1103515245 + 12345);
    return (double)prng_state * (1.0 / 0xffffffffU);
}

// Must happen before many routines can be used
void _initialiseGlobals() {
    if (!prng_initialised) {
        srand48(time(NULL));
        prng_state = time(NULL);
        prng_initialised = true;
    }
}



static double gettime() {
    timeval tv;
    gettimeofday(&tv, 0);
    //printf("%ld %ld\n", tv.tv_sec, tv.tv_usec);
    return ((double)tv.tv_sec) + tv.tv_usec * 1e-6;
}


//////////////////////////////////////////////////////////////// poss scores



class PossScoresCalc {
public:
    PossScoresCalc(board_t *board_) : board_ptr(board_) {}

    /*
    // returns pointer to length 106 array
    const char *lookup(State &state);

    int calc_key(State &state);

    unordered_map<int, PossScores> cache;

    */
    board_t *board_ptr;

    struct PossScores {
        int total;
        char ret[106];
    };

};
/*
int PossScoresCalc::calc_key(State &state) {
    return 0;
}

const char *PossScoresCalc::lookup(State &state) {
    return NULL;
}
*/


/*
static void possible_scores_them_sub(board_t &board, int ret[], int public_cols_avail[6], int rows_they_got[3]) {

    int total = 0;
    for (int col0 = 0; col0 < 6; col0++) {  //col
        if (!public_cols_avail[col0])
            continue;
        public_cols_avail[col0] = false;
        int cell0 = board[rows_they_got[0]][col0];
        total += cell0;

        for (int col1 = 0; col1 < 6; col1++) {  //col
            if (!public_cols_avail[col1])
                continue;
            public_cols_avail[col1] = false;
            int cell1 = board[rows_they_got[1]][col1];
            total += cell1;

            for (int col2 = 0; col2 < 6; col2++) {  //col
                if (!public_cols_avail[col2])
                    continue;
                int cell2 = board[rows_they_got[2]][col2];
                ret[total + cell2]++;
                
            }
            total -= cell1;
            public_cols_avail[col1] = true;
        }
        total -= cell0;
        public_cols_avail[col0] = true;
    }
}

static void possible_scores_them_recurse(int pickn, board_t &board, int ret[], int public_cols_avail[6], int rows_they_got[3]) {

    if (pickn == 3) {
        possible_scores_them_sub(*board_ptr, ret, public_cols_avail, rows_they_got);
    } else {

    }

}

// player 1 possible scores
void State::possible_scores_them(int ret[]) {

    int us = 0;
    int them = 1;

    int public_cols_avail[6];
    int rows_they_got[6];
    memcpy(public_cols_avail, public_cols_available[1], 6*sizeof(int));
    for (int pickn = 0; pickn < picks_done; pickn++) {
        rows_they_got[pickn] = picks[us][pickn][1];
    }
    
    // Fill in 

    for (int pickn = picks_done; pickn < 3; pickn++) {
    }


    possible_scores_them_sub(*board_ptr, ret, public_cols_avail, rows_they_got);
}

*/

//////////////////////////////////////////////////////////////// State


State::State(int *board_, int *coins_, int *pot_) {
    board_ptr = (board_t*)board_;
    coins[0] = coins_[0];
    coins[1] = coins_[1];
    pot[0] = pot_[0];
    pot[1] = pot_[1];
    mtype = mtPick;

    memset(forced_bet_sizes, -1, sizeof(forced_bet_sizes));

    for (int idx = 0; idx < 6; idx++) {
        cols_available[0][idx] = true;
        cols_available[1][idx] = true;
        public_cols_available[0][idx] = true;
        public_cols_available[1][idx] = true;
    }
    picks_done[0] = 0;
    picks_done[1] = 0;
    score[0] = 0;
    score[1] = 0;
    picks[0][3][1] = -1;  //not used
    picks[1][3][1] = -1;
    whose_turn = 0;
}

void State::print() {
    printf("----STATE\n");

    board_t &board = *board_ptr;

    for (int row = 0; row < 6; row++) {
        for (int col = 0; col < 6; col++) {
            printf("%2d ", board[row][col]);
        }
        printf("\n");
    }

    printf("coins=%d,%d pot=%d,%d score=%d,%d\n", coins[0], coins[1], pot[0], pot[1], score[0], score[1]);
    move_type move = min(mtype, mtIllegal);
    if (move < 0)
        move = mtIllegal;
    printf("state=%s whose_turn=%d picks_done=%d,%d\n", movenames[move], whose_turn, picks_done[0], picks_done[1]);

    printf("picks[0]: ");
    for (int picknum = 0; picknum < picks_done[0]; picknum++) {
        printf("(%d,%d)", picks[0][picknum][0], picks[0][picknum][1]);
    }
    printf("\npicks[1]: ");
    for (int picknum = 0; picknum < picks_done[1]; picknum++) {
        printf("(%d,%d)", picks[1][picknum][0], picks[1][picknum][1]);
    }

    printf("\ncols available:        ");
    for (int idx = 0; idx < 6; idx++) {  //col
        printf("%d ", cols_available[0][idx]);
    }
    printf("\npublic cols available: ");
    for (int idx = 0; idx < 6; idx++) {  //col
        printf("%d ", public_cols_available[0][idx]);
    }

    printf("\nrows available:        ");
    for (int idx = 0; idx < 6; idx++) {  //row
        printf("%d ", cols_available[1][idx]);
    }
    printf("\npublic rows available: ");
    for (int idx = 0; idx < 6; idx++) {  //row
        printf("%d ", public_cols_available[1][idx]);
    }

    printf("\n");
}



void State::update_scores(int picknum) {
    board_t &board = *board_ptr;
    int row, col;

    row = picks[1][picknum][1];  //row them
    col = picks[0][picknum][0];  //col us
    score[0] += board[row][col];

    row = picks[1][picknum][0];  //row us
    col = picks[0][picknum][1];  //col them
    score[1] += board[row][col];
    // for (int who = 0; who < 2; who++) {
    //     int col = picks[who]
    //     score[who] += 
    //         board[picks1[picknum]
}

// should only be called from next_pick_round()
void State::resolve_winner() {
    if (score[0] < score[1]) {
        coins[1] += pot[0] + pot[1];
    } else if (score[0] > score[1]) {
        coins[0] += pot[0] + pot[1];
    } else {
        int half = (pot[0] + pot[1]) / 2;
        coins[0] += half;
        coins[1] += half;
    }
    pot[0] = 0;
    pot[1] = 0;
}

void State::apply_pick(int who, int pick_us, int pick_them) {
    assert(who == whose_turn);
    assert(pick_us != pick_them);
    assert(pick_us < 6);
    assert(cols_available[who][pick_us]);
    assert(cols_available[who][pick_them]);

    cols_available[who][pick_us] = false;
    cols_available[who][pick_them] = false;
    public_cols_available[who][pick_them] = false;
    picks[who][picks_done[who]][0] = pick_us;
    picks[who][picks_done[who]][1] = pick_them;
    if (who == 1) {
        update_scores(picks_done[1]);
        mtype = mtFirstBet;
    }
    picks_done[who]++;
    whose_turn = who ^ 1;
}

void State::apply_reveal(int who, int reveal_col) {
    assert(who == whose_turn);
    assert(reveal_col < 6);
    assert(picks_done[who] == 3);
    assert(public_cols_available[who][reveal_col]);
    public_cols_available[who][reveal_col] = false;
    picks[who][3][0] = reveal_col;
    picks_done[who]++;
    if (who == 1)  // both sim moves done
        mtype = mtFirstBet;
    whose_turn = who ^ 1;
}



// Called after betting is over and noone folded
void State::next_pick_round() {
    whose_turn = 0;
    if (picks_done[0] < 3)
        mtype = mtPick;
    else if (picks_done[0] == 3)
        mtype = mtReveal;
    else {
        resolve_winner();
        mtype = mtLeaf;
        whose_turn = -1;
    }
}

// Called in mtFirstBet  CALLER CALLS place_bet twice!!
void State::resolve_bets() {
    if (pot[0] == pot[1]) {
        next_pick_round();
    } else if (pot[0] < pot[1]) {
        // in this case have to work out whose turn
        mtype = mtNextBet;
        whose_turn = 0;
    } else {
        mtype = mtNextBet;
        whose_turn = 1;
    }
}

// Called in mtNextBet CALLER CALLs place_bet!!!
void State::resolve_raise(int who, int bet) {
    if (bet == 0) {
        // folded
        mtype = mtLeaf;
        whose_turn = -1;
        coins[1 ^ who] += pot[0] + pot[1];
        pot[0] = 0;
        pot[1] = 0;
    } else {
        // another round of betting needed?
        resolve_bets();
    }
}

void State::apply_bet(int who, int bet) {
    assert(who == whose_turn);
    assert(bet >= 0);
    assert(coins[who] >= bet);
    place_bet(who, bet);
    if (who == 1) {  // both sim moves done
        resolve_bets();
    } else {
        whose_turn = 1;
    }
}

void State::apply_raise(int who, int bet) {
    assert(who == whose_turn);
    assert(bet >= 0);
    assert(coins[who] >= bet);
    place_bet(who, bet);
    assert(pot[who] >= pot[1 ^ who]);  // at least match
    resolve_raise(who, bet);
}

//////////////////////////////////////////////////////////////// Sim AI

static int sum_row(board_t &board, int row_num, int cols_avail[6]) {
    int total = 0;
    for (int idx = 0; idx < 6; idx++) {  //col
        if (cols_avail[idx]) {
            total += board[row_num][idx];
        }
    }
#ifdef DBG_SIM
    //fprintf(stderr, "sum_row(row=%d)=%d\n", row_num, total);
#endif
    return total;
}

static int sum_col(board_t &board, int col_num, int rows_avail[6]) {
    int total = 0;
    for (int idx = 0; idx < 6; idx++) {  //row
        if (rows_avail[idx]) {
            total += board[idx][col_num];
        }
    }
#ifdef DBG_SIM
    //fprintf(stderr, "sum_col(col=%d)=%d\n", col_num, total);
#endif
    return total;
}

// Return expected value of picking these columns (row if who==1)
int State::eval_pick(int who, int col_us, int col_them) {
    if (who == 0) {
        return sum_col(*board_ptr, col_us, public_cols_available[1])
            - sum_col(*board_ptr, col_them, public_cols_available[1]);
    } else {
        return sum_row(*board_ptr, col_us, public_cols_available[0])
            - sum_row(*board_ptr, col_them, public_cols_available[0]);
    }
}

void State::select_pick(int who) {
    int pick_us, pick_them;
    if (picks_done[who] == 0) {
        // pick randomly

        int remaining = 6 - picks_done[who] * 2;
        pick_us = prng_randint(remaining);
        pick_them = prng_randint(remaining - 1);

        for (int idx = 0; idx < 6; idx++) {
            if (idx <= pick_us) {
                //eg look for 2nd available
                if (!cols_available[who][idx]) {
                    pick_us++;
                }
            }
            if (idx <= pick_them) {
                //eg look for 2nd available which isn't pick_us
                if (pick_them == pick_us || !cols_available[who][idx]) {
                    pick_them++;
                }
            }
        }

        guessed_next_scores[who] = 0;

    } else {

        int bestval = -9999;

        for (int idx = 0; idx < 6; idx++) {
            if (!cols_available[who][idx])
                continue;
            for (int idx2 = 0; idx2 < 6; idx2++) {
                if (!cols_available[who][idx2])
                    continue;
                if (idx == idx2)
                    continue;
                int val = eval_pick(who, idx, idx2);
                if (val > bestval) {
                    pick_us = idx;
                    pick_them = idx2;
                }
            }
        }

        guessed_next_scores[who] = bestval;

    }
    apply_pick(who, pick_us, pick_them);
}


void State::simulation() {
    // Move randomly

    //board_t &board = *board_ptr;

    int init_picks = picks_done[0];

    double start_time = gettime();

    // may already be at leaf

    bool done = false;
    while (!done) {
        switch (mtype) {
            case mtPick:
                // both pick best move,
                // one may need to catch up
                assert(picks_done[0] == picks_done[1] || picks_done[0] == picks_done[1] + 1);
                if (picks_done[0] == picks_done[1])
                    select_pick(0);
                select_pick(1);
                assert(picks_done[0] == picks_done[1]);
                break;

            case mtReveal:
                // Don't bother
                if (picks_done[0] < 4)
                    apply_reveal(0, picks[0][0][0]);  // first picked
                if (picks_done[1] < 4)
                    apply_reveal(1, picks[1][0][0]);
                break;

            case mtFirstBet:
                // skip.
                // option one:
                /*
                if (whose_turn == 0)
                    apply_bet(0, 0);
                apply_bet(1, 0);
                */
                // option two (faster):
                next_pick_round();
                break;

            case mtNextBet:
                // Only get here when this is the start of the sim
                if (pot[0] < pot[1]) {
                    apply_raise(0, pot[1] - pot[0]);
                } else if (pot[0] > pot[1]) {
                    apply_raise(1, pot[0] - pot[1]);
                } else
                    assert(false);
                break;

            case mtLeaf:
                done = true;
                break;

            default:
                assert(false);
                break;
        }
    }

#ifdef DBG_SIM
    printf("simulated %d picks in %.5f sec\n", 4 - init_picks, gettime() - start_time);
    printf("  rewards: %.2f,%.2f  scores: %d,%d\n", reward_for(0), reward_for(1), score[0], score[1]);
#endif

}

//////////////////////////////////////////////////////////////// Node

/*
Node *Node::select_child_ucb() {

    double k;

    for (int idx = 0; idx < num_children; idx++) {
        double ucb1 = child_rewards[idx] / child_visits[idx]
            + k * sqrt(log(visit_count) / child_visits[idx]);
    }

}
*/

Node *Node::select_child_exp3() {
#ifdef DBG_SELECT
    printf("select_exp3 at %s:\n", tostr());
#endif

    // select uniformly from nodes w/ zero visits
    int pick = -1;
    int num_unvisited = 0;
    for (int idx = 0; idx < num_children; idx++) {
        if (child_visits[idx] == 0) {
#ifdef DBG_SELECT
            printf("  ch %d unvisited, reward %.3f\n", idx, child_rewards[idx]);
#endif
            num_unvisited++;
            if (prng() < 1. / num_unvisited)
                pick = idx;
        }
    }
    if (pick > -1) {
#ifdef DBG_SELECT
        printf(" picked %d\n", pick);
#endif
        return &getchild(pick);
    }

    // expected reward distribution assumed to have average 0?

    int K = num_children;

    double gamma = min(1., sqrt(K * log(K) / (2.7182 - 1.) / visit_count));

    double eta = gamma / K;

    // rewards should be in range of 0-1
    double scale = 1. / 50;

    double exp_sum = 0.;
    for (int idx = 0; idx < num_children; idx++) {
        exp_sum += exp(eta * scale * child_rewards[idx]);
    }

#ifdef DBG_SELECT
    printf("  gamma %.4f exp_sum %.4f\n", gamma, exp_sum);
#endif

    double rand = prng();  // in [0,1]

    double cum_prob = 0.;
    pick = num_children - 1; // safety
    bool picked = false;
    for (int idx = 0; idx < num_children; idx++) {
        // normalised
        double exp_term = exp(eta * scale * child_rewards[idx]);
        double prob = eta + (1 - gamma) * exp_term / exp_sum;

#ifdef DBG_SELECT
        printf("   ch %d (visits %d reward %.4f) expterm %.4f prob %.4f\n", idx, child_visits[idx], child_rewards[idx], exp_term, prob);
#endif
    
        cum_prob += prob;
        if (rand <= cum_prob && !picked) {
            pick = idx;
            picked = true;
            break;
        }
    }
    if (!(cum_prob > 0.999 && cum_prob < 1.001)) {
        fprintf(stderr,"ERROR: pmf adds to %f\n", cum_prob);
        assert(false);
    }

#ifdef DBG_SELECT
        printf(" picked %d\n", pick);
#endif

    return &getchild(pick);
}

char *Node::tostr() {
    static char buf[1000];
    reward_t reward = -99.999;
    if (parent && parent->child_rewards)
        reward = parent->child_rewards[sibling_idx];
    int our_turn = (child_visits != NULL);
    snprintf(buf, 999, "Node(%s(%d,%d), (hidden move idx %d) our_turn=%d, visits=%d, reward=%.3f, parent=%lx (idx %d), %d children)", movenames[move], pick_us, pick_them, (int)move_index_other_tree, our_turn, visit_count, reward, (intptr_t)parent, sibling_idx, num_children);
    return buf;
}

void Node::print_lineage() {
    Node *node = this;
    printf("lineage:\n");
    while (node) {
        int chidx = node->sibling_idx;
        Node *child = node;
        printf("  %lx %s\n", (intptr_t)node, node->tostr());
        node = node->parent;
        if (!node)
            break;
        // Update parent
        assert(&node->getchild(chidx) == child);
    }
}

// don't initialise children
//Node::Node(Node *parent_) {
Node::Node() {
    parent = NULL; //parent_;
    num_children = 0;
    visit_count = 0;
    child_rewards = NULL;
    child_visits = NULL;
    children = NULL;
    // uninitialised stuff
    sibling_idx = 255;
    move_index_other_tree = 255;  // must never be used
    move = mtIllegal;
    pick_us = pick_them = 255;

    // if (parent) {
    // } else {
    //     // root
    //     move = mtRoot;
    //     //our_turn == (who = 0);
    // }
}

Node::~Node() {
    if (child_rewards)
        delete [] child_rewards;
    if (child_visits)
        delete [] child_visits;
    if (children) {
        // for (int idx = 0; idx < num_children; idx++)
        //     delete getchild(idx);
        delete [] children;
    }
}

void Node::add_blank_children(int num_poss_moves, bool our_move) {
    num_children = num_poss_moves;
    if (our_move) {
        child_visits = new  int[num_children];
        child_rewards = new  reward_t[num_children];
    }
    children = new Node[num_children]; //(this);
    for (int idx = 0; idx < num_children; idx++) {
        children[idx].parent = this;
        children[idx].sibling_idx = idx;
    }
}

// who: who is acting
// tree num is who
void Node::add_pick_children_our_move(State &state, int who) {
    assert(state.whose_turn == who);
    int cols_remaining = 6 - 2 * state.picks_done[who];
    int num_poss_moves = cols_remaining * (cols_remaining - 1);
    add_blank_children(num_poss_moves, true);

    int other_tree_move_index = -1;
    int chidx = 0;
    for (int idx = 0; idx < 6; idx++) {  // pick_them
        if (state.public_cols_available[who][idx])
            other_tree_move_index++;

        if (!state.cols_available[who][idx])
            continue;
        for (int idx2 = 0; idx2 < 6; idx2++) {  // pick_us
            if (!state.cols_available[who][idx2])
                continue;
            if (idx == idx2)
                continue;
            Node &child = getchild(chidx);
            child.move = mtPick;
            child.pick_us = idx2;
            child.pick_them = idx;
            child.move_index_other_tree = other_tree_move_index;
            chidx++;
        }
    }
    assert(chidx == num_children);
    // check number of unassigned rows is right (+1 because we started
    // at -1)
    int rows_remaining = 6 - state.picks_done[who];
    assert(other_tree_move_index + 1 == rows_remaining);
}

// who: who is acting
// tree num is who^1
void Node::add_pick_children_their_move(State &state, int who) {
    assert(state.whose_turn == who);
    int rows_remaining = 6 - state.picks_done[who];
    int num_poss_moves = rows_remaining;
    add_blank_children(num_poss_moves, false);

    int chidx = 0;
    for (int idx = 0; idx < 6; idx++) {
        if (!state.public_cols_available[who][idx])
            continue;
        Node &child = getchild(chidx);
        child.move = mtPick;
        child.pick_us = -1;  //unknown
        child.pick_them = idx;
        child.move_index_other_tree = -1; // not relevant
        chidx++;
    }
    assert(chidx == num_children);
}

void Node::add_reveal_children(State &state, int which_tree) {
    int who = state.whose_turn;
    add_blank_children(3, who == which_tree);
    for (int idx = 0; idx < num_children; idx++) {
        Node &child = getchild(idx);
        child.move = mtReveal;
        child.move_index_other_tree = idx;
        child.reveal_col = state.picks[who][idx][0];
    }
}

void Node::add_betting_children(State &state, int which_tree, int betsizes[3], move_type mtype) {
    // check, low, high

    int who = state.whose_turn;
    int betting_round = state.picks_done[0] - 1;  // 0 to 3
    if (who == 1) {
        for (int idx = 0; idx < NUM_BET_SIZES; idx++) {
            int forced_betsize = state.forced_bet_sizes[betting_round][0][idx];
            if (forced_betsize != -1) {
                betsizes[idx] = forced_betsize;
            }
        }
    }
    int num_unique_betsizes = 1;
    // remove duplicates
    if (betsizes[0] != betsizes[1])
        num_unique_betsizes++;
    if (betsizes[1] != betsizes[2])
        num_unique_betsizes++;

    add_blank_children(num_unique_betsizes, who == which_tree);

    // Special case in which turns don't necessarily alternate
    for (int idx = 0; idx < num_children; idx++) {
        Node &child = getchild(idx);
        child.move = mtype;
        child.move_index_other_tree = idx;
        child.bet = betsizes[idx];
    }
}

void Node::add_firstbet_children(State &state, int which_tree) {
    int who = state.whose_turn;
    int max_bet = state.max_bet(who);
    int pot = state.pot[who];

    int betsizes[NUM_BET_SIZES] = {
        0, //check
        min(1 + pot / 2, max_bet),
        min(1 + pot, max_bet),
    };

    add_betting_children(state, which_tree, betsizes, mtFirstBet);
}

void Node::add_nextbet_children(State &state, int which_tree) {
    int who = state.whose_turn;
    int min_bet = state.min_bet(who);

    int betsizes[NUM_BET_SIZES] = {
        0,        // fold
        min_bet,  // call
        min_bet,  // duplicated so removed; we never raise to prevent chain of raises
    };
    if (who == 1) {
        // ... but the opponent might raise
        int max_bet = state.max_bet(who);
        int pot = state.pot[who];
        betsizes[2] = min(max_bet, min_bet + pot / 2);
    }
    add_betting_children(state, which_tree, betsizes, mtNextBet);
}

// At a node add children
// which_tree: 0 or 1, which tree we are adding to.
//     Could be either player's turn
void Node::add_children(State &state, int which_tree) {
    assert(num_children == 0);
    assert(!state.is_leaf());

    int who = state.whose_turn;
    switch (state.mtype) {
        case mtPick:
            if (which_tree == who)
                add_pick_children_our_move(state, who);
            else
                add_pick_children_their_move(state, who);
            break;
        case mtReveal:
            add_reveal_children(state, which_tree);
            break;
        case mtFirstBet:
            add_firstbet_children(state, which_tree);
            break;
        case mtNextBet:
            add_nextbet_children(state, which_tree);
            break;
        default:
            assert(false);
    }
}

// Used during selection
void Node::update_state(State &state, int who) {
    switch (state.mtype) {
        case mtPick:
            state.apply_pick(who, pick_us, pick_them);
            break;
        case mtReveal:
            state.apply_reveal(who, reveal_col);
            break;
        case mtFirstBet:
            state.apply_bet(who, bet);
            break;
        case mtNextBet:
            state.apply_raise(who, bet);
            break;
        default:
            assert(false);
    }
}

/*
/// Given a node, returns child number of
// move in other tree this correponds to
int Node::encode_public_chidx() {
    switch (move) {
        case mtPick:
            return pick_them_idx;
        case mtReveal:
            return reveal_col_idx;
        case mtFirstBet:
            return bet_idx;
        case mtNextBet:
            return bet_idx;
        default:  //including leaf
            assert(false);
    }
}
*/

// descend both trees one step by selecting move for one player
// who: who is acting at ournode.
void Game::selection_descend_node(int who, State &state, Node *&ournode, Node *&othernode) {
    assert(ournode->is_our_turn());
    assert(state.whose_turn == who);
    assert(ournode->num_children > 0);
    assert(othernode->num_children > 0);
    ournode = ournode->select_child_exp3();
    // update state
    ournode->update_state(state, who);

    // int chidx = ournode->encode_private_chidx();
    // assert(ournode->num_children > chidx);
    // ournode = ournode->getchild(chidx);

    // descend in other tree using the public information
    assert(!othernode->is_our_turn());
    //int chidx = ournode->encode_public_chidx();
    int chidx = ournode->move_index_other_tree;
    // Check not trying to descend on a partially observed move;
    // other tree should be processing this
    assert(chidx > 0);
    assert(othernode->num_children > chidx);
    othernode = &othernode->getchild(chidx);
}

void Game::selection_step(State &state, Node *&node0, Node *&node1) {
    if (state.whose_turn == 0) {
        selection_descend_node(0, state, node0, node1);
    } else {
        selection_descend_node(1, state, node1, node0);
    }
}
/*
void Game::selection(State &state, Node *&node0, Node *&node1) {
    // Recurse down both trees at once
    
    node0 = tree0;
    node1 = tree1;

    while (true) {
        // Any time one player takes an action,
        // the other observes something, so descends in their
        // tree too, though no statistics are saved. 

        if (state.is_leaf())
            break;

        if (!node0->initialised() || !node1->initialised())
            break;

        selection_step(state, node0, node1);
    }
}
*/

// node: in one of the two trees
void Game::backprop(Node *node, reward_t reward) {
    while (node) {
        node->visit_count++;

        int chidx = node->sibling_idx;
        Node *child = node;
        node = node->parent;
        if (!node)
            break;
        // Update parent
        assert(&node->getchild(chidx) == child);
        //if (node->our_turn) {
        if (node->child_rewards) {
            // EXP3 specific: divide by prob it had of being
            // selected
            node->child_rewards[chidx] += reward / child->remem_select_prob;
            node->child_visits[chidx]++;
        }
    }
}

void Game::MCTS(double time_avail) {

    double deadline = gettime() + time_avail;

    int iterations = 0;
    while (gettime() < deadline) {
        iterations++;

        printf("\n\n[[[[[[[[[[[[[[[[ MCTS iteration %d\n", iterations);

        State state(initial_state);
        Node *node0 = tree0, *node1 = tree1;

        // Selection: Recurse down both trees at once
        //selection(state, node0, node1);

        bool expanded = false;
        while (!state.is_leaf() && !expanded) {
            // Any time one player takes an action,
            // the other observes something, so descends in their
            // tree too, though no statistics are saved. 
            
            // if (!node0->initialised() || !node1->initialised())
            //     break;
            if (!node0->initialised()) {
                printf("------- MCTS: add children to node0\n");
                node0->add_children(state, 0);
                tree0_size += node0->num_children;
                printf("Now %s\n", node0->tostr());
                // Do one more selection step, randomly
                // selecting from one of the new children, so that
                // the visit to it is recorded by backprop.
                // (Note: allowing simulation() to select a child
                // would be better)
                expanded = true;
            }
            if (!node1->initialised()) {
                printf("------- MCTS: add children to node1\n");
                node1->add_children(state, 1);
                tree1_size += node1->num_children;
                printf("Now %s\n", node1->tostr());
                expanded = true;
            }

            // updates all args
            selection_step(state, node0, node1);
        }

        printf("------- MCTS: End of selection.\n\n");
        printf("------- MCTS: node0:\n");
        node0->print_lineage();
        printf("------- MCTS: node1:\n");
        node1->print_lineage();

        reward_t reward0, reward1;

        if (!state.is_leaf()) {
            // Expansion
            // We also 

            printf("------- MCTS: Simulation:\n");
            // Can return computed/averaged rewards rather than
            // reward at a single terminal.
            state.simulation(&reward0, &reward1);

        } else {
            printf("------- MCTS: Skipping simulation.\n");
            reward0 = state.reward_for(0);
            reward1 = state.reward_for(1);
        }

        printf("------- MCTS: Leaf reached by selection/simulation:\n");
        state.print();

        printf("------- MCTS: backprop*2:\n");
        backprop(node0, reward0);
        backprop(node1, reward1);

        if (iterations == 3)   //FIXME
            break;
    }

    printf("------- Ending MCTS, tree sizes: %d, %d nodes\n", tree0_size, tree1_size);
}

Game::Game(State &init_state) : initial_state(init_state),
                                current_state(init_state) {
    _initialiseGlobals();
    tree0 = new Node(); //NULL, 0);   // no parent
    tree0->move = mtRoot;
    tree1 = new Node(); //NULL, 1);
    tree1->move = mtRoot;
    tree0_size = tree1_size = 1;
}

Game::~Game() {
    delete tree0;
    delete tree1;
}

void Game::set_state(State &state) {
    current_state = state;
}

int main() {
    _initialiseGlobals();

    int board[] = {8,26,14,30,32,1,21,12,33,25,2,18,31,27,10,3,17,23,4,6,34,29,16,22,11,35,7,15,24,19,36,5,13,9,20,28};
    int coins[2] = {100,100};
    int pot[2] = {1,1};

    State state(board,coins,pot);
    state.print();

    Game game(state);

    game.MCTS(10000.);

    // state.simulation();
    // state.print();


    return 0;
}
