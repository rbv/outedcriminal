// -*- mode:c++ -*-
#ifndef _mcts_h
#define _mcts_h

#include <stdint.h>
#include <assert.h>

struct Game;

Game *new_game();
void delete_game();


#ifdef __cplusplus

#include <algorithm>

typedef double reward_t;


typedef int board_t[6][6];

//typedef int(*board_ptr)[6][6];

enum move_type {
    mtRoot,   //only used on the root node
    mtPick,
    mtReveal,
    mtFirstBet,
    mtNextBet,
    mtLeaf,   // indi
    mtIllegal
};

#define NUM_BET_SIZES 3

// Partially visible game state
class State {

public:

    //int *board[6][6];  [row][col]
    board_t *board_ptr;

    // pending move type
    move_type mtype;  /// mtLeaf if no more moves possible
    int whose_turn;   /// 0 or 1 or -1 at leaf

    bool is_leaf() {
        assert( (whose_turn == -1) == (mtype == mtLeaf));
        return whose_turn == -1;
    }

    // each player
    int picks_done[2];

    // [0][*]: cols available, [1][*] rows available
    int cols_available[2][6];

    // Rows/cols that havn't been given assigned to the other player
    // by who, so what appears to be available for cols/rows given to self
    // [0]: rows, [1]: cols
    int public_cols_available[2][6];

    int coins[2];
    int pot[2];

    // according to seen so far
    int score[2];

    // stored by eval_pick:
    int guessed_next_scores[2];

    // player 1 only. -1 if not forced
    // We never raise, so limited to 2 rounds
    // [betting_round][betting_count][bet_size_idx]
    int forced_bet_sizes[4][2][3];

    // [who][picknum][us/them]
    // col/rowus, col/row them
    int picks[2][4][2];  // [*][3][1] is empty

    // // colus, colthem
    // int picks1[4][2];  // [3][1] is empty
    // // rowus, rowthem
    // int picks2[4][2];  // [3][1] is empty

    // board is NOT COPIED!!
    State(int *board_, int *coins, int *pot);
    void print();

    ///// sim AI

    int eval_pick(int who, int col_us, int col_them);
    void select_pick(int who);

    void simulation(reward_t &reward0, reward_t &reward1);

    ///// Moves

    void apply_pick(int who, int pick_us, int pick_them);
    void apply_reveal(int who, int reveal_col);
    void apply_bet(int who, int bet);
    void apply_raise(int who, int bet);

    void update_scores(int picknum);

    void place_bet(int who, int amnt) {
        coins[who] -= amnt;
        pot[who] += amnt;
    }
    int min_bet(int who) {
        return std::max(0, pot[who ^ 1] - pot[who]);
    }
    int max_bet(int who) {
        assert(pot[who] <= pot[who ^ 1]);
        // coins the other guy has plus the coins you owe
        return std::min(coins[who], coins[who ^ 1] + pot[who ^ 1] - pot[who]);
    }

    void next_pick_round();
    void resolve_winner();
    void resolve_bets();
    void resolve_raise(int who, int bet);

    int reward_for(int player) {
        if (coins[player] < 50)
            // penalise being short stacked
            return coins[player] - (50 - coins[player]);
        else
            return coins[player];
    }
};

class Node {
public:

    Node();
    //Node(Node *parent_);
    ~Node();

    ////// Move that got us here, or mtRoot at root
    move_type move;  // never mtLeaf!!
    union {
        uint8_t pick_us;   // col/row num
        uint8_t reveal_col; // "
        uint8_t bet;  // coins
    };
    uint8_t pick_them; // col/row num

    uint8_t move_index_other_tree;

    uint8_t sibling_idx;  // in parent
    Node *parent;

    char *tostr();
    void print_lineage();

    /*
    union {
        uint8_t pick_us_idx;   // index of column/row amongst available ones
        uint8_t reveal_col_idx; // index of reveal in range 0-2
        uint8_t bet_idx;   // index of bet size minus index of min bet
    };
    uint8_t pick_them_idx; // index of column/row amongst available ones
    */    

    //bool our_turn;

    //bool leaf;

    /////// tree stuff

    // This can only be called after checking state.is_leaf()
    // Check whether children have been added
    bool initialised() {
        return num_children > 0;
    }

    bool is_our_turn() {
        return !!child_rewards;
    }


    int visit_count;  // duplicated in parent
    double remem_select_prob; // for EXP3: what the prob of selection was

    int num_children;

    Node &getchild(int idx) { return children[idx]; }

    Node *children;

    // if our_turn == false the following are not used and are NULL.

    // These are for whichever player this is
    reward_t *child_rewards;
    // Visit count
    int *child_visits;

    Node *select_child_exp3();


private:
    void add_blank_children(int num_poss_moves, bool our_move);
    void add_pick_children_our_move(State &state, int who);
    void add_pick_children_their_move(State &state, int who);
    void add_reveal_children(State &state, int which_tree);
    void add_betting_children(State &state, int which_tree, int betsizes[3], move_type mtype);
    void add_firstbet_children(State &state, int which_tree);
    void add_nextbet_children(State &state, int which_tree);
public:
    void add_children(State &state, int which_tree);

    void update_state(State &state, int who);

};


// Previously determined this chance
class PriorNode {

};

struct Game {  // for C
public:


    //Game(int *board_, int *coins, int *pot);
    Game(State &init_state);
    ~Game();
    void set_state(State &state);

    // in seconds
    void MCTS(double time_avail);

private:

    void selection(State &state, Node *&node0, Node *&node1);

    void selection_descend_node(int who, State &state, Node *&ournode, Node *&othernode);

    void backprop(Node *node, reward_t reward);

    State initial_state;  // just to store coins and ante
    State current_state;

    // info set trees
    Node *tree0, *tree1;

    // Number of nodes in each tree
    int tree0_size, tree1_size;

};

#endif


#endif
