#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include "possible_scores.hpp"

/*
static double gettime() {
    timeval tv;
    gettimeofday(&tv, 0);
    printf("%ld %ld\n", tv.tv_sec, tv.tv_usec);
    return ((double)tv.tv_sec) + tv.tv_usec * 1e-6;
}
*/

// realize that choices for other player, and order of those choices,
// don't matter for computing possible scores of a given player
// Thus, we can save a bunch of computation time by computing them
// separately than by walking entire game tree with 500K leaves.
static void computePossibleScoresForPlayer( 
    // these are -1 if not assigned yet
    int inColumnsToPlayer[3],
    int inRowsToPlayer[3],
    char inColumnAvailable[6],
    char inRowAvailable[6],
    int *inGameBoard,
    // where we should set flags when we compute that a score for this
    // player is possible
    char *inPossibleScoreMap ) {

    for( int c=0; c<3; c++ ) {
        
        if( inColumnsToPlayer[c] == -1 ) {
        
            // set from each of available cols
            for( int a=0; a<6; a++ ) {
                
                if( inColumnAvailable[a] ) {
                    //mark as not avail

                    inColumnAvailable[a] = false;
                    
                    inColumnsToPlayer[c] = a;

                    // recurse
                    computePossibleScoresForPlayer( 
                        inColumnsToPlayer,
                        inRowsToPlayer,
                        inColumnAvailable,
                        inRowAvailable,
                        inGameBoard,
                        inPossibleScoreMap );

                    // undo what we set
                    inColumnAvailable[a] = true;
                    }
                }
            
            // undo what we set and return
            inColumnsToPlayer[c] = -1;
            return;
            }
        }
    // all cols set

    for( int r=0; r<3; r++ ) {

        if( inRowsToPlayer[r] == -1 ) {
        
            // set from each of available cols
            for( int a=0; a<6; a++ ) {
                
                if( inRowAvailable[a] ) {
                    //mark as not avail

                    inRowAvailable[a] = false;
                    
                    inRowsToPlayer[r] = a;

                    // recurse
                    computePossibleScoresForPlayer( 
                        inColumnsToPlayer,
                        inRowsToPlayer,
                        inColumnAvailable,
                        inRowAvailable,
                        inGameBoard,
                        inPossibleScoreMap );

                    // undo what we set
                    inRowAvailable[a] = true;
                    }
                }
            
            // undo what we set and return
            inRowsToPlayer[r] = -1;
            return;
            }
        }

    // both rows and cols set.

    // compute score for player from these

    // set it in inPossibleScoreMap
    
    int score = 0;
    
    for( int p=0; p<3; p++ ) {
        score += 
            inGameBoard[ inRowsToPlayer[p] * 6 + inColumnsToPlayer[p] ];
        }
    
    inPossibleScoreMap[score]++;

    return;
    }


// NOTE: destroys contents of class
void PossibleScores::computePossibleScoresFast() {
    for( int i=0; i<MAX_SCORE_RANGE; i++ ) {
        mOurPossibleScores[i] = false;
        mTheirPossibleScores[i] = false;
        
        mOurPossibleScoresFromTheirPerspective[i] = false;
        }
    
    char columnsAvail[6];
    char rowsAvail[6];
    
    memset( columnsAvail, true, 6 );
    memset( rowsAvail, true, 6 );
    
    for( int i=0; i<3; i++ ) {
        if( columnsGivenUs[i] != -1 ) {
            columnsAvail[ columnsGivenUs[i] ] = false;
            }
        if( columnsGivenThem[i] != -1 ) {
            columnsAvail[ columnsGivenThem[i] ] = false;
            }

        if( rowsGivenUs[i] != -1 ) {
            rowsAvail[ rowsGivenUs[i] ] = false;
            }
        if( rowsGivenThem[i] != -1 ) {
            rowsAvail[ rowsGivenThem[i] ] = false;
            }
        }

    // our possible scores from our perspective
    computePossibleScoresForPlayer( 
        columnsGivenUs,
        rowsGivenUs,
        columnsAvail,
        rowsAvail,
        mGameBoard,
        mOurPossibleScores );


    // their possible scores from our perspective
    computePossibleScoresForPlayer( 
        columnsGivenThem,
        rowsGivenThem,
        columnsAvail,
        rowsAvail,
        mGameBoard,
        mTheirPossibleScores );


    // if not final reveal
    if( rowsGivenThem[0] == -1 || 
        rowsGivenThem[1] == -1 ||
        rowsGivenThem[2] == -1 ) {
        
        // clear info about what we've given ourselves
        for( int i=0; i<3; i++ ) {

            if( columnsGivenUs[i] != -1 && 
                // but don't clear a pending reveal
                columnsGivenUs[i] != mRevealChoiceForUs ) {

                // clear it
                columnsAvail[ columnsGivenUs[i] ] = true;
                    
                columnsGivenUs[i] = -1;
                }
            }
        }
    
    
     // our possible scores from their perspective
    computePossibleScoresForPlayer( 
        columnsGivenUs,
        rowsGivenUs,
        columnsAvail,
        rowsAvail,
        mGameBoard,
        mOurPossibleScoresFromTheirPerspective );
    }


