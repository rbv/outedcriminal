
#define MAX_SCORE_RANGE 106

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

class PossibleScores {
public:
    void computePossibleScoresFast();
    
    //// Input
    
    // The game board is stored column-wise, [row*6 + col]
    int mGameBoard[36];

    // -1 if not known or applicable
    int columnsGivenUs[3];
    int rowsGivenUs[3];
    int columnsGivenThem[3];
    int rowsGivenThem[3];
    // The column we revealed, or -1 for none
    int mRevealChoiceForUs;
    
    //// Result (boolean arrays)
    char mOurPossibleScores[MAX_SCORE_RANGE];
    char mTheirPossibleScores[MAX_SCORE_RANGE];
    char mOurPossibleScoresFromTheirPerspective[MAX_SCORE_RANGE];
};
