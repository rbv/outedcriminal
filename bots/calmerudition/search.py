#!/usr/bin/env python
import sys
import copy
import numpy as np
import itertools
import random

import cordialminuet.util as util
from cordialminuet.util import print3
#import setup
from . import inner_circle
from . import endgame

verbose = False

class PossibleScores(object):
    def __init__(self, state):
        #with util.Timer() as timer:
        #print("PUT")
        #state.printout()

        self.ours, self.theirs, self.ours_their_pov = inner_circle.possible_scores(state)
        #print("PossibleScores in %s (round %d)" % (timer, state.round))

        # all_scores = range(len(self.theirs)) #range(1+2+3, 34+35+36 + 1)
        # nonzero_indices = list(filter(lambda x: self.theirs[x], all_scores))

        # if len(nonzero_indices) == 0:
        #     print  self.theirs
        #     print("IMPOSSIBLE POSSIBLESCORES")
        #     state.printout()
        #     assert False

    #FIXME: idiotic names
    def us_our_pov(self):
        return [idx for (idx, poss) in enumerate(self.ours) if poss]
    def us_their_pov(self):
        return [idx for (idx, poss) in enumerate(self.ours_their_pov) if poss]
    def them(self):
        return [idx for (idx, poss) in enumerate(self.theirs) if poss]

    def possibilities(self):
        "Pair of lists of possible scores"
        return [self.us_their_pov(), self.them()]

    def __str__(self):
        "Return numerical ranges"
        def nonzero_intervals(x):
            "Return a string like '5,7-8' describing the nonzero intervals of a sequence"
            ret = []
            start = -1
            for idx in range(len(x)):
                if start == -1 and x[idx]:
                    start = idx
                is_end = (idx == len(x) - 1) or not x[idx + 1]
                if start > 0 and is_end:
                    if start == idx:
                        ret.append(str(start))
                    else:
                        ret.append("%d-%d" % (start, idx))
                    start = -1
            return ','.join(ret)
        xx = (nonzero_intervals(x) for x in (self.ours, self.ours_their_pov, self.theirs))
        return "PossibleScores(ours=%s  ours(their_pov)=%s  theirs=%s)" % tuple(xx)

    def print_graphs(self):
        "Print graphs like in the client"
        def plot_graph(arr):
            ret = ""
            all_scores = range(len(arr)) #range(1+2+3, 34+35+36 + 1)
            nonzero_indices = list(filter(lambda x: arr[x], all_scores))
            ret = "%3d-%3d " % (min(nonzero_indices), max(nonzero_indices))
            return ret + ''.join('+' if arr[idx] else ' ' for idx in all_scores)
        print("ours     :" + plot_graph(self.ours))
        print("ours(PoV):" + plot_graph(self.ours_their_pov))
        print("theirs   :" + plot_graph(self.theirs))

def setup_endgame(state, score_probs, ante, betsizes):
    pscores = PossibleScores(state)
    scores = list(pscores.possibilities())
    if len(scores[0]) == 1:
        s = scores[0][0]
        scores[0] = [s,s]
    if len(scores[1]) == 1:
        s = scores[1][0]
        scores[1] = [s,s]
    game = endgame.BasicEndGame(scores, score_probs, ante)
    if betsizes == None:
        value, quad, betsizes = endgame.find_optimal_betsizes(game)
    game.set_betsizes(betsizes)
    us_their_pov, them = scores
    if pscores.ours[us_their_pov[0]]:
        score_idx = 0
    elif pscores.ours[us_their_pov[1]]:
        score_idx = 1
    else:
        print3("BAD SCORE")
        assert False
    return game, score_idx

def process_endgame(state, score_probs, ante, betsizes):
    game, score_idx = setup_endgame(state, score_probs, ante, betsizes)
    endgame_advice(game, score_idx)

def manual_endgame(scores, score_idx0, p1, p2, ante, betsizes):
    "Test with gambit's normal form games"
    score_probs = [[1 - p1, p1], [1 - p2, p2]]

    game = endgame.BasicEndGame(scores, score_probs, ante)
    if betsizes == None:
        value, quad, betsizes = endgame.find_optimal_betsizes(game)
    game.set_betsizes(betsizes)

    game = endgame.BasicEndGame(scores, score_probs, ante, betsizes)
    #game.save_gambit_Game('tempgame.nfg')

    endgame_advice(game, score_idx0)

def endgame_advice(game, score_idx, verbose = True):
    """
    game: endgame.BasicEndGame
    score_idx:  0 or 1, the score we have
    """
    if verbose:
        print3()
        print3("possible scores = ", game.scores, "probabilities =", game.score_probs)
    strat0, strat1, quadrant_payoffs, total_value = game.solve(printout = verbose, force_lp = False)
    cum = np.cumsum(strat0[score_idx])
    rnd = random.uniform(0., 1.)
    # FIXME: picked strategy might not be one where we picked score_idx
    #  -> What? I probably meant that this isn't correct for working out
    #     whether to raise
    for idx, val in enumerate(cum):
        if rnd <= val:
            break
    if verbose:
        print3("Uniform [0,1]:", rnd, "index:", idx)
        print3("Behaviour:", game.behaviours[0][idx])
    return game.behaviours[0][idx]


def manual_tree_endgame(scores, score_idx0, p1, p2, ante, betsizes):
    "Test of gambit's slow tree form"
    score_probs = [[1 - p1, p1], [1 - p2, p2]]
    game = endgame.BasicEndGame(scores, score_probs, ante, betsizes)
    game.save_gambit_Game('tempgame.nfg')
    tree = game.form_and_solve_gambit_tree_Game()
    # contents = tree.write()
    # with open('tempgame.efg', "w") as fil:
    #     fil.write(contents)

    endgame_advice(game, score_idx0)
        
def process(state):
    if state.state == '1stbet':# 'pick':
        #print("selecting")
        scores = PossibleScores(state)
        #print(scores)
        scores.print_graphs()
    if state.round == 4:
        score_probs = [[0.5, 0.5], [0.5, 0.5]]
        #return
        if state.state == '1stbet':
            assert state.pot[0] == state.pot[1]
            ante = state.pot[0]
            process_endgame(state, score_probs, ante, None)
        elif state.state == 'bet':
            ty, b0, b1 = state.history[-1]
            assert ty == 'bet'
            if b0 is None:
                ante = state.pot[1] - b1
            else:
                ante = state.pot[0] - b0
            print3("***Guessed ante for this round is", ante)
            betsizes = [[b0], [b1]]
            process_endgame(state, score_probs, ante, betsizes)
    elif state.round >= 1:
        if state.pot[0] == state.pot[1]:
            tree = SearchTree(state)
            rn = state.round
            value = tree.walk(state.round)
            print3("searched for", rn)
            print3("remember_best", tree.remember_best, "value", value, "cnt", tree.cnt1, tree.cnt2)
            return
            

def nonzero(array):
    return [idx for idx in range(len(array)) if array[idx]]

def result_chances(probs_us, probs_them):
    """probs_us/them are pmf arrays of length 106.
    Return a tuple of chances (win, draw, lose) which sums to 1.
    """
    try:
        win, draw, lose = 0, 0, 0
        assert len(probs_us) == len(probs_them) == 106
        assert np.allclose(probs_us.sum(), 1.)
        assert np.allclose(probs_them.sum(), 1.)

        cum_probs_us = np.cumsum(probs_us)
        draw = (probs_us * probs_them).sum()
        # for idx in range(1, 106):
        #     lose += probs_them[idx] * cum_probs_us[idx - 1]
        lose = np.array(probs_them[1:106]).dot(cum_probs_us[0:105])
        win = 1. - draw - lose
        assert min(win, draw, lose) >= -1e8
        return win, draw, lose
    except:
        print("probs_us")
        print(probs_us)
        print("probs_them")
        print(probs_them)
        print((win, draw, lose))
        raise


def format_chances(chances):
    return "win:%.3f draw:%.3f lose:%.3f" % chances

def calc_chances(possible_scores, secret):
    "Calc (win,draw,lose). Secret: use our POV rather than theirs"
    if verbose:
        print("Possible_scores:")
        print(possible_scores)

    if secret:
        probs_us = np.array(possible_scores.ours, dtype=float)
    else:
        probs_us = np.array(possible_scores.ours_their_pov, dtype=float)
    probs_us /= probs_us.sum()
    probs_them = np.array(possible_scores.theirs, dtype=float)
    probs_them /= probs_them.sum()
    return result_chances(probs_us, probs_them)

def score_with_win_chance(ours_their_pov, win_chance):
    "Compute the minimum score that gives a win chance at least 'win_chance'"
    cum_probs_us = np.cumsum(ours_their_pov).astype(float)
    cum_probs_us /= cum_probs_us[-1]
    ret = np.searchsorted(cum_probs_us, win_chance)
    return min(ret, 105)
   

def possible_bet_sequences(bet1, bet2, ante):
    """Return all possible sequences of moves in an endgame where the players
    may only bet b_i or 0, followed by matching the opponent or folding.

    Returns (move_lists, folded, total_bet_components).
    move_lists is a length N list, of lists of 1 or more pairs of bet amounts for each player.
    <s>potsizes is the corresponding list of potsizes (equal to sums plus)</s>
    folded is an [N] array containing the number 1,2 of the player who folded or -1 for none.
    total_bet_components is a [N][2] ndarray, where [:][0,1,2] is multiples of the ante, bet1,
    or bet2 respectively each player has in the pot. Each is 0 or 1.
    """
    move_lists = []
    folded = []
    for b1_1 in (0, bet1):
        for b2_1 in (0, bet2):
            fold = -1
            if b1_1 > b2_1:
                for b2_2 in (0, b1_1 - b2_2):
                    if b2_2 == 0:
                        fold = 2
                        
    return ret

PLAYER0 = 0
PLAYER1 = 1

def tribute(pot):
    "pot: length 2 array of pot contents"
    #return 0    #####FIXME
    if min(pot) <= 2:
        return 0
    return 1

class SearchTree:
    def __init__(self, state):
        self.state = copy.deepcopy(state)
        self.state.pot = np.array(self.state.pot)
        self.state.coins = np.array(self.state.coins)
        self.available_picks = [[True] * 6, [True] * 6]

        for us, them in self.state.picks1[:3]:
            self.available_picks[0][us] = False
            self.available_picks[0][them] = False
        for us, them in self.state.picks2[:3]:
            self.available_picks[1][them] = False
            if us != '?':
                self.available_picks[1][us] = False

        self.cached = endgame.load_endgame_cache()
        self.score_total = [0, 0]
        self.cnt1 = 0
        self.cnt2 = 0
        self.score_probs = [[0.5,0.5],[0.5,0.5]]
        self.replaypicks = [state.picks1[:], state.picks2[:]]
        ## Guess picks
        # for idx, (us, them) in enumerate(self.state.picks2[:3]):
        #     if us == '?':
        #         guessed_row = self.available_picks[1].index(True)
        #         self.state.picks2[idx][0] = guessed_row
        #         self.available_picks[1][guessed_row] = False
        # print("Guessed player 2 picks:", self.state.picks2)

    def walk(self, want_which):
        self.state.picks1 = []
        self.state.picks2 = []
        self.want_which = want_which
        print("SearchTree.walk want_which=%d" % want_which)
        val = self.pick(0, 0)
        return val
        #round = self.state.round
        round = 0
        if round <= 2:
            self.pick(round, 0)
        else:
            self.reveal(0)

    def update_scores(self):
        "<s>Calling code responsible for updating self.score_total"
        old_picks2 = self.state.picks2
        new_picks2 = self.state.picks2[:]
        #old_picks1 = self.state.picks1
        #new_picks1 = self.state.picks1[:]
        #revealed_col = old_picks2[3][0]
        revealed_row = old_picks2[3][0]
        # hide some
        #print3("oriignal",  self.state.picks2)
        for idx, pair in enumerate(self.state.picks2[:3]):
            if pair[0] != revealed_row:
                #pair[0] = '?'
                new_picks2[idx] = ['?', pair[1]]
        #self.state.picks1 = new_picks1
        self.state.picks2 = new_picks2
        #print3("obscured",  self.state.picks2)
        pscores = PossibleScores(self.state)
        self.poss_scores = pscores.possibilities()  #[2][2]
        if len(self.poss_scores[0]) == 1:
            a = self.poss_scores[0][0]
            self.poss_scores[0] = [a,a]
        if len(self.poss_scores[1]) == 1:
            a = self.poss_scores[1][0]
            self.poss_scores[1] = [a,a]
        #self.state.picks1 = old_picks1
        self.state.picks2 = old_picks2
        #print3("ybobscured",  self.state.picks2)
        self.score_total = [self.state.score(0), self.state.score(1)]

    def leaf(self, score_probs = None):
        "Returns value"
        self.cnt1 += 1
        score_probs = self.score_probs
        self.update_scores()
        scores = self.poss_scores
        pot = self.state.pot[0]

        # FIXME
        key = inner_circle.endgame_type(scores, score_probs[0][1], score_probs[1][1], pot)
        if key not in self.cached:
            print3("eee",scores, score_probs[0][1], score_probs[1][1], pot)
            return 0.
        (value, quad, bets) = self.cached[key]
                
        low, high = self.poss_scores[0]
        if self.score_total[0] == low:
            ours = 0
        elif self.score_total[0] == high:
            ours = 1
        else:
            print3("BAD SCORE")
        low, high = self.poss_scores[1]
        if self.score_total[1] == low:
            theirs = 0
        elif self.score_total[1] == high:
            theirs = 1
        else:
            print3("BAD SCORE2")
        #self.score_indices = [ours, theirs]

        #[ours, theirs] = self.score_indices
        ret=  quad[ours, theirs] * pot / 10.
        if self.want_which == 3:
            print3("    leaf", self.state.picks1, self.state.picks2,  "scores", scores, "pot", pot, "value", ret)
        return ret

    def get_and_encode_information_set(self, who):
        """Returns (picks, a hashable encoding of the information set
        visible to 'who'. Bets not supported"""

        # 3 bits times 9 picks  (col_us, col_them, row_us)
        # plus 3 bits time 2 reveals (col_us, row_them)

    def opponent_pick(self, pick):
        """pick has already been added to self.state"""

        key = self.encode_information_set(1)
        self.opponent_model_cache

        weight = 1.
        if round >= 1:
            pscores = PossibleScores(self.state)
            # poss_theirs = [idx for (idx, poss) in enumerate(pscores.theirs) if poss]
            # poss_ours = [idx for (idx, poss) in enumerate(pscores.ours_their_pov) if poss]
            # #print3("possibles",poss)
            # num1 = float(pscores.theirs)
            # num2 = float(pscores.ours_their_pov)
            # weight = 18.5*3 + (sum(poss_theirs)/num1 - sum(poss_ours)/num2)
            if False:
                weight = np.array(pscores.theirs).mean() - np.array(pscores.ours_their_pov).mean()
            else:
                chances = calc_chances(pscores, secret=True)
                win_chance, draw_chance, lose_chance = chances
                score = win_chance + draw_chance / 2
                weight = score ** 2

        
    def pick(self, round, who):
        "Select columns/rows"
        available = nonzero(self.available_picks[who])
        # Replay existing pick
        require = None
        self.cnt2 += 1
        if who == 0:
            if len(self.replaypicks[who]) > round:
                # don't change self.available_picks
                # Nothing to do
                require = self.replaypicks[who][round]
                #return self.pick(round, 1)
            picks = self.state.picks1
        elif who == 1:
            if len(self.replaypicks[who]) > round:
                # don't change self.available_picks
                require = self.replaypicks[who][round]

            picks = self.state.picks2
        total = [0.]
        total_weight =[ 0.]
        bestval = [None]   # nonlocal trick
        #print3("round", round, "who", who, "avail", available, "require=", require, "picks so far", picks)
        bestpick = [None]
        values = []
        weights = []



        # FIXME:
        # need to average over information sets for us,
        # and have a cache for guessed oppponent actions (which should not
        # depend on our hidden moves when computed). Beware picks1

        def inner():
            """Consider a single option. us and them are implicit arguments"""
            remember_available_picks = self.available_picks[who][:]
            self.available_picks[who][us] = False
            self.available_picks[who][them] = False
            if who == 0: #PLAYER0:
                self.state.picks1.append([us, them])
                val = self.pick(round, PLAYER1)
                if val is not None and (bestval[0] is None or val > bestval[0]):
                    bestval[0] = val
                    bestpick[0] = self.state.picks1[:], self.state.picks2[:]
                self.state.picks1.pop()
            else:
                #self.state.history.append(('move',))
                self.state.picks2.append([us, them])

                if round + 1 <= 2:
                    val = self.pick(round + 1, 0)
                else:
                    val = self.reveal(0)
                #print(".")

                weight = self.opponent_pick([us,them])

                values.append(val)
                weights.append(weight)

                self.state.picks2.pop()
                #self.state.history.pop()

            self.available_picks[who] = remember_available_picks

            # self.available_picks[who][us] = True
            # self.available_picks[who][them] = True


        if require is not None:
            them = require[1]
            if require[0] == '?':
                for us in available:
                    inner()
            else:
                us = require[0]
                inner()
        else:
            for us, them in itertools.permutations(available, 2):
                inner()

        if who == 1:
            #total = sum(values)
            #total_weight = sum(weights)
            #if total_weight > 0:
            weights = np.array(weights)
            #weights += 3*4 - weights.min() 
            return np.array(values).dot(weights) / (weights.sum() + 1e-7)
        else:
            bestval = bestval[0]
            bestpick = bestpick[0]
            total = total[0]
            total_weight = total_weight[0]

            if self.want_which == round and who == 0:
                self.remember_best = bestpick

            #print3("round", round, "who", who, "avail", available, "require=", require, "picks so far", picks, "VALUE", bestval, bestpick)
            return bestval

    def reveal(self, who):
        """
        Pessimistically assume opponent knows what we have and does best response
        """
        if who == 0:
            picks = self.state.picks1
        else:
            picks = self.state.picks2
        bestval = None
        # if len(picks) >= 4:
        #     # replay
        # we never replay

        #print3("reveal round", round, "who", who, "picks so far", picks)
        
        for idx in range(3):
            # From the player's perspective this is their column
            revealed = picks[idx][0]
            #print3("revealed:", revealed, "idx", idx)

            if who == PLAYER0:
                # Partially construct the 'reveal' history step
                #self.state.history.append(('reveal', [revealed, self.state.picks2[idx][1]], None))
                self.state.picks1.append([revealed, None])
                val = self.reveal(PLAYER1)
                
                if val is not None and (bestval is None or val > bestval):
                    bestval = val
                    bestpick = self.state.picks1[:], self.state.picks2[:]

                self.state.picks1.pop()
                #self.state.history.pop()
            else:
                # This is totally wrong, opponent doesn't know what we have
                #self.state.history[-1][2] = [self.state.picks1[idx][1], revealed]
                self.state.picks2.append([revealed, None])

                val = self.leaf()
                if val is not None and (bestval is None or val < bestval):
                    bestval = val
                    bestpick = self.state.picks1[:], self.state.picks2[:]

                self.state.picks2.pop()
                #print "after leaf:", self.state.picks2

        if self.want_which == 3:
            self.remember_best = bestpick
        if self.want_which == 3:
            print3("  reveal(%d)" % who, self.state.picks1, self.state.picks2, "value", bestval, "best", bestpick)
        return bestval

class GameTree:
    """Walks the game tree. Was intended to allow callback at each
    leaf. Not used for anything"""

    betsizes = [0, 1, 2]
    max_betting_rounds = 3  # initial bet, raise, and match
    
    def __init__(self, state):
        self.state = copy.deepcopy(state)
        self.state.pot = np.array(self.state.pot)
        self.state.coins = np.array(self.state.coins)
        self.available_picks = [[True] * 6, [True] * 6]

        for us, them in self.state.picks1[:3]:
            self.available_picks[0][us] = False
            self.available_picks[0][them] = False
        for us, them in self.state.picks2[:3]:
            self.available_picks[1][them] = False
            if us != '?':
                self.available_picks[1][us] = False

        ## Guess picks
        for idx, (us, them) in enumerate(self.state.picks2[:3]):
            if us == '?':
                guessed_row = self.available_picks[1].index(True)
                self.state.picks2[idx][0] = guessed_row
                self.available_picks[1][guessed_row] = False
        print("Guessed player 2 picks:", self.state.picks2)


    def get_betsizes(self, at_least, maxbet):
        "Get a range of possible bet sizes >= at_least"
        ret = [x + at_least for x in self.betsizes]
        ret = [x for x in ret if x <= maxbet]
        if ret[-1] < int(maxbet / 2):
            ret.append(int(maxbet / 2))
        if ret[-1] < maxbet:
            ret.append(maxbet)
        return ret

    def at_leaves(self, callback):
        self.callback = callback

    def _gameover(self, winner):
        "winner is 0 or 1, or -1 for draw"
        coins_afterwards = self.state.coins.copy()
        pot = self.state.pot.sum()
        if winner == -1:
            coins_afterwards += pot / 2
            value = 0
        else:
            coins_afterwards[winner] += pot - tribute(self.state.pot)
            value = self.state.pot[winner ^ 1]
            if winner == 1:
                value *= -1
        # We assume that if we stay in the game, we will continue playing until we've won
        # about a third of what the opponent has left, provided we have enough left to play
        total_value = coins_afterwards[0] + min(coins_afterwards[0] * 0.67, coins_afterwards[1] / 3)
        #print(self.state.history, "Value=", value, "value2=", value2, "Total_value=", total_value, "coins_after=", coins_afterwards)
        print(self.state.history, "Value=%d Total_value=%d" % (coins_afterwards[0], total_value), "coins_after=", coins_afterwards)

    def walk(self):
        self._next_action()

    # simultaneous actions linearised into us, them

    def _next_action(self):
        "Call whatever should happen next"
        assert len(self.state.picks1) == len(self.state.picks2)
        last_action = ''
        if len(self.state.history):
            last_action = self.state.history[-1][0]
        if last_action == 'flop':
            return
        if (last_action != 'bet') or self.state.pot[0] != self.state.pot[1]:
            print("_next_action: bet")
            self._bet()
        else:
            if len(self.state.picks1) == 4:
                print("_next_action: flop")
                assert self.state.pot[0] == self.state.pot[1]
                score0 = self.state.score(PLAYER0)
                score1 = self.state.score(PLAYER1)
                if score0 < score1:
                    winner = 1
                elif score0 == score1:
                    winner = -1
                else:
                    winner = 0
                self._gameover(winner)
            elif len(self.state.picks1) == 3:
                print("_next_action: reveal")
                self._reveal(PLAYER0)
            else:
                print("_next_action: pick")
                self._pick(PLAYER0)
        
        
    def _pick(self, who):
        "Pick columns/rows"
        available = nonzero(self.available_picks[who][idx])
        for us, them in itertools.permutations(available, 2):
            self.available_picks[who][us] = False
            self.available_picks[who][them] = False
            if who == PLAYER0:
                self.state.picks1.append((us, them))
                self._pick(PLAYER1)
                self.state.picks1.pop()
            else:
                self.state.history.append(('pick',))
                self.state.picks2.append((us, them))
                self._bet()
                self.state.picks2.pop()
                self.state.history.pop()
            self.available_picks[who][us] = True
            self.available_picks[who][them] = True

    def _reveal(self, who):
        if who == 0:
            picks = self.state.picks1
        else:
            picks = self.state.picks2
        for idx in range(3):
            revealed = picks[idx][0]
            assert revealed != '?'
            if who == PLAYER0:
                # Partially construct the 'reveal' history step
                self.state.history.append(('reveal', [revealed, self.state.picks2[idx][1]], None))
                self.state.picks1.append([revealed, None])
                self._reveal(PLAYER1)
                self.state.picks1.pop()
                self.state.history.pop()
            else:
                self.state.history[-1][2] = [self.state.picks1[idx][1], revealed]
                self.state.picks1.append([revealed, None])
                self._bet()
                self.state.picks1.pop()

    def _bet(self, betting_round = 0):
        print("BET ", betting_round)
        """Either both players bet (first round, or one replies).
        betting_round is the number of previous bets placed"""
        if self.state.pot[0] == self.state.pot[1]:
            #print(" EVEN")
            maxbet = min(self.state.coins[0], self.state.coins[1])
            betsizes = self.get_betsizes(0, maxbet)
            self.state.state = '1stbet'
            for bet0 in betsizes:
                for bet1 in betsizes:
                    self.state.pot += [bet0, bet1]
                    self.state.coins -= [bet0, bet1]
                    self.state.history.append(('bet', bet0, bet1))

                    if self.state.pot[0] != self.state.pot[1]:
                        self._bet(betting_round + 1)
                    else:
                        self._next_action()

                    self.state.pot -= [bet0, bet1]
                    self.state.coins += [bet0, bet1]
                    self.state.history.pop()

            return

        for who_replies in [0, 1]:
            self.state.state = 'bet'
            other = who_replies ^ 1
            shortby = self.state.pot[other] - self.state.pot[who_replies]
            if shortby > 0:
                #self.state.printout()
                #print(" %d short by %d" % (who_replies, shortby))

                if betting_round + 1 < self.max_betting_rounds:
                    # Not the last round, so can raise
                    maxbet = min(self.state.coins[who_replies], shortby + self.state.coins[other])
                    raises = self.get_betsizes(shortby, maxbet)
                else:
                    # Match only
                    raises = [shortby]
                # Either fold or bet
                self._fold(who_replies)
                for bet in raises:
                    self.state.pot[who_replies] += bet
                    self.state.coins[who_replies] -= bet
                    if who_replies == PLAYER0:
                        self.state.history.append(('bet', bet, None))
                    else:
                        self.state.history.append(('bet', None, bet))

                    if self.state.pot[0] != self.state.pot[1]:
                        self._bet(betting_round + 1)
                    else:
                        self._next_action()

                    self.state.pot[who_replies] -= bet
                    self.state.coins[who_replies] += bet
                    self.state.history.pop()
                return
        assert False

    def _fold(self, who):
        self.state.history.append(('fold', who))
        self._gameover(who ^ 1)
        self.state.history.pop()

class FixedGameState:
    def score(self, who):
        return self._scores[who]
    
class _old_Endgame(GameTree):
    """Starting from betting before the reveal pick."""
    def __init__(self, state, scores): #, probs_us, probs_them):

        self.state = state
        super(self, EndGame).__init__(state)
        self.state.score = FixedGameState.score
        self.state._scores = scores
        # self.scores = PossibleScores(state)
        # self.prior_probs_us = probs_us
        # self.prior_probs_them = probs_them

    # def walk(self):
    #     pass

def _old_experiment_endgame():
    state = GameState()
    state.board = None
    state.coins = [99, 99]
    state.pot = [1, 1]
    state.picks1 = [[], [], [], []]
    state.picks2 = [[], [], [], []]
    endgame = EndGame(state, [])


# When deciding which columns to reveal, and when computing posterior
# probabilities of having certain scores, the available bet sizes intuitively shouldn't
# matter. However variable bet sizes later do matter when deciding whether to match now.

# After reveal, can ignore 4 possibilities, and compute new posteriors given
# prior, and computed chance of reveal possibilities for each of those two remaining
# possibilities.

class OppModel:
    fold_multiplier = 1.
    bluff_multiplier = 1.
    risk_aversion = 0.  # Negative means
    adaption = 0.2   # The amount that they adjust according to our behaviour (our computed opponent model of ourselves)
      # The chance that the opponent will suddenly change strategy and assume we will assume the old one (fixed).
      # E.g. outrageous bluffs
    running_ploy = 0.25
      # How obvious the opponent is being (eg picking row with 36 4 times in a row),
      # which increases chance that it's a ploy.
      # chance = running_ploy * (1 + obviousness)
    obviousness = 0.


    # Suppose we have a prior P on the opponent score s \in S and they perform an
    # action x \in X (eg fold) in situation y \in Y (eg we just bet 20 in final betting, with a pot of...).

    #   Bayesian strategy multiplier approach
    # First we compute some kind of "mean" or "optimal" behavioural strategy B_base(x|s,y)
    # for situation y.
    # Then we examine situation y and decide which multipliers apply (and how much)
    # according to opponent model O
    # to give our estimate B_est(x|s,y) = B_base(x|s,y) mult(x|s,y,O) / (\sum_{x' \in X} B_base(x'|s,y) mult(x'|s,y,O))
    # Next after observing action x we want to update O and

    #   Bayesian strategy mixture approach
    # As above, except that instead of B_base we have various pure strategies
    #
    # For each pure strategy, and for all rounds so far, track whether the
    # behaviour agrees or disagrees with that strategy.
    # Then compute:
    #  -k that maximises evidence in the last k rounds (changed strategy k rounds ago)
    #    (maybe also track our win rate over time, to correlate
    #  -total evidence over all rounds (is following a pure strategy)
    #  -total evidence in rounds where it's positive (mixed strategy)

    #   Betting:
    # Start with Kelly criterion where we have a certain adjustable virtual total bankroll
    # <= actual.
    # In order to decide whether to go all in, we assume that our model of
    # the opponent is improving, and thus that the value of remaining in the game is
    # better than even, so we don't want to end the match unless we have a good chance.
    # R=min(coins[0], coins[1])

    
    #   Entire game flow:
    # For pick moves, we use our opponent model to assign probabilities to
    # opponent moves at each branch, given info visible to them.
    # At each leaf of this 6! * choose(6, 3) = 86400 tree, we have estimated
    # probability to reach this node.
    # Then for each of our possible hidden picks (6 options total)
    # we can play the endgame given pot on entering the endgame,
    # and compute expected value.
    # Divide pot size on reaching endgame into four earlygame (first 2 bet rounds) betting behaviours:
    # -no bets
    # -small bet 1-4
    # -medium bet 5-15
    # -big bet, (Kelly) fraction of max
    #
    # When searching the (early) tree, we assume that the opponent doesn't
    # search. But rather than maximising our expected payoff according to
    # estimates of opponent moves (which will tend towards assuming they behave randomly), we can try to interpolate between expectation and
    # risk adversion (minimax) by doing node_value = 0.5(expected(node_value) + probable(node_value))
    # where probable() is calculated using is the estimated most likely strategy
    # (problem: that might be a mixed strategy)

    #   Caching endgame:
    # Value is based on:
    #  -13 distinct possible_score configurations * 4 actual scores = 52,
    #  -prior probability each player has for the other, and thinks the other has for them (4 distibutions in total...)
    # Get priors from:
    # whether each player has {0:hasn't bet or had to match anything, 1:low bets or only matched, 2:medium bets or matched big bets, 3:bet or raised aggressively}.  Maybe 4: went all in
    #   possible combinations of those are: [0,0], [1,1], [1,2], [2,1], [2,2], [2,3], [3,2], [3,3]
    # potsize: simplifly and deterministically set by betting behaviour: {0:1, 1:3, 2:6, 3:12}
    #
    # Need to determine each player's estimate of the prior probability of each player's
    # score given the bets so far.
    # That depends on the reveal immediately prior.
    # For simplicity, assume the opponent's prior before reveal is based only on aggressiveness of betting.

    #   Early game betting modelling:
    # To decide how to bet in early game, or determine amout of information
    # in the betting (how to compute priors based on bets placed), use a simplified game consisting of:
    # -player recieves a number with uniform distrib
    # -betting
    # -repeated at most TWICE, not TRICE (for simplicity)
    # -highest sum wins (probabilty of winning is proportional to recieved numbers)
    # However on first (next) turn we know that the numbers aren't uniformly drawn,
    # since we get to set to 6 numbers. But ignore that.

    # Idea 1 : we do simple betting modelling as above, which:
    # -(?) at the beginning of the game can be used to estimate betting strategies to
    #  use at each node, allowing us to inform endgame nodes. Tree ~ 512k * 9 * 3**3
    # -at the end of the game the betting that occurs lets us form reasonable priors
    #  for each player for the possible scores. For us, we combine the
    #  the opponent modelling with the betting using Bayes to estimate prior for opponent,
    #  while the estimate of their estimate of us is only based on betting.


    # Idea 2: bet sizes, and determining what a human thinks of them, is too difficult.
    # Instead, fix bet sizes with some randomness to confuse opponent, use Kelly to compute that,
    # and compute only bet/don't bet by bot. (Adjust by human)
    # Then can do tree search which includes betting and uses cached endgames with simple
    # "number of bets" parameter ???

    # Roadmap:
    # - first do separately:
    #   -endgame (betting)
    #   -reveal (Bayes calc fro priors)
    #   -early game tree search without betting, and simple heuristic for endgame value,
    #    and no opponent modelling
    # - add opp modelling to game tree search
    # - add simple betting to game tree
    # - (Easy) compute Kelly criterion
    
