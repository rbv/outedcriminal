import random
import itertools
import numpy as np

from cordialminuet.util import print3
from calmerudition import search, endgame

import bots.simplebots as simplebots


from player import GamePlayer

#import calmerudition.inner_circle as inner_circle

DefBettingClass = simplebots.DefaultBetting
#DefBettingClass = simplebots.BasicBetting
DefBettingClass = simplebots.LessSimpleBetting


class EndgameBetting(DefBettingClass):

    opp_bets = 0

    def start_round(self):
        self.opp_bets = 0

    def endgame_bet(self, min_bet, max_bet):
        "Called for last round only"

        if max_bet == 0:
            return 0

        if min_bet == 0:
            # First bet
            thepot = self.state.pot[0] # both equal
            self.initial_pot = thepot
            betinc = min(thepot, 30)
            betsizes = [int(betinc * .5), int(betinc), int(betinc * 1.6)]
            betsizes = [max(1, min(max_bet, x)) for x in betsizes]
            betsizes0 = betsizes1 = betsizes
            start_pot = thepot

        else:
            pot = self.state.pot
            # The following is quite wrong game-theoretically

            # Have to compensate for actual bets
            # bet0 = self.state.pot[0] - self.initial_pot
            # bet1 = self.state.pot[1] - self.initial_pot

            # We pretend that we didn't bet, and pot already
            # had whatever we put in it, so that we
            # can allow the possibility that we now raise
            # FIXME: this is a limitation of BasicEndGame not
            # allowing raising
            start_pot = pot[0]

            bet1 = pot[1] - pot[0]
            assert pot[1] > pot[0]

            betsizes0 = simplebots.pick_bet_sizes(self.state, min_bet, max_bet)

            # They could have bet higher...
            betsizes1 = [bet1, min(max_bet, int(bet1 * 1.5))]

            #betsizes = [min(max_bet, x) for x in betsizes]

        # remove dups
        betsizes0 = list(set(betsizes0))
        betsizes1 = list(set(betsizes1))
        betsizes = [betsizes0, betsizes1]


        # Chance of each player to have the higher score
        p1 = 0.40
        p2 = 0.40
        p2 = min(0.7, p2 + self.opp_bets * 0.1)
        score_probs = [[1 - p1, p1], [1 - p2, p2]]

        #game = endgame.BasicEndGame(scores, score_probs, start_pot)
        game, score_idx = search.setup_endgame(self.state, score_probs, start_pot, betsizes)

        behaviour = search.endgame_advice(game, score_idx, verbose = True)
        print3("betsizes:", betsizes, "score_idx:",  score_idx)
        print3("selected behaviour: ", behaviour)
        our_bet = behaviour[1]
        if our_bet < min_bet:
            # Ok, look at response. Opponent bet bet1, which is betindex 0
            response = behaviour[2 + 0]
            if response == endgame.bhFOLD:
                our_bet = 0
            else:
                our_bet = response
                assert our_bet is not None
                assert our_bet >= min_bet
        return our_bet
        

    def bet_or_fold(self, min_bet, max_bet):
        if min_bet > 0: self.opp_bets += 1
        if len(self.state.picks1) == 4:
            print("Using endgame_bet")
            ret = self.endgame_bet(min_bet, max_bet)
        else:
            #print("fallback to default, round %d" % self.state.round)
            ret = DefBettingClass.bet_or_fold(self, min_bet, max_bet)
        if ret > 0:
            self.opp_bets += 0.5
        return ret

    def first_bet(self, max_bet):
        return self.bet_or_fold(0, max_bet)


class EndgameD3Bot(EndgameBetting, simplebots.SensibleReveal, simplebots.D3Picking):
    pass


class Walker(simplebots.D2Picking):


    def _picker(self):
        state = self.state
        tree = search.SearchTree(state)
        #rn = state.round
        rn = len(state.picks1)
        value = tree.walk(rn)
        print "searched for round", rn
        print3("remember_best", tree.remember_best, "value", value, "cnt", tree.cnt1, tree.cnt2)
        return tree.remember_best[0][rn]


    def pick(self):
        "Select and return (col, row)"
        if len(self.state.picks1) == 0:
            return simplebots.D2Picking.pick(self)
        return self._picker()

    def reveal(self):
        "Return col to reveal"
        # col = self.state.picks1[0][0]
        # row = self.state.picks2[0][1]
        # return col, row
        return self._picker()

class EndgameWalkBot(EndgameBetting, Walker):
    pass



if __name__ == '__main__':
    scores = [ [50, 100], [75, 101]]
    betsizes = [5] #, 10, 17]
    search.manual_endgame(scores, 0, 0.9, 0.5, 10, [betsizes, betsizes])
