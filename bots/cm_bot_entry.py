"""
Invoked from CM
"""

import simplebots
import ce

def get_bot(botnum, stakes):
    """Returns new bot instance, for a new game.
    stakes: in dollars"""
    bot = simplebots.SRLessSimpleR3Bot()
    #bot = ce.EndgameWalkBot()
    bot.start_game(stakes)
    return bot

