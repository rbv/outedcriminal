#!/usr/bin/python

import os
import numpy as np




# row, column. player1 pick columns, player2 rows

def scores(square, move1, move2):
    """
    Moves are a pair of column numbers (start at 0) for each player,
    Returns pair of scores
    """
    player1 = square[move2[1], move1[0]]
    player2 = square[move2[0], move1[1]]
    return player1, player2

#print(scores(square, (5,2), (0,1)))

def get_player_probabilities(square, col_probs, row_probs):
    """
    Return (6,6) array with chance of player getting each cell
    """
    return (row_probs[None:] * col_probs[:None])

def outcome(square, strat1, strat2):
    """
    Strategies are a (6,6) array for each player,
    with cell [x,y] giving chance of playing mine=x, theirs=y
    Returns chance of win for player 1.
    """

    # Doesn't matter what player 2's square is, so reduce each player's strat
    # to array of 6 probs
    player1 = get_player_payouts(square, strat1.sum(axis=1), strat2.sum(axis=0))
    player2 = get_player_payouts(square, strat1.sum(axis=0), strat2.sum(axis=1))
    print(player1)

