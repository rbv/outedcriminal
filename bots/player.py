import round
from calmerudition.search import PossibleScores


class GamePlayer():
    state = None
    stakes = 0.
    easy_mode = False
    def start_game(self, stakes = 0.):
        self.stakes = stakes
        self.easy_mode = (stakes > 0 and stakes <= 0.04)
        print("easy_mode = %s" % self.easy_mode)
    def start_round(self):
        # stakes in dollars
        self.state = None
    def want_leave(self):
        "Called before asking for bet, etc"
        if self.state is None:
            return False
        # if self.state.pot[0] == 0 and 10 < self.state.coins[0] < 21:
        #     print("Only %d coins, leaving" % self.state.coins[0])
        #     return True
        return False
    def receive_left(self):
        "Opponent left. Can happen at any point in round. Can still receive state after this."
        pass

    def pick(self):
        "Select and return (col, row)"
        assert False
    def first_bet(self, max_bet):
        "Return a bet amount >= 0"
        assert False
    def bet_or_fold(self, min_bet, max_bet):
        "Return additional amount to put in pot, or 0 to fold"
        assert False
    def reveal(self):
        "Return col,row to reveal (row is redundant)"
        assert False

    def _receive_historied_state(self, state):
        "Used by tournament.py, .history is filled out so don't want time on it."
        self.state = state
        self.possible_scores = PossibleScores(state)

    def receive_state(self, state):
        if isinstance(state, str):
            state = round.RoundState.from_string(state)
        if False:
            if self.state is None:
                state.start_round()
                self.state = state
            else:
                # Set the history correctly
                self.state.update_from_new_state(state, verbose=True)
        else:
            if self.state is None:
                state._ante = state.pot[0]
            else:
                state._ante = self.state._ante
            state.round = min(len(state.picks1), len(state.picks2))
            self.state = state
            
        self.possible_scores = PossibleScores(state)

    # Generally unimplemented

    def receive_pick(self, pick1, pick2):
        """Receive the pick that each player made during rounds 0-2. No return.
        pick1 is us. Each move is (col, row) pair; '?' for the unknown element."""
        pass
    def receive_bet(self, amount1, amount2):
        "Receive the amounts each player bet"
        pass
    def receive_fold(self, which_player):
        "Receive who folded, either 1 or 2. No return"
        pass
    def receive_col_reveal(self, col1, row2):
        "Receive col and row revealed by player1, player2; no return"
        pass
    def receive_pick_lists(self, picks1, picks2):
        "Full reveal, receive unhidden list of 4 picks for each player"
        pass
