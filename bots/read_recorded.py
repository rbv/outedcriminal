#!/usr/bin/env python

import re
import os
import sys
import time
import numpy as np
import copy

from cordialminuet import util
import calmerudition
import round
from player import GamePlayer
#import inner_circle


class ScannableFile():
    """Because Python's file buffering
    a) doesn't have any fscanf-like ability, or ability to use re on
       file contents
    b) sucks for reading in a non-line-orientated way
    c) doesn't have a working peek() function (it ignores its argument,
       instead returning only 1+ bytes) or un-read function
    """
    def __init__(self, filename):
        # Returns a BytesIO (buffered binary stream)
        self.file = open(filename, 'rb')
        self.buff = bytes()
    def ready(self):
        """Whether any data ready to read"""
        #if self.buff and self.buff.strip():
        #    return True
        text = self.peek(6)
        #if len(text) < 5:
        ret = len(text) > 5
        #if not ret:
        #    print("ready:%d left" % len(text))
        return ret
    def peek(self, length = 100):
        if len(self.buff) < length:
            added = self.file.read(length)
            #print("read %d of %d" % (len(added), length))
            self.buff += added
        return self.buff[:length]
    def read(self, length):
        ret = self.peek(length)
        self.buff = self.buff[length:]
        return ret

    def read_regex(self, pattern):
        """pattern must be a bytes. Returns None or the text"""
        text = self.peek(1000)
        if len(text) <= 1:
            print("SHORT: '%s'" % util.bytes_to_str(text))
            return None
        #text = str(text, 'ASCII')
        #print("TEXT: '%s'" % str(text[:100], 'ASCII'))
        match = re.match(pattern, text)
        #if not match:
        #    return None
        #print('readint "%s"' % (text,), match, match.end())
        self.read(match.end())  # skip
        return match.group()

    # def readint(self):
    #     text = self.file.peek(14)[:14]
    #     text = str(text, 'ASCII')
    #     print("TEXT " + text)
    #     match = re.match('\d+', text)
    #     print('readint "%s"' % (text,), match, match.end())
    #     self.file.read(match.end())
    #     return int(match.group())
    def readspace(self):
        self.read_regex(b'\W*')
    def readint(self):
        """Ignore leading space"""
        text = self.read_regex(b'\W*\d+')
        #if text is None:
            
        return int(text)
    def readints(self, num):
        return [self.readint() for i in range(num)]
    def readdata(self, length):
        # Returns a str
        ret = util.bytes_to_str(self.read(length))
        #print("readdata(%d) = '%s'" % (length, ret))
        return ret
    def readline(self):
        return self.read_regex(b'[^\n]*\n?')


class IterWebEvents():
    """Yields content of each recieved web event in a recorded game file"""
    def __init__(self, rfile):
        """
        rfile: a ScannableFile"""
        self.rfile = rfile

    def __iter__(self):
        """Iterate until the file isn't ready any more"""
        while True:
            rfile = self.rfile
            if not rfile.ready():
                return
            num_events = rfile.readint()
            for idx in range(num_events):
                rfile.readspace()
                code = rfile.readdata(2)
                if code[0] == 'w':
                    # incoming web event
                    handle, type = rfile.readints(2)
                    if type == 2:
                        length = rfile.readint()
                        if code[1] == 'b':
                            # plain text body
                            # trim space
                            rfile.readdata(1)
                            content = rfile.readdata(length)
                            yield content
                        else:
                            # wx is hex-encoded body
                            raise ValueError('unsupported code "%s"' % code)
                    else:
                        # others are ignored other than their handles
                        pass

                elif code[0] in ('t',):
                    rfile.readints(1)
                elif code in ('mm', 'md'):
                    rfile.readints(2)
                elif code[0] in ('k', 's', 'x'):
                    rfile.readints(3)
                elif code in ('mb',):
                    rfile.readints(4)
                else:
                    # anything else
                    raise ValueError('unknown code "%s"' % code)

    def ready(self):
        return self.rfile.ready()

    def wait(self):
        if self.ready():
            return
        #print("Waiting...")
        while not self.ready():
            time.sleep(1)
            #input('ENTER to stop waiting...')


def last_recorded_game():
    "Return filename"
    indir = '/mnt/common2/src/CordialMinuet/build/source/CordialMinuet_v11/recordedGames/'
    for (dirpath, dirnames, filenames) in os.walk(indir):
        logs = []
        for x in filenames:
            if x.startswith('recordedGame'):
                logs.append((int(x[len('recordedGame'):][:5]), x))
        logs.sort()
        if len(logs):
            # get last log
            print(logs[-1])
            return dirpath + logs[-1][1]
        raise ValueError('file not found')

round_num = 0
def process_log(eventiter, state = None):
    player = GamePlayer()
    """Process log until end; returns final state"""
    global round_num
    for event in eventiter:
        event = event.rstrip()
        lines = event.split('\n')
        #if len(lines) > 1:
        # filter out some stuff
        if lines[0] in ('OK', 'waiting', 'move_ready'):
            continue
        if len(lines) >= 3 and lines[2] == '999999999.99':
            continue    
        #print("event: '%s'" % event)
        if lines[0] in ('started', 'next_round_started'):
            state = None #RoundState()
            round_num += 1
            print("round_num", round_num)
            print("event: '%s'" % event)
            continue
        if lines[0] in ('round_ended', 'opponent_left'):
            print("event: '%s'" % event)
            # Followed by another game state message to show final coins.
            # Delay sending receive_left until we get the state
            state.set_done()
            continue
        if len(lines) >= 10 and len(lines[1]) == 98:
            # Game state
            print("-----------------------------------------")
            print("event: '%s'" % event)
            
            # temporary
            nextstate = RoundState.from_string(event)
            if state is None:
                state = nextstate
                state.start_round()
                state.printout()
            else:
                print("<<<<-Nextstate:")
                nextstate.printout()
                print("->>>>")
                print("<<<<-update_from_new_state:")
                if state.update_from_new_state(nextstate, verbose=False):
                    print(" ....")
                    state.printout()
                    # if round_num > 112:
                    #     calmerudite.search.process(state)
                print("-------------------------------------->>>>")
            if not nextstate.running:
                player.receive_left()
        else:
            print("event: '%s'" % event)
    return state


#fname = '/mnt/common2/src/CordialMinuet/build/source/CordialMinuet_v9/recordedGames/recordedGame00051.txt'

#fname = '/mnt/common2/src/CordialMinuet/build/source/CordialMinuet_v11/recordedGames/recordedGame00015.txt'
fname = last_recorded_game()
print(fname)
recordfile = ScannableFile(fname)

# Ignore header
print(recordfile.readline())
            
eventiter = IterWebEvents(recordfile)
state = None
while True:
    eventiter.wait()
    state = process_log(eventiter, state)
 
