import copy
import numpy as np

from cordialminuet.util import all_unique, print3

import cordialminuet.board
import cordialminuet as cm
import bots.calmerudition.inner_circle as inner_circle

disable_tribute = False

def tribute(coins):
    if not disable_tribute and coins >= 6:
        return 1
    return 0


def _parse_picks(line):
    """Parse picks line from a get_game_state message Returns (col,row) pairs, where 4th element (col,None) is revealed column;
    all are from the view of that player!
    """
    if line == '#':
        return []
    line = line.split('#')
    #print(line)
    line = ['?' if x == '?' else int(x) for x in line]
    ret = []
    # group into pairs
    for idx in range(0, min(6, len(line)), 2):
        ret.append([line[idx], line[idx + 1]])
    if len(line) >= 7:
        ret.append([line[6], None])
    return ret


class RoundState():
    """State of a single game round"""
    # States are the following:
    # pick:      Each select col + row
    # reveal:    Select column
    # 1stbet:    The betting after a pick/reveal. Both players.
    # bet:       An additional betting turn, ie raise. One player only.
    # preflop:   read_recorded only; nothing happens. Waiting for full board state.
    # done:      Nothing happens. ()
    # opponent_left:

    # .history is a sequence of the following:
    # ante:   Always first item. (amount, amount)
    # bet:    On '1stbet': (int:amount, int:amount)
    #         On 'best':   (None, int:amount) or (int:amount, None)
    # fold:   Follows a 1stbet. (False, True) or (True, False)
    # pick:   pick row/column. No args! (unimplemented) recorded in picks1 and picks2 instead.
    #         (Remember to update censored_state if implemented)
    # reveal: Revealed squares (all player 1's POV).
    #         (revealedcol1, revealedrow1), (revealedcol2, revealedrow2)
    # flop:   (score1, score2)
    # left:   Who left: (True, False) or (False, True)

    def __init__(self):
        # The board is from the perspective of the first player,
        # picks from perspective of respective player
        self.history = []
        self.state = ''  #
        self.round = 0   # 0..2 is picking, 3 is reveal, 4 is after reveal
                         # Equal to number of picks completed (Gets incremented AFTER picking is done)
        self.done = False  # server says game is over for some reason
        self.revealed = False
        self.board = None
        # Picks is sequence length 0 to 4 of (col1,col2) pairs from player's
        # perspective. col1 is col for self, col2 is row for opponent
        self.picks1 = []  # Sequence of up to three (col,row) followed by (col,None) (optionally (col,row))
        self.picks2 = []  # Sequence of up to three (row,col) followed by (row,None)
        self.pot = [0, 0]
        self.coins = [0, 0]
        self.ignored_states = 0
        self._last_picks1 = self._last_picks2 = None
        self.before_reveal_col = None
        self._leave_penalty = 6   #if not known from server

    @staticmethod
    def from_string(string):
        "Returns a RoundState from a server get_game_state response (excluding 'OK')"
        lines = string.split('\n')
        state = RoundState()
        state.running = lines[0]  # 1 if opponent left, else 0
        state.board = [int(x) for x in lines[1].split('#')]
        # in [y][x] order
        state.board = np.array(state.board, dtype = np.int32).reshape((6,6))

        if '#' in lines[6]:
            # This is an old recording from a previous version
            gametype = 0
        else:
            gametype = int(lines[2])
            del lines[2]

        state.coins[0] = int(lines[2])  # ours
        state.coins[1] = int(lines[3])  # theirs
        state.pot[0] = int(lines[4])  # ours
        state.pot[1] = int(lines[5])  # theris

        state.picks1 = _parse_picks(lines[6])  # col,row,col,row... our picks
        state.picks2 = _parse_picks(lines[7])  # row,col,row,col... their picks
        state.seconds_left = int(lines[8])
        state._leave_penalty = int(lines[9])
        return state

    def put_in_ante(self, amount):
        """Put in ante"""
        assert self.pot == [0, 0]
        amount = min(amount, self.coins[0], self.coins[1])
        self.coins[0] -= amount
        self.coins[1] -= amount
        self.pot = [amount, amount]

    def ante(self):
        "What the ante was"
        if hasattr(self, '_ante'):  # hack
            return self._ante
        h = self.history[0]
        assert h[0] == 'ante'
        return h[1]

    def leave_penalty(self, player_num):
        "Coins to leave, not including pot contents"
        # Note: server already takes the min itself
        return min(self.coins[player_num], self._leave_penalty)

    def start_round(self):
        """Call after the ante is set (whether by put_in_ante or manually
        setting pot)."""
        self.state = 'pick'
        assert self.picks1 == [] and self.picks2 == []
        assert len(self.history) == 0
        # Record the ante
        self.history.append(('ante', self.pot[0], self.pot[1]))
        self._last_picks1 = self._last_picks2 = []

    def printout(self):
        print('State: %s, round = %d, revealed=%s done: %s; Coins: ours = %d, theirs = %d, pot = %d+%d' % (self.state, self.round, self.revealed, self.done, self.coins[0], self.coins[1], self.pot[0], self.pot[1]))
        print('Our picks: %s' % self.picks1)
        print('Their picks: %s' % self.picks2)
        print('history: %s' % self.history)
        cm.board.show_board(self.board, self.picks1, self.picks2)

    def score(self, who):
        "Return total score"
        total = 0
        if who == 0:
            for idx in range(min(3, len(self.picks1))):
                total += self.board[self.picks2[idx][1]][self.picks1[idx][0]]
        else:
            for idx in range(min(3, len(self.picks1))):
                if self.picks2[idx][0] == '?':
                    raise ValueError("RoundState.score can't compute")
                total += self.board[self.picks2[idx][0]][self.picks1[idx][1]]
        return total

    def columns_left(self):
        "List of column indices left (player 0 POV)"
        selected = sum(self.picks1, [])  # may contain '?'; doesn't matter
        return filter(lambda x: x not in selected, range(6))

    def rows_left(self):
        "List of rows indices left (player 0 POV)"
        selected = sum(self.picks2, [])  # may contain '?'; doesn't matter
        return filter(lambda x: x not in selected, range(6))

    # def reveal_row_from_column(self, who, col):
    #     "Given just a reveal column, return row number."
    #     picks = (state.picks1, state.picks2)[who]
    #     other_picks = (state.picks1, state.picks2)[1 ^ who]
    #     for it, (us, them) in enumerate(self.picks1):
    #         if us == col:
    #             return self.picks2[it][1]
    #     assert False

    def set_done(self):
        "opponent_left or round_ended"
        self.done = True

    def min_and_maxbet(self, who):
        "A pair (min_bet, max_bet) a player can put in"
        owing = max(0, self.pot[1 ^ who] - self.pot[who])
        return owing, min(self.coins[who], self.coins[1 ^ who] + owing)

    def rotate(self, skip_history = False):
        """Rotate the board and everything referring to it"""
        self.board = np.array(zip(*self.board), dtype = np.int32)
        self.pot = list(reversed(self.pot))
        self.coins = list(reversed(self.coins))
        self.picks1, self.picks2 = self.picks2, self.picks1
        if not skip_history:
            hist = []
            for item in self.history:
                ty = item[0]
                if ty in ('ante', 'bet', 'fold', 'flop'):
                    ty, m1, m2 = item
                    hist.append((ty, m2, m1))
                elif ty == 'pick':
                    # No data
                    hist.append((ty,))
                elif ty == 'reveal':
                    # everything in 'reveal' is from first player's perspective
                    ty, m1, m2 = item
                    m1 = list(reversed(m1))
                    m2 = list(reversed(m2))
                    hist.append((ty, m2, m1))
                else:
                    assert False, "Unknown history item %s" % (item,)
            self.history = hist
        self._last_picks1 = self._last_picks2 = []

    def censor_picks(self, picks):
        "Remove picks not seen by other player. Operate on picks1 or picks2"
        if not self.revealed:
            revealed_col = None
            if len(picks) == 4:
                revealed_col = picks[3][0]
            for idx in range(0, min(3, len(picks))):
                if picks[idx][0] != revealed_col:
                    picks[idx][0] = '?'

    def censored_state(self, player_num):
        """Given player_num in (0,1), returns a copy of this
        state with info hidden from player_num censored, and also board
        rotated for 2nd player."""
        ret = copy.deepcopy(self)
        assert player_num in (0,1)
        if player_num == 1:
            ret.rotate()
        ret.censor_picks(ret.picks2)
        return ret

    def _bets_expected(self):
        "Add any betting that was overlooked because the players didn't bet"
        if self.history[-1][0] != 'bet':
            self.history.append(('bet', 0, 0))

    def update_from_new_state(self, nextstate, verbose = False): #, player1 = None):
        """Figure out what the current game state is, after members have been set externally.
        If player isn't None, then send it recieve_* messages.

        Returns true if the state changed
        """

        def log(msg):
            if verbose:
                print(msg)

        log("update_from_new_state: initial .state=%s" % self.state)

        # seconds_left is ignored for equality checking
        self.seconds_left = nextstate.seconds_left
        all_equal = True
        no_bet = False
        attrs = ('pot', 'coins') # 'picks1', 'picks2')
        for attr in attrs:
            if not hasattr(self, attr) or getattr(self, attr) != getattr(nextstate, attr):
                all_equal = False

        if not nextstate.running:
            # Opponent left; state records coins after leaving penalty.
            # Server sets the moves lists to zero, only the coins change.
            assert self.state == 'done'
            self.pot = nextstate.pot
            self.coins = nextstate.coins
            
            self.state = 'opponent_left'
            return


        # The last pick lists given to update(), which may be different
        # to self.picks1/2
        # print("last picks1", self._last_picks1, "new picks1", nextstate.picks1)
        # print("last picks2", self._last_picks2, "new picks2", nextstate.picks2)
        if self._last_picks1 != nextstate.picks1 or self._last_picks2 != nextstate.picks2:
            all_equal = False
        self._last_picks1 = nextstate.picks1
        self._last_picks2 = nextstate.picks2
        self._leave_penalty = nextstate._leave_penalty

        print3("nextstate.picks1", nextstate.picks1)
        print3("nextstate.picks2", nextstate.picks2)

        if len(self.picks1) <= 3 and len(nextstate.picks1) >= 4:
            # Reveal col
            pass
            #self.before_reveal_col = copy.deepcopy(self)

        if all_equal:
            # Everything is equal, ignore this state
            self.ignored_states += 1
            if self.ignored_states == 3 and self.state == '1stbet':
                log("assuming no-bet occurred")
                no_bet = True
            else:
                log("ignoring no-change state")
                return False
        self.ignored_states = 0

        print3("  Initial state:", self.state)
        assert (self.board == nextstate.board).all()

        handled = False

        if nextstate.pot != self.pot and nextstate.pot == [0, 0]:
            assert handled == False
            # Someone won, figure out what happened
            if self.state == 'flop':
                # End of game
                assert self.pot[0] == self.pot[1]
                # No history needed

            elif self.state in ('1stbet', 'bet'):
                # someone folded (can't fold during 1stbet)
                if nextstate.coins[0] > self.coins[0]:
                    if self.state == '1stbet':
                        # There is no game state message telling how
                        # much we bet
                        self.history.append(('bet', '?', 0))
                    self.history.append(('fold', False, True))
                    assert nextstate.coins[1] == self.coins[1]
                else:
                    if self.state == '1stbet':
                        print("self state: ################")
                        self.printout()
                        print("nextstate: ################")
                        nextstate.printout()
                        raise ValueError('bad state occurred')
                        
                    self.history.append(('fold', True, False))
                    assert nextstate.coins[0] == self.coins[0]
            elif self.done:
                # someone left
                if nextstate.coins[0] > self.coins[0]:
                    self.history.append(('left', False, True))
                else:
                    # we did
                    self.history.append(('left', True, False))
            else:
                print("self state: ################")
                self.printout()
                print("nextstate: ################")
                nextstate.printout()
                raise ValueError('bad state occurred')
            self.state = 'done'
            log("               End of game")
            handled = True
                
        elif nextstate.pot != self.pot or no_bet:
            # a player bet/raised, or we're assuming a round of betting just ended
            assert handled == False
            bet1 = nextstate.pot[0] - self.pot[0]
            bet2 = nextstate.pot[1] - self.pot[1]
            assert self.state in ('1stbet', 'bet')
            if self.state == 'bet':
                # One player has to match, raise or fold. Unfortunately
                # this gets merged together with opponent response in the
                # log; there is no intermediate gamestate.
                # Seperate it out if needed
                if self.pot[0] > self.pot[1]:
                    # First opponent responds
                    self.history.append(('bet', None, bet2))
                    if nextstate.pot[1] > self.pot[0]:
                        # Opponent actually raised, and we matched
                        self.history.append(('bet', bet1, None))
                    #self.pot[1] = nextstate.pot[1]
                else:
                    assert self.pot[0] < self.pot[1]
                    # First we respond
                    self.history.append(('bet', bet1, None))
                    if nextstate.pot[0] > self.pot[1]:
                        # We raised, opponent matched
                        self.history.append(('bet', None, bet2))
                    #self.pot[0] = nextstate.pot[0]
            else:
                self.history.append(('bet', bet1, bet2))

            if nextstate.pot[0] == nextstate.pot[1]:
                if self.round == 3:
                    self.state = 'reveal'
                elif self.round == 4:
                    # The board should also be revealed now
                    self.state = 'preflop'
                else:
                    self.state = 'pick'
            else:
                self.state = 'bet'
            log("                Change in pot")
            handled = True

        self.pot = nextstate.pot
        self.coins = nextstate.coins

        nextstate.round = len(nextstate.picks1)
        assert nextstate.round == len(nextstate.picks2)
        print("rounds: self=%d next=%d" % (self.round, nextstate.round))
        assert self.round <= nextstate.round <= self.round + 1
        # self.picks1 = nextstate.picks1
        # self.picks2 = nextstate.picks2
        if nextstate.round == self.round + 1:
            assert handled == False
            # if self.state == 'bet':
            #     # Was expecting a bet, but next changed state
            #     # was a column pick, so there was no betting
            #     self.history.append(('bet', 0, 0))
            # else:
            # if self.state not in ('pick', 'reveal'):
            #    In this case expected a bet but noone bet
            #     print("self state: ################")
            #     self.printout()
            #     print("nextstate: ################")
            #     nextstate.printout()
            #     #raise ValueError('bad state occurred')
            #     print("BAD STATE")

            if self.round == 3:
                # After the column reveal (but not for final revealall)
                # the server swaps the orders of the columns so that
                # the revealed square is col=picks1[0][1],row=picks2[0][0].
                # So don't overwrite our picks lists
                self.picks1.append(nextstate.picks1[-1])
                self.picks2.append(nextstate.picks2[-1])
                # Note: In the following everything is from player 1's POV
                # Square player 1 recieved
                # print("picks1", self.picks1)
                # print("picks2", self.picks2)
                revealedcol1 = nextstate.picks1[-1][0]  # our column
                # print("revealedcol1", revealedcol1)
                for i,(ours,theirs) in enumerate(self.picks1[:3]):
                    if ours == revealedcol1:
                        revealedrow1 = self.picks2[i][1]  # corresponding row they picked
                # Square player 2 recieved
                revealedrow2 = nextstate.picks2[0][0]  # their row picked by them
                revealedcol2 = nextstate.picks1[0][1]  # their column picked by us
                # Figure out which row got swapped in our picklist
                # print("revealedcol2", revealedcol2)
                # print("revealedrow2", revealedrow2)
                for i,(ours,theirs) in enumerate(self.picks1[:3]):
                    if theirs == revealedcol2:
                        turn_number = i
                # ...and set it in unswapped list
                self.picks2[turn_number][0] = revealedrow2
                self.history.append(('reveal', (revealedcol1, revealedrow1), (revealedcol2, revealedrow2)))
                log("                round 3")
            else:
                self.picks1 = nextstate.picks1
                self.picks2 = nextstate.picks2
                # Get the picks from self.pick*
                self.history.append(('pick',)) # self.picks1[-1], self.picks2[-1]))
                log("                normal pick")
            self.round = nextstate.round
            self.state = '1stbet'
            handled = True
        else:
            assert nextstate.round == self.round

        # Check whether just revealed
        if not self.revealed and self.round == 4:
            self.revealed = '?' not in sum(nextstate.picks2, [])
            # for (row2, row1), (row2_new, row1_new) in zip(self.picks2, nextstate.picks2):
            #     if row2_new == '?':
            #         self.revealed = False
            #         break
            # else:
            #     self.revealed = True
            if self.revealed:
                assert self.state in ('1stbet', 'bet', 'preflop')
                self.picks1 = nextstate.picks1
                self.picks2 = nextstate.picks2
                for (row2, row1), (row2_new, row1_new) in zip(self.picks2, nextstate.picks2):
                    assert row1 == row1_new
                    assert row2 == row2_new or row2 == '?'
                self._bets_expected()
                our_scores, their_scores, our_scores_their_pov = inner_circle.possible_scores(self)
                # Append the final scores
                self.history.append(('flop', int(np.argmax(our_scores)), int(np.argmax(their_scores))))
                self.state = 'flop'
                log("                flop")
                handled = True

        if self.done:
            assert self.state in ('done', 'flop')
            self.state = 'done'
            handled = True

        assert handled
        return True

        
    def ai_move(self, player):
        """Given a player.GamePlayer, send it a message to ask for the next move,
        if required"""
        if self.state == 'bet':
            player.bet()
        elif self.state == 'reveal':
            player.reveal()


def new_random_round(coins, seed = None):
    state = RoundState()
    state.board = cm.board.random_board(seed)
    state.coins = coins[:]
    state.seconds_left = 50
    state.running = True
    return state


# class PlayerProxy:
#     def __call__(self, name, *args):
#         print("%d: %s(%s)" % (self.num, name, args))


def update_from_moves(state, player0_move, player1_move):
    """Reimplements much of CM server.
    Each move is a tuple, e.g. ('bet', 0) or None if the player has nothing to do.
    The actions correspond to RoundState.history:
    'bet',    amount
    'fold'
    'pick',   mycol, theircol (aka col,row)
    'reveal', col, row   (row may be None)
    'leave'   (leave game)

    Return pair pending_moves, which gives next move required of each player,
    in form of tuple (<method_name>, *args)"""

    class QueuedCalls:
        calls = [[], []]
        moves = [None, None]
    pending = QueuedCalls()

    move0 = player0_move
    move1 = player1_move
    moves = move0, move1
    print3("-----update_from_moves, state.state=", state.state, "moves:", move0, move1)


    def end_round():
        state.state = 'done'
        state.done = True
        state.pot = [0, 0]

    def win_pot(who):
        "Award the pot"
        pot = sum(state.pot)
        state.coins[who] += pot - tribute(pot)
        end_round()

    def leave_game(who):
        state.pot[who] += state.leave_penalty(who)
        state.coins[who] -= state.leave_penalty(who)
        win_pot(1 ^ who)
        state.history.append(('left', who == 0, who == 1))

    def draw():
        "Split the pot"
        state.coins[0] += state.pot[0]
        state.coins[1] += state.pot[1]
        end_round()

    def compare_scores():
        "End of the round reached"
        assert state.pot[0] == state.pot[1]
        score0 = state.score(0)
        score1 = state.score(1)
        state.history.append(('flop', score0, score1))
        if score0 == score1:
            draw()
        elif score0 < score1:
            win_pot(1)
        elif score0 > score1:
            win_pot(0)

    def done_betting():
        "picking/reveal is over, so increment round and move to next"
        if len(state.picks1) <= 2:
            state.state = 'pick'
            pending.moves = ['pick', 'pick']
        elif len(state.picks1) == 3:
            state.state = 'reveal'
            pending.moves = ['reveal', 'reveal']
        else:
            state.revealed = True
            compare_scores()

    def process_bet(who):
        move = moves[who]
        assert move[0] == 'bet', "'%s' move, expected 'bet'" % (move[0],)
        amnt = move[1]
        assert amnt >= 0 and amnt <= state.coins[who], "Invalid bet amount"
        # Not bothering to recompute the max_bet
        state.coins[who] -= amnt
        state.pot[who] += amnt

    def compare_bets():
        "After a pair of bets or a raise, what's next?"
        amnt0, amnt1 = state.pot
        if amnt0 != amnt1:
            state.state = 'bet'
            if amnt0 < amnt1:
                min_bet, max_bet = state.min_and_maxbet(0)
                assert state.coins[0] >= max_bet >= min_bet
                pending.moves = [('bet_or_fold', min_bet, max_bet), None]
            else:
                min_bet, max_bet = state.min_and_maxbet(1)
                assert state.coins[1] >= max_bet >= min_bet
                pending.moves = [None, ('bet_or_fold', min_bet, max_bet)]
        else:
            done_betting()

    def process_raise_or_fold(who):
        other = 1 ^ who
        assert moves[other] == None, "Wasn't expecting a move"
        # Was a raise
        move = moves[who]
        if move[0] == 'fold' or (move[0] == 'bet' and move[1] == 0):
            state.history.append(('fold', who == 0, who == 1))
            win_pot(other)
        elif move[0] == 'bet':
            assert min(state.pot) + move[1] >= max(state.pot), "Must at least match other's bet"
            process_bet(who)
            if who == 0:
                state.history.append(('bet', move[1], None))
            else:
                state.history.append(('bet', None, move[1]))
            compare_bets()
        else:
            assert False

    def process_pick(who):
        picks = (state.picks1, state.picks2)[who]
        move = moves[who]
        assert move[0] == 'pick'

        # col, row from the player's perspective
        _, col, row = move
        assert 0 <= col < 6
        assert 0 <= row < 6
        picks2 = picks + [(col,row)]
        assert all_unique([r for c,r in picks2])
        assert all_unique([c for c,r in picks2])
        picks.append([col,row])

    def process_reveal(who):
        picks = (state.picks1, state.picks2)[who]
        other_picks = (state.picks1, state.picks2)[1 ^ who]
        move = moves[who]
        assert move[0] == 'reveal'

        # col, row from the player's perspective
        if move[2] is None:
            _, col,_ = move
            for it, (us, them) in enumerate(picks):
                if us == col:
                    row = other_picks[it][1]
                    break
            #row = state.reveal_row_from_column(who, col)
        else:
            _, col, row = move
        picks.append([col,row])   # row isn't needed, but might as well

        # Check player actually received that square
        # (r and c from player's perspective)
        cols = [c for c,r in picks[:3]]
        rows = [r for c,r in other_picks[:3]]
        received_squares = zip(cols, rows)
        assert (col,row) in received_squares
        return col,row

    if move0 is not None and move0[0] == 'leave':
        leave_game(0)
    elif move1 is not None and move1[0] == 'leave':
        leave_game(1)

    elif state.state == '1stbet':
        # Bet after a pick
        process_bet(0)
        process_bet(1)
        state.history.append(('bet', move0[1], move1[1]))
        compare_bets()

    elif state.state == 'bet':
        # Raise required
        # Check the PREVIOUS pot to see who's turn it is
        if state.pot[0] < state.pot[1]:
            process_raise_or_fold(0)
        elif state.pot[0] > state.pot[1]:
            process_raise_or_fold(1)
        else:
            assert False, "bad .state=='bet'"

    elif state.state == 'pick':
        process_pick(0)
        process_pick(1)
        state.history.append(('pick',))  #unimplemented
        state.state = '1stbet'
        max_bet = min(state.coins)
        pending.moves = [('first_bet', max_bet), ('first_bet', max_bet)]

    elif state.state == 'reveal':
        (revealedcol1, revealedrow1) = process_reveal(0)
        (revealedrow2, revealedcol2) = process_reveal(1)
        state.history.append(('reveal', (revealedrow1, revealedcol1), (revealedrow2, revealedcol2)))
        state.state = '1stbet'
        max_bet = min(state.coins)
        pending.moves = [('first_bet', max_bet), ('first_bet', max_bet)]

    else:
        assert False, "Unhandled .state='%s'" % (state.state,)

    state.round = len(state.picks1)
    return pending.moves


def test_update_from_moves():
    """update_from_moves() testcases"""
    seed = 1
    state = new_random_round([100, 100], seed)

    # The following series of moves produces the following history
    history = [('ante', 1, 1), ('pick',), ('bet', 1, 0), ('bet', None, 3),
               ('bet', 2, None), ('pick',), ('bet', 0, 0), ('pick',), 
               ('bet', 0, 0), ('reveal', (0, 1), (2, 2)), ('bet', 10, 10),
               ('flop', 89, 75)]

    state.put_in_ante(1)
    state.start_round()
    assert state.history == history[:1]
    assert state.pot == [1, 1]
    assert state.round == 0

    move0 = 'pick', 1, 0
    move1 = 'pick', 3, 0
    pending = update_from_moves(state, move0, move1)
    state.printout()
    assert state.history == history[:2]
    assert state.round == 1
    assert pending == [('first_bet', 99), ('first_bet', 99)]

    pending = update_from_moves(state, ('bet', 1), ('bet', 0))
    state.printout()
    assert state.history == history[:3]
    assert state.round == 1
    assert pending == [None, ('bet_or_fold', 1, 99)]

    pending = update_from_moves(state, None, ('bet', 3))
    assert state.history == history[:4]
    assert pending == [('bet_or_fold', 2, 98), None]
    state.printout()

    pending = update_from_moves(state, ('bet', 2), None)
    assert state.history == history[:5]
    assert state.round == 1
    assert pending == ['pick', 'pick']
    state.printout()

    move0 = 'pick', 3, 2
    move1 = 'pick', 2, 5
    try:
        # Illegal player 0 pick
        pending = update_from_moves(state, ('pick', 3, 0), move1)
    except AssertionError:
        pass
    else:
        assert False
    pending = update_from_moves(state, move0, move1)
    state.printout()
    assert state.history == history[:6]
    assert state.round == 2
    assert state.coins == [96, 96]
    assert pending == [('first_bet', 96), ('first_bet', 96)]

    pending = update_from_moves(state, ('bet', 0), ('bet', 0))
    state.printout()
    assert state.history == history[:7]
    assert pending == ['pick', 'pick']

    move0 = 'pick', 4, 5
    move1 = 'pick', 1, 4
    pending = update_from_moves(state, move0, move1)
    state.printout()
    assert state.history == history[:8]
    assert pending == [('first_bet', 96), ('first_bet', 96)]

    pending = update_from_moves(state, ('bet', 0), ('bet', 0))
    state.printout()
    assert state.history == history[:9]
    assert pending == ['reveal', 'reveal']

    try:
        # Illegal player 2 reveal
        pending = update_from_moves(state, ('reveal', 1, 0), ('reveal', 2, 3))
    except AssertionError:
        pass
    else:
        assert False
    pending = update_from_moves(state, ('reveal', 1, 0), ('reveal', 2, 2))
    state.printout()
    assert state.history == history[:10]
    assert pending == [('first_bet', 96), ('first_bet', 96)]
    assert not state.revealed

    pending = update_from_moves(state, ('bet', 10), ('bet', 10))
    state.printout()
    assert state.history == history
    assert pending == [None, None]
    assert state.revealed
    assert state.done

    #### Testing leaving

    state = new_random_round([100, 90], seed)
    state.put_in_ante(4)
    state.start_round()

    pending = update_from_moves(state, ('pick', 1, 0), ('leave',))
    state.printout()
    assert pending == [None, None]
    assert state.revealed == False
    assert state.done
    cost = 4 + 6  # pot, next ante, 6
    assert state.coins == [100 + cost - 1, 90 - cost]  #-1 for tribute
    assert state.history == [('ante', 4, 4), ('left', False, True)]


if __name__ == '__main__':
    test_update_from_moves()
