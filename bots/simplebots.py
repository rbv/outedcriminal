import random
import itertools
import numpy as np
import copy

from cordialminuet.util import print3, Timer
from .calmerudition.search import PossibleScores, calc_chances, format_chances, score_with_win_chance, result_chances

from player import GamePlayer

#import calmerudition.inner_circle as inner_circle
import calmerudition.search


verbose = 1#False



def pmf_sample(weights):  #pick_with_weights()
    "Given an unnormalised pmf, sample, returning pmf array index"
    #weights = np.array(weights)
    # weights.
    assert (weights >= 0.).all()
    cum = np.cumsum(weights)
    rnd = random.uniform(0., cum[-1])
    for idx, val in enumerate(cum):
        if rnd <= val:
            return idx
    assert False


def pick_options(state):
    """All possible pick or reveal moves for player 0"""
    if len(state.picks1) < 3:
        cols = state.columns_left()
        options = itertools.permutations(cols, 2)
    else:
        options = [[x[0],None] for x in state.picks1]
    return options

def look_ahead_at_picks(state, secret = True):
    """
    Returns a sorted array of (winchance, picks_pair) pairs.
    secret: If true, use secret rather than public possible scores
    Note: this can work for reveal too, but result probably nonsensical.
    """
    options = pick_options(state)
    remem_picks1 = state.picks1
    ranking = []
    for option in options:
        state.picks1 = remem_picks1 + [option]
        #state.printout()
        possible_scores = PossibleScores(state)
        chances = calc_chances(possible_scores, secret)
        win_chance, draw_chance, lose_chance = chances
        score = win_chance + draw_chance / 2
        ranking.append((score, option, possible_scores))

    state.picks1 = remem_picks1
    ranking.sort()
    return ranking

################################################################

def pick_bet_sizes(state, min_bet, max_bet, easy_mode = False):
    pot = state.pot
    assert pot[1] >= pot[0]

    #betsizes0 = [min_bet, bet1, int(pot[0] / 2), pot[0], bet1 * 2, bet1 + pot[1]]
    if easy_mode:
        bet1 = min_bet
        if bet1 == 0:
            bet1 = 2 + int(pot[0] / 3)
        betsizes0 = [min_bet, 5, min_bet + 2, bet1, int(pot[0] / 2), pot[0], bet1 * 2]
        max_bet = min(max_bet, max(min_bet, pot[1] - 1, 4))
    else:
        effpot = max(pot[0], 3)
        betsizes0 = [min_bet, int(effpot / 2) + 2, effpot]

    betsizes0 = [max(min_bet, min(max_bet, x)) for x in betsizes0]
    # remove dups
    betsizes0 = list(sorted(set(betsizes0)))
    print("pick_bet_sizes(min=%d,max=%d,pot=%s) = %s" % (min_bet, max_bet, pot, betsizes0))
    return betsizes0

################################################################ op model

def strength_of_betting(state, who, pot):
    """Crappy. Returns a multiplier, start at 1, increase up to 3 when 'who' bets a 'huge' amount (6x ante)"""
    bet_mult = (pot + 4.) / (state.ante() + 4) - 1.
    return 1. + min(2.5, bet_mult * .5)


def simple_bet_chance(state, possible_scores, best_possible_scores, best_chances, min_bet, max_bet, bet):
    """Chance that this bet has positive expectation
    value, assuming some crude uncertainty and betting strength
    compensation.
    possible_scores:  current, to use for their pov
    best_possible_scores:  best that we can get next move
    best_chances: best (win,draw,lose) chances we can get next move
    """

    pot0 = state.pot[0]
    pot1 = state.pot[1]
    bet0 = bet

    # We assume they fold if their win chance is less than this
    # This is the main heuristic guess here.

    #expected_value = -bet + 2*(bet+pot)*win_chance
    # so break even point is
    # bet < 2*(bet+pot)*chance
    # =>

    def get_break_even_chance(pot, bet):
        # expectation for additional investment size is:
        #expected_value = -bet + 2*(bet+pot)*win_chance
        # so break even point is
        # bet < 2*(bet+pot)*chance
        # =>
        ret = bet / (2.*(bet+pot))
        assert 0 < ret < 1.
        return ret

    if min_bet > 0:
        # Check their break-even chance for the bet they made
        # (not correct if we also bet)

        their_break_even_chance = get_break_even_chance(pot1 - min_bet, bet)
        # FIXME: this might be very close to 1 for a huge
        # bet, but it would be incredibly wrong to conclude that they must
        # be bluffing (which would then cause all scores to be equiprobable!!!!!)
    else:
        their_break_even_chance = 0

    betting_rounds_left = 4 - state.round

    fold_chance = None
    if bet == min_bet:
        fold_chance = 0.
    else:
        # 
        # what they will have to bet
        bet1 = pot0 + bet - pot1
        effective_bet1 = min(max_bet, bet1 * (1.4 ** min(2.5, betting_rounds_left)))
        assert bet1 > 0
        chance2 = get_break_even_chance(pot1, effective_bet1)
        if chance2 < their_break_even_chance:
            # not a serious raise
            fold_chance = 0.
        else:
            their_break_even_chance = chance2

    # another guess: chance they'll continue with a score
    # with negative expectation
    their_bluff_chance = 0.24

    # Can compute fold_chance from 
    #their_chances = public_chances[::-1]

    their_needed_min_score = score_with_win_chance(possible_scores.ours_their_pov, their_break_even_chance)
    print("their_break_even_chance %.3f, their_needed_min_score %d" % (their_break_even_chance, their_needed_min_score))

    # We assume their scores are equiprobable here
    cum_probs_theirs = np.cumsum(possible_scores.theirs).astype(float)
    assert cum_probs_theirs[-1] > 0
    cum_probs_theirs /= cum_probs_theirs[-1]

    if fold_chance is None:
        # we haven't yet decided they won't fold
        
        #print(possible_scores)
        #possible_scores.print_graphs()
        fold_chance = cum_probs_theirs[their_needed_min_score]
        print("their_break_even_chance %.2f their_needed_min_score %.2f, foldchance %.3f" % (their_break_even_chance, their_needed_min_score, fold_chance))
        assert 0 <= fold_chance <= 1. + 1e-8
        fold_chance = min(0.8, fold_chance)


    win_chance, draw_chance, lose_chance = best_chances
    win_chance = win_chance + draw_chance / 2
    lose_chance = 1 - win_chance

    initial_win_chance = win_chance

    # If they match our raise we can compute what the new 
    strength = strength_of_betting(state, 1, pot0 + bet)
    
    # One correction
    strength_win_chance = win_chance / (win_chance + lose_chance * strength)

    # Second correction, based on break_even
    # Now we assume that if they don't fold, any score above
    # their_needed_min_score is equiprobable:
    # lower prob of low scores, and renormalise
    probs_them = possible_scores.theirs.copy().astype(float)
    probs_them[:their_needed_min_score] *= their_bluff_chance
    probs_them /= probs_them.sum()

    probs_us = best_possible_scores.ours.copy().astype(float)
    probs_us /= probs_us.sum()
    win_chance, draw_chance, lose_chance = result_chances(probs_us, probs_them)
    BE_win_chance = win_chance + draw_chance / 2

    win_chance = min(strength_win_chance, BE_win_chance)
    lose_chance = 1 - win_chance


    # break_even_chance = get_break_even_chance(pot0, bet0)
    # print("break_even_chance without folding: %.3f" % break_even_chance)

    if False:
        # This is our win_chance
        # expectation for additional investment size is:
        #expected_value = -bet + (bet+pot0+pot1)*fold_chance + 2*(bet+pot0)*win_chance_on_match*(1-fold_chance)
        # so break even point is
        # bet - (bet+pot0+pot1)*fold_chance < 2*(bet+pot)*(1-fold_chance)*chance
        # =>
        break_even_chance = (bet0 - (bet0+pot0+pot1)*fold_chance) / (2.*(bet0+pot0)*(1-fold_chance))
        
        if break_even_chance <= 0.05:
            # break_even_chance can be negative if they have a very
            # high chance to fold
            break_even_chance = 0.05
    else:
        # It's incorrect to consider their fold chance, as they'll
        # only fold if they probably wont' win anyway
        break_even_chance = get_break_even_chance(pot0, bet0)

    assert 0 < break_even_chance < 1.

    # We overestimate opponent: our winning scores more likely
    # than appear
    win_chance_high = win_chance * 1.7
    win_chance_high /= win_chance_high + lose_chance

    # And underestimate: less likely
    win_chance_low = win_chance / 1.7
    win_chance_low /= win_chance_low + lose_chance


    if win_chance_high <= break_even_chance or win_chance == 0:
        ret = 0.
    elif win_chance_low >= break_even_chance:
        ret = 1.
    else:
        ret = (win_chance_high - break_even_chance) / (win_chance_high - win_chance_low)

    if verbose:
        print("simple_bet_chance(pot=(%d,%d),min_bet=%d,max_bet=%d,bet=%d):" % (pot0, pot1, min_bet, max_bet, bet))
        print("    equiprobable win_chance: %.3f strength_of_betting: %.2f strength-corrected win_chance: %.3f BE-corrected win_chance: %.3f" %
              (initial_win_chance, strength, strength_win_chance, BE_win_chance))
        print( "fold_chance: %.2f win_chance range: %.3f-%.3f break_even: %.3f -- bet_chance=%.3f" %
              (fold_chance, win_chance_low, win_chance_high, break_even_chance, ret))

    return ret

class BettingBase(object):

    def public_chances(self):
        """By assuming all scores equiprobable, calc win chance seen by them.
        Returns (win_chance, draw_chance, lose_chance)"""
        chances = calc_chances(self.possible_scores, False)
        if verbose:
            print3("public win/draw/lose chances:", format_chances(chances))
        return chances, self.possible_scores

    def secret_chances(self):
        """By assuming all scores equiprobable, calc actualy win chance.
        Returns (win_chance, draw_chance, lose_chance)"""
        chances = calc_chances(self.possible_scores, True)
        if verbose:
            print3("secret win/draw/lose chances:", format_chances(chances))
        return chances, self.possible_scores

class StupidBetting(BettingBase):
    def bet_or_fold(self, min_bet, max_bet):
        return min_bet
    def first_bet(self, max_bet):
        return 0


class DefaultBetting(BettingBase):
    "Default simulation policy"

    better = True
    
    def bet_or_fold(self, min_bet, max_bet):
        win_chance, draw_chance, lose_chance = self.secret_chances()[0]
        win_chance += draw_chance / 2
        bet_size = max(min_bet, min(max_bet, random.randint(1, 4 + self.state.pot[0])))
        if win_chance < 1/3.:
            return 0
        #elif win_chance < 2/3.:
        else:
            # Chance to raise or bluff
            apparent_win_chance, apparent_draw_chance, _ = self.public_chances()[0]
            apparent_win_chance += apparent_draw_chance / 2
            if random.random() < max(win_chance, apparent_win_chance):
                return bet_size
            return min_bet
        # else:
        #     return bet_size

    def first_bet(self, max_bet):
        return self.bet_or_fold(0, max_bet)

class BasicBetting(BettingBase):
    "Crappy betting"

    def _pick_bet_size(self, chances, max_bet):
        "Silly heuristic"
        win_chance, draw_chance, lose_chance = chances
        if win_chance + draw_chance / 2 < 0.5:
            return 0
        return random.randint(0, min(max_bet, self.state.pot[0] + 3))

    def first_bet(self, max_bet):
        """Return a bet amount >= 0.
        """
        # if verbose:
        #     print ("FIRST_BET(%d)" % (max_bet,))
        chances = self.secret_chances()[0]
        win_chance, draw_chance, lose_chance = chances
        if random.random() < win_chance + draw_chance / 2:
            ret = self._pick_bet_size(chances, max_bet)
        else:
            ret = 0
        #print3("betting", ret)
        return ret

    def bet_or_fold(self, min_bet, max_bet):
        "Return additional amount to put in pot, or 0 to fold"
        #print ("BET_OR_FOLD(%d,%d)" % (min_bet, max_bet))

        chances = self.secret_chances()[0]
        win_chance, draw_chance, lose_chance = chances
        win_chance = win_chance + draw_chance / 2
        lose_chance = 1 - win_chance

        pot = self.state.pot[0]
        bet = min_bet
        # expectation for additional investment size is:
        expect = -bet + 2*(bet+pot)*win_chance

        # This is also silly; definitely not near the Nash Eq.
        # random_expect = -bet
        # if random.random() < win_chance:
        #     random_expect += 2 * (x + pot)
        # if random.random() < draw_chance:
        #     random_expect += (x + pot)
        random_expect = expect + (random.random() - 0.5) * 0.5 * pot

        #if random.random() > win_chance + draw_chance:
        if random_expect > 0:
            # If we go for it, maybe also raise
            bet = self._pick_bet_size(chances, max_bet)
            ret = max(min_bet, bet)
        else:
            # Fold
            ret = 0
        #print3("betting", ret)
        return ret



class SimpleBetting(BettingBase):
    "More disciplined"

    def bet_or_fold(self, min_bet, max_bet):
        if verbose:
            print("SimpleBetting::bet_or_fold(%d, %d)" % (min_bet, max_bet))

        # overridden in LessSimpleBetting
        best_chances, best_possible_scores = self.secret_chances()

        #public_chances = self.public_chances()[0]
        betting_rounds_left = 4 - self.state.round
        assert 0 <= betting_rounds_left <= 3
        #print("%d betting rounds left" % betting_rounds_left)

        # Hack: forbid raising when matching a bet, as easily
        # goaded into betting everything
        if min_bet > 0:
            bet_sizes = [min_bet]
        else:
            bet_sizes = pick_bet_sizes(self.state, min_bet, max_bet, self.easy_mode)

        # bet_sizes = [min_bet]
        # if max_bet > min_bet:
        #     bet_sizes.append(min(max_bet, min_bet * 2))

        rand = random.random()
        to_bet = 0  # Bet nothing or fold is default
        total_weight = 0
        bet_chances = []
        for bet in bet_sizes:
            if bet != 0:
                if verbose:
                    print("-----bet = %d" % bet)
                if True: #min_bet > 0:
                    # Assume we'll have to keep calling to win, so actual investment
                    # will be more (but assume no more than 2 raises to go

                    effective_investment = min(max_bet, bet * (1.4 ** min(2.5, betting_rounds_left)))
                else:
                    effective_investment = bet
                bet_chance = simple_bet_chance(self.state, self.possible_scores, best_possible_scores, best_chances, min_bet, max_bet, effective_investment)
                if bet > min_bet:
                    if betting_rounds_left >= 2:
                        # don't give away hand too readily
                        bet_chance *= 0.6
                    # don't make large bets too often in case they fold
                    if bet >= self.state.pot[0] + 2:
                        bet_chance *= 0.35
                #total_weight += bet * bet_chance
                bet_chances.append((bet,bet_chance))
                if rand < bet_chance:
                    to_bet = bet
        # if self.easy_mode == False:
        #     # Rather than sample under the curve, select bet size uniformly.
        #     # And decide whether to bet at all independently, based on whether expectation
        #     # given random 
        #     avg_chance = sum([chance for bet,chance in bet_chances]) / len(bet_chances)
        #     bet, _ = random.sample(bet_chances, 1)[0]
        #     print("avg_chance %.2f in %s, uniform betsize %d" % (avg_chance, bet_sizes, bet))
        #     if rand < avg_chance:
        #         # uniform bet
        #         to_bet = bet
        return to_bet

    def first_bet(self, max_bet):
        return self.bet_or_fold(0, max_bet)

class LessSimpleBetting(SimpleBetting):
    def secret_chances(self):
        """By assuming all scores equiprobable, calc actual best win
        chance after the next pick.
        Returns (win_chance, draw_chance, lose_chance), possible_scores,
        where possible_scores is for that pick"""

        #if len(self.state.picks1) < :
        ranking = look_ahead_at_picks(self.state)

        #chances = calc_chances(self.possible_scores, True)
        win_chance = ranking[-1][0]
        draw_chance = 0.
        lose_chance = 1 - win_chance
        chances = win_chance, draw_chance, lose_chance
        # The best possible_scores
        possible_scores = ranking[-1][2]
        if verbose:
            basic_chances, all_possible_scores = SimpleBetting.secret_chances(self)
            print3("  non-look_ahead win/draw/lose chances:", format_chances(basic_chances))
            print3("  look_ahead win/draw/lose chances:", format_chances(chances))
        return chances, possible_scores


def op_model_call_chance(state, min_bet, bet):
    """What's chance that opponent will CALL a bet of this amount with
    this score graph and bet history? (state has their picks guessed)
    Note: this is done in simple_bet_chance"""

    possible_scores = PossibleScores(state)
    # Use secret=False to compare chance we will win against
    # their known possible scores, because we are guessing their
    # picks here.

    chances = calc_chances(possible_scores, secret = False)

    their_chances = self.secret_chances()[::-1]
    return simple_bet_chance(state, their_chances, min_bet, bet)


def op_model_score_probabilities(state):
    # given betting history, 
    pass


def op_model_chance_to_fold(state):
    # given betting history, 
    pass

class ValueBetting(BettingBase):

    def value_of_bet(self, bet):
        "If we bet some amount, what is the expected value?"

        # Note: betting increases opponent chance to fold in
        # future, which decreases value of a good hand and
        # increases value of a bad hand

        # Assuming opponent 
        win_chance_if_bet = win_chance

        pot = self.state.pot[0]
        # expectation for additional investment size x is:
        x = min_bet
        expect = -x + (x+pot)*draw_chance + 2*(x+pot)*win_chance_if_bet

        value = (pot + bet)

        pass

    def first_bet(self, max_bet):
        #bet_sizes = [...]
        for bet in bet_sizes:
            self.value_of_bet(bet)

################################################################ Picking


class RandomPicking(GamePlayer):

    def pick(self):
        "Select and return (col, row)"
        cols = self.state.columns_left()
        random.shuffle(cols)
        return cols[:2]

    def reveal(self):
        our_cols = [us for us,them in self.state.picks1]
        assert len(our_cols) == 3
        return random.sample(our_cols, 1)[0], None

class SensibleReveal(RandomPicking):
    """Reveal to minimise our apparent win chance if our actual win chance
    is high, or maximise it if our win chance is low"""

    def reveal(self):
        # Don't call self.secret_chances because it might be overridden
        chances = calc_chances(self.possible_scores, secret=True)
        win_chance = chances[0] + chances[1]/2

        # Public win chances for each reveal option
        ranking = look_ahead_at_picks(self.state, secret=False)
        assert len(ranking) == 3

        if win_chance > 0.6:
            # go moderate
            if verbose:
                print("  SensibleReveal.reveal: moderating public chance")
            return ranking[1][1]
        else:
            # maximise
            if verbose:
                print("  SensibleReveal.reveal: maximising public chance")
            return ranking[-1][1]


# class R0Picking(GamePlayer):
#     "Pick based on highest vs lowest average score"

#     def pick(self):
#         cols = self.state.columns_left()

class D2Picking(RandomPicking):
    """Pick based on score graph win ratio (equivalent to assuming opponent
    always moves randomly and we pick randomly hereafter"""

    def pick(self):
        """Pick from options with best PossibleScores win chance"""
        return look_ahead_at_picks(self.state, secret = True)[-1][1]

class R2Picking(RandomPicking):
    def _weight_choices(self, ranking, exponent = 2.4):
        "Returns ndarray of choice probabilities, and list of pick choices"
        # (win_chance, pick) pairs
        def weight(chance):
            if chance == 1:   #would be better to check losechance == 0
                return 1e5
            if chance < score_threshold + 1e-5:
                return 0.
            ret = (chance - score_threshold) ** exponent
            if not ret >= 0.:
                print3("c", chance - score_threshold, ret)
                return False
            return ret

        scores = [score for score,_,_ in ranking]
        # options below this threshold not picked
        score_threshold = max(scores) * 0.4
        weights = np.array([weight(score) for score in scores])
        if weights.sum() == 0.:
            weights += 1. / len(weights)
        else:
            weights /= weights.sum()
        choices = [pick for _,pick,_ in ranking]
        return weights, choices
        
    def pick(self):
        """Randomly select, weighted by square of equiprobable win chance"""
        ranking = look_ahead_at_picks(self.state, secret = True)
        weights, choices = self._weight_choices(ranking)
        choice = pmf_sample(weights)
        return choices[choice]


class D3Picking(R2Picking):
    """Pick by assuming opponent picks according to R2Picking (by pretending opponent hasn't
    made any previous picks"""

    def D3_rank_picks(self):
        calc_time = Timer()

        #print("D3Picking: ACTUAL STATE:")
        #self.state.printout()
        #print("-----searching")

        rotated = copy.deepcopy(self.state)
        rotated.rotate(skip_history = True)
        rotated.censor_picks(rotated.picks2)  # hide our picks
        # Modelled possible opponent moves
        ranking = look_ahead_at_picks(rotated, secret = True)
        weights2, options2 = self._weight_choices(ranking)

        # Our possible moves
        options1 = pick_options(self.state)

        remem_picks1 = self.state.picks1
        remem_picks2 = self.state.picks2
        ranking = []
        for option1 in options1:
            self.state.picks1 = remem_picks1 + [option1]

            # total win chance for option1 according to opp model
            score = 0.

            for weight2,option2 in zip(weights2, options2):
                # weight2: chance opponent picks option2
                self.state.picks2 = remem_picks2 + [option2]
                with calc_time:
                    possible_scores = PossibleScores(self.state)
                    chances = calc_chances(possible_scores, secret = True)
                win_chance, draw_chance, lose_chance = chances
                score += weight2 * (win_chance + draw_chance / 2)
            ranking.append((score, option1, None))   # None: possible_scores not returned

        self.state.picks1 = remem_picks1
        self.state.picks2 = remem_picks2
        print("D3Picking.D3_rank_picks calc_chances in %s" % calc_time)
        ranking.sort()
        return ranking

    def pick(self):
        ranking = self.D3_rank_picks()
        return ranking[-1][1]

class R3Picking(D3Picking):

    def pick(self):
        ranking = self.D3_rank_picks()
        weights, choices = self._weight_choices(ranking, 4)
        choice = pmf_sample(weights)

        print("R3Picking.pick:")
        cum = 0.
        for idx,weight in enumerate(weights):
            cum += weight
            print("  %s win_chance %.3f weight %.3f cumulative %.3f" % (choices[idx], ranking[idx][0], weight, cum))

        best = ranking[-1]
            
        print("R3Picking.pick: picking %s with win_chance %.3f and weight %.3f (best win_chance %.3f)" %
              (choices[choice], ranking[choice][0], weights[choice], best[0]))
        return choices[choice]


# The Picking class must come last, as it inherits from GamePlayer

class BasicRandomBot(BasicBetting, RandomPicking):
    pass

class DefaultRandomBot(DefaultBetting, RandomPicking):
    pass

class DefaultD2Bot(DefaultBetting, D2Picking):
    pass

class SimpleD2Bot(SimpleBetting, D2Picking):
    pass

class SRSimpleD2Bot(SensibleReveal, SimpleBetting, D2Picking):
    pass

class SimpleR2Bot(SimpleBetting, R2Picking):
    pass

class SimpleD3Bot(SimpleBetting, D3Picking):
    pass

class LessSimpleD2Bot(LessSimpleBetting, D2Picking):
    pass

class LessSimpleD3Bot(LessSimpleBetting, D3Picking):
    pass

class SRLessSimpleD3Bot(SensibleReveal, LessSimpleBetting, D3Picking):
    pass

class SRLessSimpleR3Bot(SensibleReveal, LessSimpleBetting, R3Picking):
    pass

class BasicD2Bot(BasicBetting, D2Picking):
    pass

class StupidRandomBot(StupidBetting, RandomPicking):
    pass

class StupidD2Bot(StupidBetting, D2Picking):
    pass
