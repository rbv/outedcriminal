#!/usr/bin/env python3

import numpy as np

import bots.round
import cordialminuet.board
import cordialminuet as cm
from cordialminuet.util import print3, Timer
import bots.simplebots
import bots.ce

verbose = False

method_to_move_name = {
    'pick':'pick',
    'reveal':'reveal',
    'first_bet':'bet',
    'bet_or_fold':'bet'
}

def send_to_player(player, call, state):
    """Get a player's move.
    player: A Player
    call:   A pending move returned from update_from_moves()
    state:  Censored state
    """
    if call is None:
        return None
    if isinstance(call, str):
        call = (call,)
    methodname = call[0]
    args = call[1:]
    method = getattr(player, methodname)
    #args.append(state)
    player._receive_historied_state(state)
    #state.history = []
    #player.receive_state(state)
    print("send %s.%s%s" % (player.__class__.__name__, methodname, args))
    result = method(*args)
    move_name = method_to_move_name[methodname]
    if isinstance(result, (list, tuple)):
        return [move_name] + list(result)
    else:
        return [move_name, result]


ante = 12

def test_():

    from calmerudition.search import PossibleScores
    from simplebots import _calc_chances, _format_chances
    state.picks1=[[0,1]]
    state.printout()
    possible_scores = PossibleScores(state)
    chances = _calc_chances(possible_scores, secret=False)
    print3("public win/draw/lose chances:", _format_chances(chances))

    exit()


def one_round(coins, bot0, bot1, seed = None):
    "Returns pair of coins remaining afterwards"
    state = bots.round.new_random_round(coins, seed)
    state.put_in_ante(ante)
    state.start_round()

    # Initial moves:
    pending = ('pick', 'pick')

    timer = Timer().start()

    while not state.done:
        print3("ITERATION, pot %s coins %s" % (state.pot, state.coins))
        #print3("ITERATION, pot %s state %s" % (state.pot, state.state))

        move0 = send_to_player(bot0, pending[0], state.censored_state(0))
        move1 = send_to_player(bot1, pending[1], state.censored_state(1))
        pending = bots.round.update_from_moves(state, move0, move1)
        if verbose:
            print("pending: %s" % (pending,))
            print("state:")
            print(state.printout())

    timer.stop()
    print3("Done in", timer)
    return state.coins

def one_game(bot0, bot1, max_rounds = 6, seed = None):
    "Returns (player 0 payout, num rounds)"
    init_coins = 100
    coins = [init_coins] * 2

    rounds = 0
    while coins[0] > 0 and coins[1] > 0:
        coins = one_round(coins, bot0, bot1, seed)
        rounds += 1
        if seed:
            seed += 1
        if rounds == max_rounds:
            break

    return (coins[0] - init_coins, rounds)
   
def testing():
    #bot1 = bots.ce.EndgameD2Bot()
    bot1 = bots.simplebots.DefaultD2Bot()
    bot1.start_game(0.)

    #bot1 = bots.ce.EndgameWalkBot()
    #bot2 = bots.simplebots.SimpleD2Bot()
    bot2 = bots.simplebots.SRLessSimpleR3Bot()
    #bot2 = bots.simplebots.DefaultD2Bot()
    #bot1 = bots.simplebots.DefaultD2Bot()
    print("%s, %s" % (bot1, bot2))
    coins = [100,100]
    coins = one_round(coins, bot1, bot2, seed=1)
    print("payout player0: %d" % (coins[0] - 100))


def vs_bots(bot0ctor, bot1ctor, rounds, max_rounds_per_game = 5):
    """Play bots against each other.
    Returns (payoffs, mean, 95% error margin, num_games)"""

    bots.round.disable_tribute = True

    payoffs = []
    rounds_to_go = rounds
    games = 0
    while rounds_to_go > 0:
        payout, rounds_played = one_game(bot0ctor(), bot1ctor(), max_rounds_per_game)
        rounds_to_go -= rounds_played
        games += 1
        print("payout player0: %d" % payout)
        payoffs.append(payout)
    payoffs = np.array(payoffs)
    payoffs /= float(ante)
    err_margin = payoffs.std(ddof = 1) / len(payoffs)**0.5 * 1.98
    return payoffs, payoffs.mean(), err_margin, games

def run_bots():
    "Play bots against each other"
    played = set()
    set0 = [
        #bots.simplebots.DefaultD2Bot,
        #bots.simplebots.BasicD2Bot,
        #bots.simplebots.SimpleD3Bot,
        bots.simplebots.SRLessSimpleD3Bot,
        bots.simplebots.SRLessSimpleR3Bot,
        bots.simplebots.SimpleD2Bot,
        #bots.simplebots.BasicRandomBot,
        #bots.simplebots.DefaultRandomBot,
        #bots.simplebots.StupidRandomBot, 
        #bots.simplebots.StupidD2Bot,
    ]
    set1 = [
        bots.simplebots.SimpleD2Bot,
        bots.simplebots.LessSimpleD2Bot,
        bots.simplebots.SimpleR2Bot,
        #bots.simplebots.BasicD2Bot,
        #bots.simplebots.LessSimpleD2Bot,
        #bots.ce.EndgameWalkBot,
        #bots.ce.EndgameD2Bot
    ]
    set1 = set0



    set0 = [bots.simplebots.SRLessSimpleR3Bot]
    # set0 = [
    #     #bots.simplebots.LessSimpleD2Bot,
    #     bots.simplebots.SRLessSimpleD3Bot
    # ]
    set1 = [bots.simplebots.DefaultD2Bot]

    rounds = 40

    ret = []
    for bot0 in set0:
        for bot1 in set1:
            pair = tuple(sorted((bot0.__name__, bot1.__name__)))
            if pair in played or bot0 is bot1:
                continue
            played.add(pair)
            _, mean, err, games = vs_bots(bot0, bot1, rounds)
            ret.append("%18s vs %18s: %.2f +/ %.2f  (%d games)" % (bot0.__name__, bot1.__name__, mean, err, games))

    for r in ret:
        print(r)
    print("Ran %d rounds each pairup" % rounds)
    
if __name__ == '__main__':
    testing()
