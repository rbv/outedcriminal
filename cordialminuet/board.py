import os
import random
import numpy as np

from . import util
from . util import colorize, print3


def _getMagicSquare(seed):
    "Returns a #-delimited string"
    if seed is None:
        seed = random.randint(0,2**32-1)
    with util.Timer() as timer:
        thisdir = os.path.split(__file__)[0]
        pipe = os.popen("%s/getMagicSquare6 %d" % (thisdir, seed))
        #pipe = os.popen("/mnt/common2/src/CordialMinuet/server/specialSquare6 %d" % (seed,))
        data = pipe.read()
    print(timer)
    return data

def generate_random_boards(num = 1000):
    for i in range(num):
        with open("magic_squares.txt", "a") as ofile:
            ofile.write(_getMagicSquare(None) + "\n")

_precomp_boards = []

def _load_precomputed():
    if len(_precomp_boards) == 0:
        with open("magic_squares.txt") as ifile:
            _precomp_boards[:] = ifile.readlines()

def random_board(seed = None):
    "Returns a random 6x6 magic square as 6 lists of 6"
    if seed is None:
        _load_precomputed()
        data = random.sample(_precomp_boards, 1)[0]
    else:
        data = _getMagicSquare(seed)
    ints = list(map(int, data.split('#')))
    ret = np.array(ints, dtype = np.int32).reshape((6,6))
    # ret = []
    # for rownum in range(6):
    #     ret.append(ints[6*rownum : 6*(rownum+1)])
    return ret


def show_board(board, picks1 = [], picks2 = []):
    """Draw a 6x6 board in colour.
    Each of picks1, picks2  is a sequence length 0 to 4 of (col1,col2) pairs
    from player's perspective. col1 is col for self, col2 is row for opponent:
    picks1: (col,row) followed by (col,None)
    picks2: (row,col) followed by (row,None)
    """

    ####### Extend both picks lists to same length
    picks1 = picks1[:3]
    picks2 = picks2[:3]
    while len(picks1) < len(picks2):
        picks1.append(('?', '?'))
    while len(picks2) < len(picks1):
        picks2.append(('?', '?'))

    ###### Calc colours

    # Couldn't get colour multiply to work; so hardcode. These are web-safe.
    # Colour blending code could be way simpler

    blank = 0xaaaa55
    green = 0x5fd700 #0x87ff00
    bright_green = 0x5fff5f #0x5fd75f
    green_green = 0x008700 # or 0x00af00 
    dark_green = green_green
    red = 0xd70000 #0xff8700
    bright_red = 0xff005f #0xd75f5f
    green_red = 0x5f5f00 #0x00af00
    red_red = 0x870000 #0x5f5f00 #0x5f0000
    dark_red = red_red
    dark = dark_red #0x3a3a3a

    colmap = {
        (blank,red):red,
        (blank,green):green,
        (red,red):red_red,
        (red_red,red):red_red,
        (green,green):green_green,
        (green_green,green):green_green,
        (green,red):green_red,
        (red,green):green_red,
        (green_red,green):green_red,
        (green_red,red):green_red,
        (dark_red,green):dark,
        (dark_green,red):dark,
    }
    # for (a,b),c in colmap.items():
    #     def xx(col):
    #         return colorize(' this ', rgb = 0, bg = col)
    #     print('%s + %s = %s' % (xx(a), xx(b), xx(c)))

    def add_colour(original, new):
        if original in (bright_red, bright_green):
            return original
        return colmap[(original,new)]

    fg_cols = [ [0x0] * 6 for _ in range(6) ]
    bg_cols = [ [blank] * 6 for _ in range(6) ]

    def colour_square(r, c, col):
        if r != '?' and c != '?':
            if col == green:
                bg_cols[r][c] = bright_green
            else:
                bg_cols[r][c] = bright_red
        if r != '?':
            for cidx in range(6):
                #if cidx != c:
                #bg_cols[r][cidx] = mult_colour(bg_cols[r][cidx], col,0.4,0.6)
                bg_cols[r][cidx] = add_colour(bg_cols[r][cidx], col)
                if c != '?':
                    # Totally known, so darken this row
                    bg_cols[r][cidx] = add_colour(bg_cols[r][cidx], col)
        if c != '?':
            for ridx in range(6):
                #bg_cols[ridx][c] = mult_colour(bg_cols[ridx][c], col,0.4,0.6)
                bg_cols[ridx][c] = add_colour(bg_cols[ridx][c], col)
                if r != '?':
                    # Totally known, so darken this column
                    bg_cols[ridx][c] = add_colour(bg_cols[ridx][c], col)

    for (c1,c2),(r2,r1) in zip(picks1, picks2):
        # All from Player 1 POV
        colour_square(r1, c1, green)
        colour_square(r2, c2, red)

    ###### Display
    print3()
    for ridx,row in enumerate(board):
        line = ''
        for cidx,cell in enumerate(row):
            cell = '%3s ' % cell
            line += colorize(cell, rgb = fg_cols[ridx][cidx], bg = bg_cols[ridx][cidx])
        print(line)

def _test_show_board():
    #util._test_colors()
    board = random_board(4)
    show_board(board, [[2,'?'], ['?','?']],[['?','?'],['?',2]])
    show_board(board, [[2,'?']],[['?',2]])
    show_board(board, [[2, 3], [1,0]], [[0,1]])
    show_board(board, [[2, 3], [1,0]], [[0,1], ['?',2]])
    show_board(board, [[2, 3],[5,4]], [[0,1], ['?',2]])
    show_board(board, [[2, 3],[5,4]], [[0,1], [3,2]])
    show_board(board, [[2, 3],[5,4],[0,1]], [['?',1], [3,2],['?',5]])


if __name__ == '__main__':
    #_test_show_board()
    for i in range(10):
        show_board(random_board(i))
