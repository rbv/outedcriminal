#!/usr/bin/env python

import sys


try:
    Elo1, Elo2, coins1, coins2, K = [float(x) for x in sys.argv[1:]]
except:
    print("Usage: Elo1 Elo2 coins1 coins2 K ")
    print("Where K is 32 or 64")
    exit()

assert coins1 + coins2 <= 200

def score(coins):
    if coins < 100:
        return 0.
    elif coins > 100:
        return 1.
    return 0.5

S1 = score(coins1)
S2 = score(coins2)

Ex1 = 1. / (1 + 10 ** ((Elo2 - Elo1) / 400.))
Ex2 = 1. - Ex1
print("scores, expected: %.3f %.3f, actual %.3f %.3f" % (Ex1, Ex2, S1, S2))

house_coins = 200 - coins1 - coins2

K_scale = ( abs(coins1 - coins2) + house_coins ) / 200.

change1 = K * K_scale * (S1 - Ex1)
change2 = K * K_scale * (S2 - Ex2)

print("player 1: change by %d to %d" % (change1, Elo1 + change1))
print("player 2: change by %d to %d" % (change2, Elo2 + change2))




