import sys
import os
import time

import numpy as np
np.set_printoptions(precision = 5, suppress = True)

if os.name == 'posix':
    timer = time.time
else:
    timer = time.clock

py_ver = sys.version_info.major

if py_ver >= 3:
    def bytes_to_str(s):
        return str(s, 'ASCII')
else:
    def bytes_to_str(s):
        return s

if py_ver >= 3:
    import builtins
    print3 = getattr(builtins, 'print')  # avoid print keyword in python2
else:
    def print3(*args):
        sys.stdout.write(' '.join(str(x) for x in args) + '\n')


class Timer():
    """
    Utility class for finding total time spent in multiple sections of code.
    Is a context manager. Use either like:
        timing = Timer()
        with timing:
            ...
    or
        with Timer() as timing:
            ...
        print 'Done in', timing
    """
    def __init__(self):
        self.time = 0.
    def start(self):
        self._start = timer()
        return self
    def stop(self):
        self.time += timer() - self._start
        del self._start
        return self
    def __enter__(self):
        self.start()
        return self
    def __exit__(self, *args):
        self.stop()
    def __str__(self):
        if hasattr(self, '_start'):
            #return '<Timer running>'
            return '%.3gs' % (timer() - self._start)
        return '%.3gs' % self.time
    __repr__ = __str__


# Terminal colours:  color
try:
    from xtermcolor import colorize
    if os.name == 'nt':
        try:
            # This makes ANSI colour code sequences (via xtermcolor)
            # work on Windows
            import colorama
            colorama.init()
        except ImportError:
            colorize = lambda x, **kwargs: x
except ImportError:
    print("import colorize failed")
    colorize = lambda x, **kwargs: x


def rgb(col):
    "Convert colour from hex to triple"
    return ((col >> 16) & 0xff, (col >> 8) & 0xff, col & 0xff)
def from_rgb(r, g, b):
    "Convert colour from triple to hex"
    return r << 16 | g << 8 | b
def mult_colour(col1, col2, frac1 = 0., frac2 = 1.):
    "Multiply two colours (given in hex)"
    r1,g1,b1 = rgb(col1)
    r2,g2,b2 = rgb(col2)
    def comb(v1, v2):
        return int(v2 * frac1 + frac2 * v1 * v2/255)
    return from_rgb(comb(r1,r2), comb(g1,g2), comb(b1,b2))

def _test_colors():
    "Prints all available colours (can just run 'xtermcolor list' instead)"
    from xtermcolor.ColorMap import XTermColorMap as ColorMap
    for xterm, hexcolor in ColorMap().getColors().items():
        print(colorize('   ' + hex(hexcolor), rgb = 0x0, bg = hexcolor))

# For matplotlib
colours = ['b', 'r', 'm', 'y', 'c', 'k'] # 'm'
linestyles = []
for style in ['-', '--', ':', '-.']:
    linestyles.extend([col + style for col in colours])



def all_unique(seq):
    "Whether every element of the sequence is unique"
    return len(set(seq)) == len(seq)
