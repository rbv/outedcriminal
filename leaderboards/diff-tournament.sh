#!/bin/bash
name=$1
if [ -z $name ] ; then
    echo "Usage: diff-tournment.sh name"
    exit
fi
  

####./get-scores.sh
num=$(./last-number.sh tournament_$name)
((num++))

echo "wget " tournament_${name}_${num}.txt
echo

wget "http://cordialminuet.com/gameServer/server.php?action=tournament_report&code_name=${name}" -O - > scoreboards/tournament_${name}_${num}.txt &&

./leaderboard.py scoreboards/tournament_${name}_$((num-1)).txt scoreboards/tournament_${name}_${num}.txt
