#!/usr/bin/python

import os
import time
import sys

# If name specified, print their latest games

filter_name = None
if len(sys.argv) > 1:
    filter_name = sys.argv[1]

pin, pout = os.popen2(['./leaderboard.py', 'scoreboards/leaders_games_207.txt', '--only-names'])
names = pout.read().strip().split('\n')
pout.close()
pin.close()
print "names", names

if False:
    # Fetch all users' recent games into leakstuff/*

    start = 0
    for name in names[:]:
        if name == 'hypothesis virtue':
            start = 1
        if start:
            fetch = "wget http://minuet.isans.net/json/profit?user=" + name.replace(" ", "%20") + " -O 'leakstuff/" + name + ".txt'"
            print fetch
            os.system(fetch)

# The profit pages contain a bunch of duplicates, at least some of the older data
biglist = set()

ti = 'Thu Mar 19 10:28:52 2015'
early_cutoff =  time.mktime(time.strptime(ti, '%a %b %d %H:%M:%S %Y'))

#email sent at 8 Apr 2015 14:33:57 -0400 (EDT)   6:33 on the 9th, NZST
# Ignore everything more recent than this
cutoff = time.mktime((2015,4,9,6,33,0,0,0,0))
print cutoff - early_cutoff, "seconds", (cutoff - early_cutoff) / 60. / 60., "hours"

#exit()

oldest = 1e99
newest = 0
if False:
    # Read in all the user-specific json dumps

    for name in names:
        fname = 'leakstuff/' + name + ".txt"
        if not os.path.isfile(fname):
            continue
        with open(fname) as fff:
            data = eval(fff.read())
        print name, len(data)
        for datum in data:
            assert datum['user'] == name
            biglist.add((datum['time'], datum['user'],datum['change']))
        if len(data):
            print time.ctime(data[0]['time']), "----", time.ctime(data[-1]['time'])
            oldest = min(oldest, data[-1]['time'])
            newest = max(newest, data[0]['time'])
else:
    fname = 'profithistory/profits_0.txt'
    with open(fname) as fff:
        data = eval(fff.read())
    print "loaded %d bytes" % len(data)
    for datum in data:
        biglist.add((datum['time'], datum['user'],datum['change']))
    if len(data):
        print time.ctime(data[0]['time']), "----", time.ctime(data[-1]['time'])
        oldest = min(oldest, data[-1]['time'])
        newest = max(newest, data[0]['time'])

print "oldest", oldest, newest
print time.ctime(oldest), "oldest"
print time.ctime(newest), "newest"

biglist=list(biglist)
biglist.sort(reverse=1)

if filter_name:
    for ttime, user, change in biglist:
        if user == filter_name:
            print ttime, user, change, time.ctime(ttime)
    exit()

with open("emailleak/leak.txt") as fff:
    data = fff.readlines()
    emails = [x for x in data if '@' in x]


last_games = {}
users = {}
for ttime, user, change in biglist:
    # if ttime > 1428087753 or ttime < 1428076935:
    #     # ignore games not in first vs_one contest
    #     continue

    if ttime > cutoff or ttime < early_cutoff:
        continue
    if user not in users:
        print '%24s' % user, '  ', change, time.ctime(ttime)
    if user in users:
        users[user]+=1
    else:
        users[user]=1
    #if user not in last_games:
    last_games[user] = ttime
    # if len(users) == 110:
    #     print time.ctime(ttime)
    #     break
for user,games in users.items():
    print user, games

items = last_games.iteritems()
items = [(gtime, user) for (user,gtime) in items]
items.sort(reverse=0)

for idx,((gtime,user),email) in enumerate(zip(items, emails)):
    print "%25s %25s %3d %3d %s" % (time.ctime(gtime), user, idx, 109-idx, email.strip())

print len(biglist), "changes"
print len(emails), "emails"
print len(items), "changes in range"
print len(users), "users in range"
