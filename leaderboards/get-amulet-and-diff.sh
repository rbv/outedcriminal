#!/bin/bash

dontdownload=${1-}

name=burningSting


mkdir -p amulet_contest

if [ $dontdownload ]; then 
    num=$(./last-number.sh $name amulet_contest)
else
    num=$(./last-number-any.sh)
    ((num++))
    echo $num
    wget "http://cordialminuet.com/gameServer/server.php?action=vs_one_report&vs_one_code_name=$name" -O - > amulet_contest/${name}_$num.txt

    #./get-scoreboard.sh leaders_elo scoreboards/leaders_elo_$num.txt
fi

./leaderboard.py amulet_contest/${name}_$num.txt

prevnum=$(./second-last-number.sh $name amulet_contest)

./leaderboard.py amulet_contest/${name}_$prevnum.txt amulet_contest/${name}_$num.txt

#./show-diff.sh '================ ELO ================' leaders_elo
