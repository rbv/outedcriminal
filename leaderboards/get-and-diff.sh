#!/bin/bash
#  Fetch and save leaderboards and diff to previously saved

./get-elo.sh
./get-scores.sh


./show-diff.sh '================ PROFIT ================' leaders_profit

./show-diff.sh '================ DOLLAR ================' leaders_dollar

./show-diff.sh '================ GAMES ================' leaders_games


./show-diff.sh '================ ELO ================' leaders_elo
./show-diff.sh '================ PROV ELO ================' leaders_elo_prov
