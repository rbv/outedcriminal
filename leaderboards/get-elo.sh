#!/bin/bash

num=$(./last-number-any.sh leaders_elo)
((num++))
echo $num

echo "wget " leaders_elo_$num.txt
echo

./get-scoreboard.sh leaders_elo scoreboards/leaders_elo_$num.txt
./get-scoreboard.sh leaders_elo_provisional scoreboards/leaders_elo_prov_$num.txt
