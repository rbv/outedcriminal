#!/bin/bash
# Fetch and concatenate all pages of a leaderboard

page="http://cordialminuet.com/gameServer/server.php?action=$1"
outfile=$2
if [ ! $outfile ]; then
    echo "get-scoreboard.sh: Missing outfile"
    exit 1
fi

echo "wget " $outfile

skip=0
while true; do
    wget "$page&skip=$skip" -O - >> $outfile
    if ! grep -q "$skip\." $outfile ; then
        break
    fi
    ((skip=skip+100))
done

