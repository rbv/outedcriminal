#!/bin/bash
#  Fetch and save dollars and profit

num=$(./last-number-any.sh)
((num++))
echo $num

echo "wget " leaders_dollar_$num.txt
echo

# Just top 300
wget http://cordialminuet.com/gameServer/server.php?action=leaders_dollar -O - > scoreboards/leaders_dollar_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_dollar&skip=100" -O - >> scoreboards/leaders_dollar_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_dollar&skip=200" -O - >> scoreboards/leaders_dollar_$num.txt
#./get-scoreboard.sh leaders_dollar scoreboards/leaders_dollar_$num.txt


./get-scoreboard.sh leaders_profit scoreboards/leaders_profit_$num.txt

sleep 2

# Just top 500
wget http://cordialminuet.com/gameServer/server.php?action=leaders_games -O - > scoreboards/leaders_games_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_games&skip=100" -O - >> scoreboards/leaders_games_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_games&skip=200" -O - >> scoreboards/leaders_games_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_games&skip=300" -O - >> scoreboards/leaders_games_$num.txt
wget "http://cordialminuet.com/gameServer/server.php?action=leaders_games&skip=400" -O - >> scoreboards/leaders_games_$num.txt
#./get-scoreboard.sh leaders_games scoreboards/leaders_games_$num.txt

