#!/usr/bin/python
import os
import time
import sys

outdir = 'profithistory/'
if not os.path.isdir(outdir):
    os.mkdir(outdir)

# If limit specified, only fetch that many changes

limit = 1e99
if len(sys.argv) > 1:
    limit = int(sys.argv[1])

def fname(idx):
    return outdir + 'profits_%d.txt' % (idx,)

idx = 0
while os.path.isfile(fname(idx)):
    idx += 1

alldata = []
start = 0
while True:
    fetch = "wget http://minuet.isans.net/json/profit?start=" + str(start) + " -O -" #'%s'" % (fname(idx),)
    print fetch
    pipe = os.popen(fetch)

    data = eval(pipe.read())
    assert type(data) == list
    alldata += data
    pipe.close()

    if len(data) == 0:
        break
    start += 100

print alldata
print "Wrote", len(alldata), "profit records to", fname(idx)
print "Final game occurred at", time.ctime(alldata[-1]['time'])

with open(fname(idx),"w") as fff:
    fff.write(str(alldata))
