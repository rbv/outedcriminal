#!/usr/bin/env python
import os
import sys
import time
import datetime
import leaderboard
import matplotlib.pyplot as plt
import sys
sys.path.append("..")
from cordialminuet import util
import numpy as np

def load_scores(prov_ok = True):
    global elo_records, elo_prov_records
    elo_records = []
    elo_prov_records = []
    dirname = 'scoreboards'
    sys.stderr.write("Reading leaderboards")
    for fname in os.listdir(dirname):
        fname = os.path.join(dirname, fname)
        if 'elo' in fname:
        #if 'profit' in fname:
            sys.stderr.write(".")
            with open(fname) as fil:
                accounts, updated = leaderboard.parse_html(fil.read())
            date = os.stat(fname).st_ctime
            #if accounts.itervalues().next().amount < 900:
            records = accounts.itervalues()
            if len(accounts) > 0 and records.next().amount < 900:
                if 'prov' in fname:
                    elo_prov_records.append((date, accounts))
                else:
                    elo_records.append((date, accounts))
    sys.stderr.write("\n")


def track_single_person(who):
    records = []
    for date, accounts in elo_records + elo_prov_records:
        if who in accounts:
            amount = accounts[who].amount
            records.append((date, amount))
    print records
    return records

def averages():
    records = []
    for date, accounts in elo_records:
        avg = leaderboard.total_amount(accounts) / float(len(accounts))
        records.append((date, avg))
    return records

def maximums():
    records = []
    for date, accounts in elo_records:
        amount = max(rec.amount for (name, rec) in accounts.iteritems())
        records.append((date, amount))
    return records

def personal_highs():
    """Show personal time score and time for each person"""
    highs = {}
    for date, accounts in elo_records:
        for (name, rec) in accounts.iteritems():
            amount = rec.amount
            if name not in highs:
                highs[name] = (amount, date)
            else:
                prev = highs[name][0]
                if amount > prev:
                    highs[name] = (amount, date)
    highs = [(amnt,name,ttime) for  name,(amnt,ttime) in highs.items()]
    highs.sort(reverse=1)
    for idx, (amnt,name,ttime) in enumerate(highs[:40]):
        print "%2d. %3d [on %s] %s" % (idx+1, amnt, time.strftime("%d %b %Y", time.localtime(ttime)), name)


def personal_averages():
    """Show personal time score and time for each person"""
    highs = {}
    for date, accounts in elo_records:
        for (name, rec) in accounts.iteritems():
            amount = rec.amount
            if name not in highs:
                highs[name] = [0,0]
            prev = highs[name]
            prev[0] += amount
            prev[1] += 1
    highs = [(float(total)/counts,name) for  name,(total,counts) in highs.items()]
    highs.sort(reverse=1)
    for (avg,name) in highs[:20]:
        print "%3d %s" % (avg, name)


def plot(records, name, label):
    records.sort()
    dates, scores = zip(*records)
#    dates = [datetime.datetime(*time.localtime(x).timetuple()) for x in dates]
    #date_names = [datetime.datetime.fromtimestamp(x) for x in dates]
    stylenum = len(plt.gca().lines)
    style = util.linestyles[stylenum]
    #scores = np.log(scores)
    plt.plot(dates, scores, style, label = "%s (%s)" % (name, label), linewidth = 2.5)

    date_ticks = np.linspace(dates[0], dates[-1], 20)
    #date_tick_labels = [datetime.datetime.fromtimestamp(x) for x in date_ticks]
    date_tick_labels = [time.strftime("%d-%m-%y", time.localtime(x)) for x in date_ticks]
    plt.xticks(date_ticks, date_tick_labels, rotation = 20)  # slant 20 degrees

def graph_user(name, label):
    plot(track_single_person(name), name, label)

def people():
    load_scores()
    plt.figure()
    #plot(maximums(), '', 'Maximum Non-prov Elo')
    graph_user('jeopardy alcohol', '???')
    graph_user('creature expression', 'jere')
    graph_user('cobblestone inducement', 'CS')
    #graph_user('piano bullet', '???')
    graph_user('upkeep crime', '???')
    #graph_user('clock form', '???')
    graph_user('bird graduation', 'joshwithguitar')
    #graph_user('inducement vanity', 'jere's friend')
    graph_user('incident clemency', 'Nate')
    #graph_user('judge doorman', 'cullman')
    graph_user('journal doorman', 'Jason?')
    graph_user('string corn', '???')
    #graph_user('butterfly elaboration', 'Jason')
    #graph_user('yacht month', 'Jason2')
    graph_user('forethought tobacco', 'AnoHito')
    plot(averages(), '', 'Average Non-prov Elo')
    
    plt.legend(loc = 'best')

if __name__ == '__main__':
    load_scores()
    print "Highs"
    personal_highs()
    print "Averages"
    personal_averages()
