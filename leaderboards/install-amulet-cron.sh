#!/bin/sh

cwd=$(pwd)
script=get-amulet-and-diff.sh
crontab -l > cron.tmp
echo Old crontab contents:
cat cron.tmp
grep -v get-amulet-and-diff cron.tmp > cron2.tmp
#echo "2,8,14,20,26,32,38,44,50,56 * * * * cd $cwd && ./$script " >> cron2.tmp
echo New crontab:
cat cron2.tmp
crontab cron2.tmp
rm cron.tmp
rm cron2.tmp

