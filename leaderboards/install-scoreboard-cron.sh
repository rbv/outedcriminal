#!/bin/sh

crontab -l > cron.tmp
echo Old crontab contents:
cat cron.tmp
grep -v outedcriminal cron.tmp > cron2.tmp
echo "37 * * * * cd ~/src/outedcriminal/leaderboards && ./get-and-diff.sh > /dev/null" >> cron2.tmp
echo "42 5,11,17,23 * * * cd ~/src/outedcriminal/leaderboards && ./get-users-graph.sh > /dev/null" >> cron2.tmp
echo New crontab:
cat cron2.tmp
crontab cron2.tmp
rm cron.tmp
rm cron2.tmp
