#!/bin/bash
#  Return the number of the highest numbered leaderboard in scoreboards/
mkdir -p scoreboards/
/bin/ls scoreboards/ | grep leader | grep -o "[0-9]\+" | sort -n | tail -n 1
