#!/usr/bin/python
# Outed Criminal

import sys
import re
import os
import time

def file_ctime(filename):
    "File creation time as a string"
    ctime = os.stat(filename).st_ctime
    return time.ctime(ctime)

# An account is a (name, Record) pair, especially a dict (key, value) item

class Record:
    "A row of a table, having either an amount (e.g. Elo rating) or diff"
    def __init__(self, amount, rank):
        self.amount = amount
        self.rank = rank
        self.rank_change = ''
        self.amount_change = ''
    def compare_to_previous(self, previous_record):
        "Returns a 'diff' Record"
        ret = Record(None, None)
        ret.amount = self.amount - previous_record.amount
        ret.rank_change = '(%s -> %s)' % (previous_record.rank, self.rank)
        ret.amount_change = '(%s -> %s)' % (previous_record.amount, self.amount)
        return ret
    def __cmp__(self, rhs):
        if self.amount > rhs.amount + 1e-7:
            return 1
        elif self.amount + 1e-7 < rhs.amount:
            return -1
        #return 0
        #print int(self.rank), int(rhs.rank)
        return cmp(self.rank, rhs.rank)


def parse_html(html):
    rows = re.findall('<tr>.*?</tr>', html)
    accounts = {}
    vs_one = False
    # None or string? (Actually match object)
    updated = re.search('Updated .* ago', html)
    if not updated:
        updated = ''
    else:
        updated = updated.group(0)
    for row in rows:
        if '<hr>' in row:
            continue
        if 'Coins Won' in row:  # vs_one amulet page
            vs_one = True
            continue
        #print "ROW", row
        # Note this skips empty <td></td> which might be significant rather than spacing (eg for vs_one)
        cells = re.findall('<td[^>]*>([^<]+?)</td>', row)
        #print cells
        assert cells[0][-1] == '.'  # eg 4.
        rank = cells[0][:-1]
        if vs_one:
            if len(cells) == 4:
                assert cells[1] == '*'
                del cells[1]
            if len(cells) == 2 and cells[1] == '20':
                # glitch player with blank name
                cells = [cells[0], 'BLANK', cells[1]]
            assert len(cells) == 3
            amount = int(cells[2])
        elif '$' in cells[2]:
            temp = cells[2].replace('$', '')
            temp = temp.replace(',', '')
            amount = float(temp)
        else:
            # Elo
            amount = int(cells[2])
        name = cells[1]
        accounts[name] = Record(amount, rank)
    return accounts, updated

def compute_diffs(before, after):
    "Given two dicts of Records, returns dict of differences in amounts and ranks"
    new_accounts = set(after.keys()) - set(before.keys())
    if len(new_accounts):
        print "new accounts:"
        print_accounts({name:after[name] for name in new_accounts})
        print
    missing_accounts = set(before.keys()) - set(after.keys())
    if len(missing_accounts):
        print "missing accounts:"
        print_accounts({name:before[name] for name in missing_accounts})
        print
    diff = {}
    for name in set(before.keys()).intersection(after.keys()):
        diff[name] = after[name].compare_to_previous(before[name])
    return diff

def print_accounts(accounts):
    sorted_accounts = [(a,b) for b,a in accounts.items()]
    sorted_accounts.sort(reverse = True)
    if len(sorted_accounts) and len(sorted_accounts[0][0].rank_change):
        formstr = "%4d %14s: %24s %-7.4g %s"
    else:
        formstr = "%4d%s: %24s %-7.4g %s"
    for idx, (rec, name) in enumerate(sorted_accounts):
        print formstr % (1 + idx, rec.rank_change, name, rec.amount, rec.amount_change)

def print_sorted_names(accounts):
    sorted_accounts = [(a,b) for b,a in accounts.items()]
    sorted_accounts.sort(reverse = True)
    for idx, (rec, name) in enumerate(sorted_accounts):
        print name

def nonzero_accounts(accounts, zero = 0):
    return {name:rec for name,rec in accounts.iteritems() if rec.amount != zero}

def abs_of_accounts(accounts):
    return {name:Record(abs(rec.amount), '') for name,rec in accounts.iteritems()}

def total_amount(accounts):
    return sum(rec.amount for name,rec in accounts.iteritems())



def usage():
    print "Usage:"
    print " leaderboard.py page1.html"
    print "   Parse and print a leaderboard page"
    print " leaderboard.py page1.html --only-names"
    print "   Parse a leaderboard page and print only the account names"
    print " leaderboard.py page1.html page2.html"
    print "   Print the diff between two leaderboard pages"
    exit()

if __name__ == '__main__':
    
    if len(sys.argv) <= 1:
        usage()

    only_names = False
    if '--only-names' in sys.argv:
        only_names = True
        sys.argv.remove('--only-names')
    if len(sys.argv) not in (2, 3):
        usage()

    before, updated1 = parse_html(open(sys.argv[1]).read())

    if only_names:
        print_sorted_names(before)
        exit()

    print "%s created %s   %s" % (sys.argv[1], file_ctime(sys.argv[1]), updated1)

    is_elo = 'elo' in sys.argv[1]
    show_avg = is_elo
    if len(sys.argv) == 3:
        after, updated2 = parse_html(open(sys.argv[2]).read())
        print "%s created %s   %s" % (sys.argv[2], file_ctime(sys.argv[2]), updated2)
        print "Num records:", len(after)
        accounts = compute_diffs(before, after)
        print_accounts(nonzero_accounts(accounts))
        show_avg = False
    else:
        #print_accounts(nonzero_accounts(before))
        print_accounts(before)
        accounts = before

    if show_avg:
        print "avg:", total_amount(accounts) / float(len(accounts))
        if is_elo:
            zero = 1
        else:
            zero = 0
        nonzero = nonzero_accounts(accounts, zero)
        print "avg of nonzero:", total_amount(nonzero) / float(len(nonzero))
    else:
        print "total diff:", total_amount(accounts), "total of absolute diffs:", total_amount(abs_of_accounts(accounts))


