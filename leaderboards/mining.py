#!/usr/bin/python
import os
import leaderboard

"""
misc snippets for 'mining' data
"""

board = leaderboard.parse_html(open("scoreboards/leaders_games_207.txt","r").read())
#leaderboard.print_accounts(board)


lis = """mileage gore
institute deed
northwest nutmeg
nail victim
monk oven
saloon armadillo
reflection algebra
reminder islander
intellect salary
air cigar
garden teacher
flood agility
pride blacksmith
arrow love
hardwood tribute
tobacco contract
opinion journal
cell vegetable
fabric newspaper
library performer
lobster animosity
fireplace apple
alimony hound
adage sonata
present life
corpse fur
lubricant distraction
body brute
panic book
lemonade pipe
oats phantom
goddess thorn
velocity violation
originator jeopardy
tank cowhide
robbery destruction
workhouse oats
honour yacht
betrayal poster
upkeep crime
hypothesis event
prestige piano
poverty alimony
toy hairpin
girl procession
house seat"""

xx = []
for name in lis.split('\n'):
    xx.append((board[name].amount,name))
xx.sort()
for games,name in xx:
    print games, name
