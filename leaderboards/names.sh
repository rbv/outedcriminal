

# Number of users
echo "Num users:"
./elo.sh --only-names | sort -u | wc -l

# Number of times each noun is used: alphabetical
echo
echo "================ Alphabetically ================"
echo
./elo.sh --only-names | tr ' ' '\n' | sort | uniq -c | wc -l

# Number of times each noun is used: alphabetical
echo
echo "================ By num occurrences ================"
echo
./elo.sh --only-names | tr ' ' '\n' | sort | uniq -c | sort
