#!/usr/bin/python3

import math
def prob_all_names_unique(users, names):
    """Birthday paradox: probability of every users getting
    assigned a unique name"""
    def lfact(n): return math.lgamma(n+1)
    # names! / (names**users * (names - users)!)
    temp = lfact(names) - users * math.log(names) - lfact(names - users)
    return math.exp(temp)

for x in range(780, 800, 1):
    print(x, prob_all_names_unique(x, 666*665))
