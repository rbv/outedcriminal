#!/bin/bash
#  Return the number of the highest numbered leaderboard in scoreboards/
dir=${2:-scoreboards}
mkdir -p $dir/
/bin/ls $dir/ | grep $1 | grep -o "[0-9]\+" | sort -n | tail -n 2 | head -n 1
