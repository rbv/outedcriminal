#!/bin/bash

if ! `echo $2 | grep -q leaders_`; then
    echo "Page name should start with leaders_"
    exit 1
fi
title=$1
page_name=$2
regex=${page_name}_[0-9]
num=$(./last-number.sh $regex)
prevnum=$(./second-last-number.sh $regex)
echo ${page_name}_$prevnum.txt ${page_name}_$num.txt

echo $title
# ================= ELO ==================
./leaderboard.py scoreboards/${page_name}_$prevnum.txt scoreboards/${page_name}_$num.txt
