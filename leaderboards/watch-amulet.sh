
num=$(./last-number-any.sh)
((num++))
./get-scoreboard.sh leaders_dollar scoreboards/leaders_dollar_$num.txt

./get-scoreboard.sh leaders_elo scoreboards/leaders_elo_$num.txt

echo '================ AMULET ================'
./get-amulet-and-diff.sh

./show-diff.sh '================ ELO ================' leaders_elo

./show-diff.sh '================ DOLLAR ================' leaders_dollar
