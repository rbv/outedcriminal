#!/usr/bin/env python
from distutils.core import setup
from Cython.Build import cythonize
from numpy.distutils.misc_util import get_numpy_include_dirs
import os


if not os.path.isdir("build"):
    os.makedirs("build")

script_args = ['build_ext', '-i']
#script_args.append("--compiler=intel")
if os.name == 'nt':
    script_args.append("--compiler=mingw32")

setup(ext_modules = cythonize("bots/calmerudition/inner_circle.pyx", build_dir = "build"),
      script_args = script_args,
      include_dirs = get_numpy_include_dirs())
